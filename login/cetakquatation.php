<?php 
include '../config.php';
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>Cetak Data Quotation</title>
 	<style type="text/css">
		body{
			font-family: sans-serif;
		}
		table{
			margin: 20px auto;
			border-collapse: collapse;
		}
		table th,
		table td{
			border: 1px solid #3c3c3c;
			padding: 3px 8px;

		}
		a{
			background: blue;
			color: #fff;
			padding: 8px 10px;
			text-decoration: none;
			border-radius: 2px;
		}
	</style>
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />
 </head>
 <body>
<?php 
$kd_product_pres = $_GET['kd_product_pres'];
$query1 = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND create_quatation.kd_product_pres='$kd_product_pres'")or die(mysqli_error($db));
$h1 = mysqli_fetch_assoc($query1);

 ?>
 <h1 align="center"><?php echo $h1['nm_cust'] ?></h1>
 	<table>
 		<tr>
 			<th>No Quotation</th>
 			<th>Jenis Layanan</th>
 			<th>Bandwidth</th>
 			<th>Nbr of Qty/Volume</th>
 			<th>Unit Price(Rp)</th>
 			<th>Subtotal(Rp)</th>
 			<th>Type Of Payment</th>
 			<th>Note/Remark</th>
 		</tr>
 		<?php 
 		if($_SESSION['level']=="Sales"){
 			$query = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND create_quatation.kd_product_pres='$kd_product_pres' AND cust.kd_pengguna='$_SESSION[kd_pengguna]'")or die(mysqli_error($db));	
 		}else{
 			$query = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND create_quatation.kd_product_pres='$kd_product_pres'")or die(mysqli_error($db));
 		}
 		while ($data = mysqli_fetch_assoc($query)) {

 		 ?>
 		<tr>
 			<td><?php echo $data['no_quad'] ?></td>
            <td><?php echo $data['nm_jns_layanan'] ?></td>
            <td><?php echo $data['bw_quad'] ?></td>
            <td><?php echo $data['qty'] ?></td>
            <td>Rp. <?php echo $data['price'] ?></td>
            <td>Rp. <?php echo $data['subtotal'] ?></td>
            <td><?php echo $data['top'] ?></td>
            <td><?php echo $data['note'] ?></td>
 		</tr>
 		<?php } ?>
 	</table> 
 </body>
 <script type="text/javascript">
 	window.print();
 </script>
 </html>