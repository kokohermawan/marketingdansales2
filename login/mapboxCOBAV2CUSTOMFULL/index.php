  <?php 
      include 'assets/phppower/locations_model.php';
  ?>
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset='utf-8' />
      <title>MAPS CUSTOMER</title>
      <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
      <script src="assets/js/lightbox.js"></script>
      <link rel="stylesheet" href="assets/css/lightbox.css">
      <link rel="shortcut icon" type="image/x-icon" href="assets/images/icons8_map_marker_48_azA_icon.ico">
      <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
      <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.5.0/mapbox-gl.js'></script>
      <script src="assets/geojs/banjar.js" type="text/javascript"></script>
      <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.5.0/mapbox-gl.css' rel='stylesheet' />
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    

    
      <style>
       
        body { 
          color: #404040;
          font: 400 15px/22px 'Source Sans Pro', 'Helvetica Neue', Sans-serif;
          margin: 0;
          padding: 0;
          -webkit-font-smoothing: antialiased;
        }
                  
        * {
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
        }


        h1 {
          font-size: 18px;
          margin: 0;
          font-weight: 700;
          line-height: 20px;
          padding: 20px 2px;
        }


        h2,
        h3 {
          margin: 10px;
          font-size: 1.2em;
          font-weight: 700;
        }


        u {text-decoration: underline;}


        p {
          font-size: 0.85em;
          margin: 10px;
          text-align: left;
        }

      
        a {
          color: #404040;
          text-decoration: none;
        }
      
      
        a:hover {color: #101010;}


        sup {
          vertical-align: super;
          font-size: smaller;
        }


        img {
          top: 0;
          width: 240px;
          height: auto;
        }


        #zoom img {
          /*transform:scale 1s;
          animation: scale 1s;*/
          -moz-animation: scale 2.5s; /* Firefox */
          -webkit-animation: scale 2.5s; /* Safari and Chrome */
          -o-animation: scale 2.5s; /* Opera */
          margin-top:-20px;}
          @keyframes scale {
          from {
          width:107px;
          }
          to {
          width:240px;
          }}
          @-moz-keyframes scale { /* Firefox */
          from {
          width:107px;
          }
          to {
          width:240px;
          }}
          @-webkit-keyframes scale { /* Safari and Chrome */
          from {
          width:107px;
          }
          to {
          width:240px;
          }}
          @-o-keyframes scale { /* Opera */
          from {
          width:107px;
          }
          to {
          width:240px;
          }
        }
                  
        .sidebar {
          position: absolute;
          width: 22.3333%;
          height: 100%;
          top: 0;
          right: 0;
          overflow-y: scroll;
          border-right: 1px solid rgba(0, 0, 0, 0.25);
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
        }

    
        @media screen and (max-width: 800px) {
          .sidebar {
            position: absolute;
            width: 33.3333%;
            height: 100%;
            top: 0;
            right: 0;
            overflow-y: scroll;
            border-right: 1px solid rgba(0, 0, 0, 0.25);
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
          }
        }
    

        .pad2 {padding: 0px;}
    

        .map {
          position: absolute;
          right: 22.3333%;
          width: 77.6666%;
          top: 0;
          bottom: 0;
        }

        @media screen and (max-width: 800px) {
        .map {
            position: absolute;
            right: 33.3333%;
            width: 66.6666%;
            top: 0;
            bottom: 0;
          }
        }

      
        .heading {
          background: DodgerBlue;
          color: #eee;           
          max-height: 60px;
          line-height: 30px;
          padding: 0 5px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.4), 0 6px 20px 0 rgba(0,0,0,0.38);           
        }
      
      
        .listings {
          height: 100%;
          overflow: auto;
          padding-bottom: 60px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
          font-style: italic;
          font-size: 14px;       
        }
      
      
        .listings .item {
          display: block;
          border-bottom: 1px solid #eee;
          padding: 20px;
          text-decoration: none;                               
        }

                                 
        .listings .item:last-child {
          border-bottom: 1px solid #eee;
        }
        

        .listings .item .title {
          display: block;
          color: #00853e;
          font-weight: 700;
          font-style: normal;
          font-size: 16px;                  
        }
                  
    
        .listings .item .title small {font-weight: 400;}
      
      
        .listings .item.active .title,
        .listings .item .title:hover {
          color: #8cc63f;              
        }
       
      
        .listings .item.active {background-color: #f5f5f5;} 
      
      
        ::-webkit-scrollbar {
          width: 3px;
          height: 3px;
          border-left: 0;
          background: rgba(0, 0, 0, 0.1);
        }

        ::-webkit-scrollbar-track {background: none;} 

        ::-webkit-scrollbar-thumb {
          background: #00853e;
          border-radius: 0;
        }


        .clearfix { display: block; }

        .clearfix::after {
          content: '.';
          display: block;
          height: 0;
          clear: both;
          visibility: hidden;
        }
           

        .map-overlay {
          top: 0;
          height: 100px;
          margin-top: 60px;
          margin-right: 10px;
          width: 240px;
        }


        .map-overlay > * {
          position: absolute;
          top: 60px;
          right: 23.111111%;
          background: rgba(255, 255, 255, 0.8);
          margin-right: 0px;
          font-family: Arial, sans-serif;
          overflow: auto;
          border-radius: 3px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
        }


        #features {
          top: 0;
          height: 100px;
          margin-top: 60px;
          margin-right: 10px;
          width: 240px;
        }


        .map-overlay-img {
          top: 0;
          height: 0px;
          margin-top: 0px;
          margin-right: 0px;
          width: 240px;
        }


        .map-overlay-img > * {
          position: absolute;
          top: 10px;
          left: 50px;
          z-index:1;
          background: rgba(255, 255, 255, 0.8);
          margin-right: 0px;
          font-family: Arial, sans-serif;
          overflow: auto;
          border-radius: 7px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
          border: 5px solid white;
          object-fit: cover;
          text-align:center;
          font-size:14px;
          color:#808080;   
        }


        #features-img {
          top: 0;
          height: 0px;
          margin-top: 0px;
          margin-right: 0px;
          width: 240px;
        }


        /*.map-overlay1 {
          position: absolute;
          bottom: 210px;
          right: 22.333333%;
          background: rgba(255, 255, 255, 0.8);
          margin-right: 20px;
          font-family: Arial, sans-serif;
          overflow: auto;
          border-radius: 3px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
        }

        @media screen and (max-width: 800px) {
        .map-overlay1 {display: none;}
        }*/


        .map-overlay2 {
          position: absolute;
          bottom: 0;
          right: 22.333333%;
          background: rgba(255, 255, 255, 0.8);
          margin-right: 20px;
          font-family: Arial, sans-serif;
          overflow: auto;
          border-radius: 3px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
        }

        @media screen and (max-width: 800px) {
        .map-overlay2 {display: none;}
        }


        #legend {
          padding: 10px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
          line-height: 18px;
          height: 220px;
          margin-bottom: 30px;
          margin-right: 10px;
          width: 190px;
        }

        /*#legend1 {
          padding: 10px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
          line-height: 18px;
          height: 100px;
          margin-bottom: 50px;
          margin-right: 10px;
          width: 190px;
        }*/


        .legend-key {
          display: inline-block;
          border-radius: 20%;
          width: 10px;
          height: 10px;
          margin-right: 5px;
          opacity: 0.6;
        }


        .calculation-box {
          height: 1p0x;
          width: 105px;
          position: absolute;
          top: 170px;
          right: 10px;
          background-color: rgba(255, 255, 255, .8);
          padding: 5px;
          font-family: Arial, sans-serif;
          border-radius: 3px;
          line-height: 6px;
        }


        .calculation-box1 {
          height: 1p0x;
          width: 105px;
          position: absolute;
          top: 170px;
          right: 135px;
          background-color: rgba(255, 255, 255, .8);
          padding: 5px;
          font-family: Arial, sans-serif;
          border-radius: 3px;
          line-height: 6px;
        }

      </style>
    </head>

    <body>

      <style type='text/css'>
        #info {
          display: inline-block;
          position: absolute;
          bottom: 0px;
          left: 28%;
          transform: translate(-50%, -50%);
          margin: 0px auto;
          width: 28%;
          padding: 2px;
          border: none;
          border-radius: 3px;
          font-size: 12px;
          text-align: center;
          color: #fff;
          background-color: rgba(0, 0, 0, 0.5);
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
        }
      </style>


      <style>

        .legend label,
        .legend span {
          display:block;
          float:left;
          height:15px;
          width:70%;
          text-align:Left;
          font-size:7px;
          color:#808080;
        }
          
        /*.legend1 label,
        .legend1 span {
          display:block;
          float:left;
          height:18px;
          width:88%;
          text-align:Left;
          font-size:16px;
          color:#000000;
        }*/
     
        .mapboxgl-popup-content {
          max-width: 400px;
          font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
          padding: 0;
        }              

        .mapboxgl-popup-content h3 {
          background: #91c949;
          color: #fff;
          margin: 0;
          display: block;
          padding: 10px;
          border-radius: 3px 3px 0 0;
          font-weight: 700;
          margin-top: 0px;
        }

        .mapboxgl-popup-content h4 {
          margin: 0;
          display: block;
          padding-bottom: 10px;
          padding-right: auto;
          padding-left: auto;
          font-weight: 400;
        }

        .mapboxgl-popup-content div {
          padding: 10px;
        }

        .mapboxgl-popup-anchor-top > .mapboxgl-popup-content {
          margin-top: 15px;
        }

        .mapboxgl-popup-anchor-top > .mapboxgl-popup-tip {
          border-bottom-color: #91c949;
        }
     

        .distance-container {
          position: absolute;
          top: 181px;
          left: 42px;
          z-index: 1;          
        }
      
        .distance-container > * {
          background-color: rgba(0, 0, 0, 0.5);
          color: #fff;
          font-size: 11px;
          line-height: 18px;
          display: block;
          margin: 0;
          padding: 5px 10px;
          border-radius: 3px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);      
        }

        .distance-container1 {
          position: absolute;
          top: 151px;
          left: 42px;
          z-index: 1;
        }
      
        .distance-container1 > * {
          background-color: rgba(0, 0, 0, 0.5);
          color: #fff;
          font-size: 11px;
          line-height: 18px;
          display: block;
          margin: 0;
          padding: 5px 10px;
          border-radius: 3px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);    
        }

        .distance-container2 {
          position: absolute;
          top: 121px;
          left: 42px;
          z-index: 1;
        }

        .distance-container2 > * {
          background-color: rgba(0, 0, 0, 0.5);
          color: #fff;
          font-size: 11px;
          line-height: 18px;
          display: block;
          margin: 0;
          padding: 5px 10px;
          border-radius: 3px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);    
        }
      
        #fit {
          display: block;
          position: absolute;
          top: 332px;
          left: 10px;
          background-color: white;
          color: black;
          padding: 7px;
          font-size: 14px;
          border: none;
          cursor: pointer;
          border-radius: 6px;
          border: 1px solid rgba(204, 201, 201, 0.911);
          box-shadow: 0px 2px 2px 0px rgba(0,0,0,0.2);
        }

      
        #dragableMarker {
          display: block;
          position: absolute;
          top: 380px;
          left: 10px;
          background-color: white;
          color: black;
          padding: 10px;
          font-size: 14px;
          border: none;
          cursor: pointer;
          border-radius: 6px;
          border: 1px solid rgba(204, 201, 201, 0.911);
          box-shadow: 0px 2px 2px 0px rgba(0,0,0,0.2);    
        }


        #jump {
          display: block;
          position: absolute;
          top: 425px;
          left: 10px;
          background-color: white;
          color: black;
          padding: 10px;
          font-size: 12px;
          border: none;
          cursor: pointer;
          border-radius: 6px;
          border: 1px solid rgba(204, 201, 201, 0.911);
          box-shadow: 0px 2px 2px 0px rgba(0,0,0,0.2);   
        }

        #stop {
          display: block;
          position: absolute;
          top: 470px;
          left: 10px;
          background-color: white;
          color: black;
          padding: 10px;
          font-size: 12px;
          border: none;
          cursor: pointer;
          border-radius: 6px;
          border: 1px solid rgba(204, 201, 201, 0.911);
          box-shadow: 0px 2px 2px 0px rgba(0,0,0,0.2);
        }

        #custom {
          display: block;
          position: absolute;
          top: 515px;
          left: 10px;
          background-color: white;
          color: black;
          padding: 10px;
          font-size: 12px;
          border: none;
          cursor: pointer;
          border-radius: 6px;
          border: 1px solid rgba(204, 201, 201, 0.911);
          box-shadow: 0px 2px 2px 0px rgba(0,0,0,0.2);
        }

        .coordinates {
          background: rgba(0,0,0,0.5);
          color: #fff;
          position: absolute;
          top: 333px;
          left: 43px;
          padding:5px 10px;
          margin: 0;
          font-size: 11px;
          line-height: 18px;
          border-radius: 3px;
          display: none;
        }
    

        .btn {
          background-color: DodgerBlue;
          border: none;
          color: white;
          padding: 12px 16px;
          font-size: 14px;
          cursor: pointer;
        }


        /* Darker background on mouse-over */
        #fit:hover:hover {
         background-color: rgb(243, 243, 243);
        }

        .dropbtn {
          display: block;
          position: absolute;
          top: 273px;
          left: 10px;
          background-color: white;
          color: black;
          padding: 8px;
          font-size: 14px;
          border: none;
          cursor: pointer;
          border-radius: 6px;
          border: 1px solid rgba(204, 201, 201, 0.911);
          box-shadow: 0px 2px 2px 0px rgba(0,0,0,0.2);
        }
      

        .dropbtn:hover, .dropbtn:focus {background-color: rgb(243, 243, 243);}
      

        .dropdown {
          position: relative;
          display: inline-block; 
        }
      

        .dropdown-content {
          display: none;
          position: absolute;
          background-color: #f1f1f1;
          min-width: 100px;
          overflow: auto;
          border-radius: 6px;
          border: 1px solid rgba(204, 201, 201, 0.911);
          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
          z-index: 1;
          top: 274px;
          left: 41px;
          font-family: 'Open Sans', sans-serif;
          font-size: 12px;
        }
      

        .dropdown-content a {
          color: black;
          padding: 3px 12px;
          text-decoration: none;
          display: block;
          border-bottom: 1px solid rgba(0,0,0,0.25);
        }


        .dropdown-content a.active {
          background-color: #38be86;
          color: #ffffff;
        }
      
        .dropdown a:hover {background-color: #ddd;}


        .dropdown a.active:hover {background: DodgerBlue;}


        .show {display: block;}

      
        .geocoder {
          position:absolute;
          z-index:1;
          
          right:23.111111%;
          margin-left:-25%;
          top:10px;
        }


        .mapboxgl-ctrl-geocoder { min-width:100%; }

        @media screen and (max-width: 800px) {
          .geocoder {  
            position:absolute;
            z-index:1;
            right:10px;
            margin-left:-2%;
            top:10px;
          }


          .mapboxgl-ctrl-geocoder { max-width:5%; }

          .mapboxgl-ctrl-fullscreen {display: none;}
        }

        #geocoder-container > div {
          min-width:50%;
          margin-left:25%;
        }
    
        .sembunyikan{
          position: absolute;
          width: 20%;
          top: 10px;
          left: -500px;
          padding: auto;
          z-index: 1;
        }
    
        .map-overlay-play-container{
          position: absolute;
          width: 20%;
          top: 10px;
          left: 45px;
          padding: auto;
          z-index: 1;
        }
   

        .map-overlay-play{
          font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
          background-color: #fff;
          border-radius: 3px;
          padding: 10px;
          box-shadow:0 1px 2px rgba(0,0,0,0.20);
        }

    
    
        .map-overlay-play h2,
        .map-overlay-play p {
          margin: 0 0 10px;
        }

        @media screen and (max-width: 800px) {
        .map-overlay-play-container{display: none;}
        }

        .marker {
          background-image: url('mapbox-icon.png');
          background-size: cover;
          width: 50px;
          height: 50px;
          border-radius: 50%;
          cursor: pointer;
        }

        .mapboxgl-popup {
            max-width: 200px;
          }

          .mapboxgl-popup-content {
            text-align: center;
            font-family: 'Open Sans', sans-serif;
          }
      </style>

      <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.js'></script>
      <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.css' type='text/css' />    
      <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.min.js'></script>
      <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.css' type='text/css' />
      <script src='https://api.tiles.mapbox.com/mapbox.js/plugins/turf/v3.0.11/turf.min.js'></script>
      <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.js'></script>
      <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css' type='text/css'/>
          <!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
      <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
      

      <div class='map-overlay-play-container'>
        <div class='map-overlay-play'>
          <h2>Peta Customer ICON+ Banjarbaru</h2>        
          <small><em>GIS Map credit: <a target='_blank' href=''>Renaldi Haris Aksara</a></em></small> <br>
          <script type="text/javascript">        
                                                  function tampilkanwaktu(){         //fungsi ini akan dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik    
                                                  var waktu = new Date();            //membuat object date berdasarkan waktu saat 
                                                  var sh = waktu.getHours() + "";    //memunculkan nilai jam, //tambahan script + "" supaya variable sh bertipe string sehingga bisa dihitung panjangnya : sh.length    //ambil nilai menit
                                                  var sm = waktu.getMinutes() + "";  //memunculkan nilai detik    
                                                  var ss = waktu.getSeconds() + "";  //memunculkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
                                                  document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
                                                  }
                                              </script>                                            <body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">        
                                              <span id="clock"></span>
                                              <?php
                                              $hari = date('l');
                                              /*$new = date('l, F d, Y', strtotime($Today));*/
                                              if ($hari=="Sunday") {
                                               echo "Minggu";
                                              }elseif ($hari=="Monday") {
                                               echo "Senin";
                                              }elseif ($hari=="Tuesday") {
                                               echo "Selasa";
                                              }elseif ($hari=="Wednesday") {
                                               echo "Rabu";
                                              }elseif ($hari=="Thursday") {
                                               echo("Kamis");
                                              }elseif ($hari=="Friday") {
                                               echo "Jum'at";
                                              }elseif ($hari=="Saturday") {
                                               echo "Sabtu";
                                              }
                                              ?>,
                                              <?php
                                              $tgl =date('d');
                                              echo $tgl;
                                              $bulan =date('F');
                                              if ($bulan=="January") {
                                               echo " Januari ";
                                              }elseif ($bulan=="February") {
                                               echo " Februari ";
                                              }elseif ($bulan=="March") {
                                               echo " Maret ";
                                              }elseif ($bulan=="April") {
                                               echo " April ";
                                              }elseif ($bulan=="May") {
                                               echo " Mei ";
                                              }elseif ($bulan=="June") {
                                               echo " Juni ";
                                              }elseif ($bulan=="July") {
                                               echo " Juli ";
                                              }elseif ($bulan=="August") {
                                               echo " Agustus ";
                                              }elseif ($bulan=="September") {
                                               echo " September ";
                                              }elseif ($bulan=="October") {
                                               echo " Oktober ";
                                              }elseif ($bulan=="November") {
                                               echo " November ";
                                              }elseif ($bulan=="December") {
                                               echo " Desember ";
                                              }
                                              $tahun=date('Y');
                                              echo $tahun;
                                              ?>
        </div>
      </div>
      <div id='geocoder' class='geocoder'><a href="../index.php?halaman=index&now=<?php echo $now = date('Y-m-d') ?>" class="btn btn-danger">Dahsboard</a></div>
      <div class='sidebar pad2'></div>
      <div id='map' class='map pad2'></div>

      <div class='sidebar'>
        <div class='heading'><h1><strong>Lokasi Customer</strong></h1></div>
        <div id='listings' class='listings'></div>
      </div>

      <button id='fit' class="btn"><i class="fa fa-home"></i></button>
      <button id='dragableMarker' onclick="myFunction()" class="btn"><i class="fa fa-map-marker-alt"></i></button>
      <button id='jump' onclick="myFunction()" class="btn"><i class="fa fa-play"></i></button>
      <button id='stop' onclick="myFunction()" class="btn"><i class="fa fa-stop"></i></button>
      <button id='custom' onclick="myFunction()" class="btn"><i class="fa fa-comment-dollar"></i></button>

      <div class="dropdown">
        <button onclick="myFunction()" class="dropbtn"><i class="fa fa-layer-group"></i></button>
        <div id="myDropdown" class="dropdown-content"></div>
      </div>

      <pre id='info'></pre>
      <pre id='coordinates' class='coordinates'></pre>
      <div id='distance' class='distance-container'></div>
      <div id='distance1' class='distance-container1'></div>
      <div id='customer' distance1='distance-container2'></div>

      <!--<div class='map-overlay1' id='legend1'><strong>SIMBOL</strong>
        <nav class='legend1 clearfix'>
          <span style="background-image: url(https://res.cloudinary.com/mrdj73cool/image/upload/v1539840737/DJ/star-15-2.gif); height: 17px; width: 18px; border: none"></span><label>Daerah Customer</label>
        </nav>
      </div>-->

      <div class='map-overlay' id='feature'><div id='pd'></div></div>
  <!--    <div class='map-overlay2' id='legend'><strong>KECAMATAN</strong></div>-->
      <div class='map-overlay-img' id='features-img'><div id='pd-img'></div></div>

      <div class='sembunyikan'>
          <h2 id='location-title' class='hiddene'></h2>
          <p id='location-description'></p>
      </div>
       

      <script>
        var saved_markers = <?= get_saved_locations() ?>;
        mapboxgl.accessToken = 'pk.eyJ1IjoicmVpbmRoYXJkIiwiYSI6ImNrMXVmY2psMzEwdmEzY25zcGZ0ZnA0NW0ifQ.NbmkWLWlWjfSvUwXLGx1MQ';

        // Set bounds to Aceh
       /* var bounds = [
          [94.3727, 1.7546], // Southwest coordinates
          [102.9798, 6.8691]  // Northeast coordinates
        ];*/

        if (!mapboxgl.supported()) {
          alert('Browser Anda tidak support untuk Web ini');
        } 
        else {
          var map = new mapboxgl.Map({

          container: 'map',
          style: 'mapbox://styles/reindhard/ck55o4ikw05ap1cmfvc9i0ase',
          center: [114.81727664299137, -3.4464967261525032],
          zoom: 9.41,
          //maxBounds: bounds // Sets bounds as max
          });
        }

  /*      map.addControl(new MapboxDirections({
  accessToken: mapboxgl.accessToken
  }), 'bottom-left');*/

        var title = document.getElementById('location-title');
        var description = document.getElementById('location-description');
        //ini ya? datanya di locations? iya inikan masih cuma geojson
        //servernya buat nampilin geojson sdah dapat? server map nya? mana querymu yang buat nampilkan data lokasi ?

        var locations = <?php get_location_coordinate() ?>;


        function playback(index) {
          title.textContent = locations[index].title;
          description.textContent = locations[index].description;

          // Animate the map position based on camera properties
          map.flyTo(locations[index].camera);

          map.once('moveend', function() {
            // Duration the slide is on screen after interaction
            window.setTimeout(function() {
              // Increment index
              index = (index + 1 === locations.length) ? 0 : index + 1;
              playback(index);
            }, 9000); // After callback, show the location for 9 seconds.
          });
        }


        function stop(index) {
          window.location.reload(index);
        }
         
        
        // Display the last title/description first
        title.textContent = locations[locations.length - 1].title;
        description.textContent = locations[locations.length - 1].description;

        // playLocations
        document.getElementById('jump').addEventListener('click', function() {
    
          // Start the playback animation for each borough
          playback(0);
    
        });

        // StopLocations
        document.getElementById('stop').addEventListener('click', function() {
    
        // Stop the playback animation for each borough
         stop();
        
         });

        document.getElementById('custom').addEventListener('click', function(){
          add_markers();
        });


        /* given a query in the form "lng, lat" or "lat, lng" returns the matching
         * geographic coordinate(s) as search results in carmen geojson format,
         * https://github.com/mapbox/carmen/blob/master/carmen-geojson.md
         */
        var coordinatesGeocoder = function (query) {
          // match anything which looks like a decimal degrees coordinate pair
          var matches = query.match(/^[ ]*(?:Lat: )?(-?\d+\.?\d*)[, ]+(?:Lng: )?(-?\d+\.?\d*)[ ]*$/i);
            if (!matches) {
            return null;
          }

          function coordinateFeature(lng, lat) {
            return {
              center: [lng, lat],
              geometry: {
                  type: "Point",
                  coordinates: [lng, lat]
              },
              place_name: 'Lat: ' + lat + ', Lng: ' + lng, // eslint-disable-line camelcase
              place_type: ['coordinate'], // eslint-disable-line camelcase
              properties: {},
              type: 'Feature'
            };
          }

          var coord1 = Number(matches[1]);
          var coord2 = Number(matches[2]);
          var geocodes = [];

          if (coord1 < -90 || coord1 > 90) {
            // must be lng, lat
            geocodes.push(coordinateFeature(coord1, coord2));
          }

          if (coord2 < -90 || coord2 > 90) {
            // must be lat, lng
            geocodes.push(coordinateFeature(coord2, coord1));
          }

          if (geocodes.length === 0) {
            // else could be either lng, lat or lat, lng
            geocodes.push(coordinateFeature(coord1, coord2));
            geocodes.push(coordinateFeature(coord2, coord1));
          }

          return geocodes;
        };
        //oke. leat atribut databasemu apa aj
        var data = '<?php get_location_sidebar()?>';
        var stores = JSON.parse(data);

        //ini include jquery gak? hadang gak genah nah... jquery yang mana kak?
        //klo gak ad jquery, tak embed php disini aj lah? gak usah pakai ajax brati. tinggal masukin kodingan php disinibya



        // This adds the data to the map
        map.on('load', function (e) {
          // Add the data to your map as a layer
          map.addLayer({
            "id": "locations",
            "type": "symbol",
            // Add a GeoJSON source containing place coordinates and information.
            "source": {
              "type": "geojson",
              "data": stores
            },
            "layout": {
              "icon-image": "star-15",
              "icon-allow-overlap": true,
              "cursor": "pointer",
            }
          });
        
          // Initialize the list
          buildLocationList(stores);
        });


        map.on('click', function(e) {
          var features = map.queryRenderedFeatures(e.point, {
          layers: ['locations']
          });

          if (features.length) {
            var clickedPoint = features[0];
            // 1. Fly to the point
            flyToStore(clickedPoint);

            // 2. Close all other popups and display popup for clicked store
            createPopUp(clickedPoint);

            // 3. Highlight listing in sidebar (and remove highlight for all other listings)
            var activeItem = document.getElementsByClassName('active');
            if (activeItem[0]) {
              activeItem[0].classList.remove('active');
            }

            var selectedFeature = clickedPoint.properties.NAMA;
        
            for (var i = 0; i < stores.features.length; i++ ) {
              if (stores.features[i].properties.NAMA === selectedFeature) {
               selectedFeatureIndex = i;
              }
            }

            var listing = document.getElementById('listing-' + selectedFeatureIndex);
            listing.classList.add('active');

          }
        });


        function flyToStore(currentFeature) {
          map.flyTo({
            center: currentFeature.geometry.coordinates,
            zoom: 15
          });
        }


        function createPopUp(currentFeature) {
          var popUps = document.getElementsByClassName('mapboxgl-popup');
          if (popUps[0]) popUps[0].remove();

          var popup = new mapboxgl.Popup({closeOnClick: true})
          .setLngLat(currentFeature.geometry.coordinates)
          .setHTML('<h3>Daerah Customer</h3>' + '<h4>' + currentFeature.properties.NAMA + '</h4>')
          .addTo(map);
        }

        function buildLocationList(data) {
          for (i = 0; i < data.features.length; i++) {
            // Create an array of all the stores and their properties
            var currentFeature = data.features[i];
            // Shorten data.feature.properties to just `prop` so we're not
            // writing this long form over and over again.
            var prop = currentFeature.properties;
            // Select the listing container in the HTML
            var listings = document.getElementById('listings');
            var filterEl = document.getElementById('feature-filter');
            // Append a div with the class 'item' for each store 
            var listing = listings.appendChild(document.createElement('div'));
            listing.className = 'item';
            listing.id = "listing-" + i;

            // Create a new link with the class 'title' for each store 
            // and fill it with the store address
            var link = listing.appendChild(document.createElement('a'));
            link.href = '#';
            link.className = 'title';
            link.dataPosition = i;
            link.innerHTML = prop.NAMA;

            // Create a new div with the class 'details' for each store 
            // and fill it with the city and phone number
            var details = listing.appendChild(document.createElement('div'));
            details.innerHTML = 'PIC: ' + prop.PIC ;
            details.innerHTML += ' <br> Jabatan: ' + prop.Jabatan;
            details.innerHTML += ' <br> CP: ' + prop.CP;
          
            link.addEventListener('click', function(e) {
              // Update the currentFeature to the store associated with the clicked link
              var clickedListing = data.features[this.dataPosition];

              // 1. Fly to the point associated with the clicked link
              flyToStore(clickedListing);

              // 2. Close all other popups and display popup for clicked store
              createPopUp(clickedListing);

              // 3. Highlight listing in sidebar (and remove highlight for all other listings)
              var activeItem = document.getElementsByClassName('active');

              if (activeItem[0]) {
                activeItem[0].classList.remove('active');
              }
          
              this.parentNode.classList.add('active');
            });
   
          }
        }

        /*map.on('load', function (e) {
            map.addLayer({
                'id': 'batasbalangan',
                'type': 'polygon',
                'source': {
                    'type': 'JSON',
                    'data': '/assets/GeoJson/KALSELJSON/kecamatanBalangan.JSON'
                },
                'layout': {},
                'paint': {
                    'fill-color': '#f08',
                    'fill-opacity': 1
                }
            // This is the important part of this example: the addLayer
            // method takes 2 arguments: the layer as an object, and a string
            // representing another layer's name. if the other layer
            // exists in the stylesheet already, the new layer will be positioned
            // right before that layer in the stack, making it possible to put
            // 'overlays' anywhere in the layer stack.
            });
        });*/


        function forwardGeocoder(query) {
          var matchingFeatures = [];
          for (var i = 0; i < stores.features.length; i++) {
            var feature = stores.features[i];
            // handle queries with different capitalization than the source data by calling toLowerCase()
            if (feature.properties.NAMA.toLowerCase().search(query.toLowerCase()) !== -1) {
              // add a tree emoji as a prefix for custom data results
              // using carmen geojson format: https://github.com/mapbox/carmen/blob/master/carmen-geojson.md
              feature['place_name'] = feature.properties.NAMA;
              feature['center'] = feature.geometry.coordinates;
              feature['place_type'] = ['D.I.'];
              matchingFeatures.push(feature);
            }
          }
           return matchingFeatures;
        }

      // file gejson mu mana?
      // itu titik kordinat apa yang di geejson?
      // Wilayah nya per kabupaten (contoh geojson kab. tanah laut, geojson hulusungai)
      // bingung nya ulun enakan javascrip apa pakai php dipanggil gitu? kalo bisa geojsonnya dipanggil dari database ntar misalkan mau update geojsonnya atau menambahkan bisa tinggal input data geojson nya di database dan auto masuk ke map
      // itu data statis kan? dan bakal jarang di ganti?
      // oiya jarang tapi ada kemungkinan bisa menambahkan,
      // sek bentar, tadi kmu export nya dai mana geojson tdi? download dari data geospasial

        var geocoder = new MapboxGeocoder({
          accessToken: mapboxgl.accessToken,
          localGeocoder: forwardGeocoder,
          //localGeocoder: coordinatesGeocoder,
          zoom: 15,
          country: 'id',
          placeholder: "Masukkan Nama Customer"
        });

         var marker ;

         map.on('load', function() {
              add_markers(saved_markers);
          });

         function add_markers(coordinates) {

              var geojson = (saved_markers == coordinates ? saved_markers : '');

            console.log(geojson);
            // add markers to map
            geojson.forEach(function (marker) {
                console.log(marker);
                // make a marker for each feature and add to the map
                new mapboxgl.Marker()
                    .setLngLat(marker)
                    .addTo(map);
              });

          }

        document.getElementById('geocoder').appendChild(geocoder.onAdd(map));

        // Add zoom and rotation controls to the map.
        var nav = new mapboxgl.NavigationControl();
        map.addControl(nav, 'top-left');

       
        // Add full screen control to the mam
        var fullscreen = new mapboxgl.FullscreenControl();
        map.addControl(fullscreen, 'top-left');


        var scale = new mapboxgl.ScaleControl({
          maxWidth: 80,
          unit: 'imperial'
        });

        map.addControl(scale);
        scale.setUnit('metric');


        var geolocate = new mapboxgl.GeolocateControl({
          positionOptions: {
            enableHighAccuracy: true
          },
          trackUserLocation: true
        });

        map.addControl(geolocate, 'top-left');

        /*(function() {
          var proxied = geolocate._updateCamera;
          geolocate._updateCamera = function() {
            // get geolocation
            var location = new mapboxgl.LngLat(arguments[0].coords.longitude, arguments[0].coords.latitude);

            var bounds = map.getMaxBounds();

            if (bounds) {
              // if geolocation is within maxBounds
              if (location.longitude >= bounds.getWest() && location.longitude <= bounds.getEast() &&
                  location.latitude >= bounds.getSouth && location.latitude <= bounds.getNorth) {
                    return proxied.apply( this, arguments );
                  } else {
                      console.log('Lokasi Anda berada di luar jangkauan setting Peta ini, matikan tombol ini!');
                    return null;
                  }
            }
            return proxied.apply( this, arguments );
          };
        })();*/


        var draw = new MapboxDraw({
          displayControlsDefault: false,
            controls: {
            line_string: true,
            polygon: true,
            trash: true
          }
        });

        map.addControl(draw, 'top-left');

        map.on('draw.create', updateArea);
        map.on('draw.delete', updateArea);
        map.on('draw.update', updateArea);

        map.on('draw.create', updateDist);
        map.on('draw.delete', updateDist);
        map.on('draw.update', updateDist);


        function updateArea(e) {
          var data = draw.getAll();
          var answer = document.getElementById('distance');
          //var distanceContainer = document.getElementById('distance');
          if (data.features.length > 0) {
            var area = turf.area(data);
            // restrict to area to 2 decimal points
            var rounded_area = Math.round(area*100)/100;
            answer.innerHTML = '<p><em><strong>Luas Area: </strong>' + rounded_area + ' meter persegi</em></p>';
          } else {
            answer.innerHTML = '';
            if (e.type !== 'draw.delete') alert("Use the draw tools to draw a polygon!");
          }
        }


        function updateDist(e) {
          var data = draw.getAll();
          var distanceContainer = document.getElementById('distance1');
          if (data.features.length > 0) {
            var dist = turf.lineDistance(data);
            // restrict to area to 2 decimal points
            var jarak = Math.round(dist*1000);
            distanceContainer.innerHTML = '<p><em><strong>Total Jarak: </strong>' + jarak + ' meter</em></p>';
          } else {
            distanceContainer.innerHTML = '';
            if (e.type !== 'draw.delete') alert("Use the draw tools to draw");
          }
        }


        map.on('mousemove', function (e) {
          document.getElementById('info').innerHTML =
          // e.point is the x, y coordinates of the mousemove event relative
          // to the top-left corner of the map
          //JSON.stringify(e.point) + '<br />' +
          // e.lngLat is the longitude, latitude geographical position of the event
          JSON.stringify(e.lngLat);
        });

  //////////////////////////////////////////////
        // When a click event occurs on a feature in the states layer, open a popup at the
        // location of the click, with description HTML from its properties.
        map.on('click', 'Photo Survey', function (e) {
          new mapboxgl.Popup()
              .setLngLat(e.lngLat)
              .setHTML('<h3>Photo Survey ' + e.features[0].properties.NAMA + '<br>Keterangan: ' + e.features[0].properties.KETERANG + '<br>Klik <a class="example-image-link" href=\"' + e.features[0].properties.image + '\" data-lightbox="example-1" data-title="Photo by STAR Team Survey"><u><strong>di sini</strong></u></a> untuk melihat photo ukuran penuh</em></p>')
              .addTo(map);    
        });


        // Change the cursor to a pointer when the mouse is over the states layer.
        map.on('mouseenter', 'Photo Survey', function () {
          map.getCanvas().style.cursor = 'pointer';
        });


        // Change it back to a pointer when it leaves.
        map.on('mouseleave', 'Photo Survey', function () {
          map.getCanvas().style.cursor = '';
        });

        //---------------------------------------------------
      
        map.on('mousemove', function(e) {
          var states = map.queryRenderedFeatures(e.point, {
            layers: ['Photo Survey']
          });

          if (states.length > 0) {
            document.getElementById('pd-img').innerHTML = '<img src=\"' + states[0].properties.image + '\"></img><br><sup><em>Klik Simbol <i class="fa fa-camera"></i> untuk informasi</em></sup>';
    
          } else {
            document.getElementById('pd-img').innerHTML = '';
          }
   
        });
  //////////////////////////

        // Create a popup, but don't add it to the map yet.
        var popup = new mapboxgl.Popup({
          closeButton: false,
          closeOnClick: false
        });


        /*map.on('mouseenter', 'saluran-irigasi-highlighted', function(e) {
          // Change the cursor style as a UI indicator.
          //DI BM Point
          map.getCanvas().style.cursor = 'pointer';
          popup.setLngLat(e.lngLat)
          .setHTML('<h3>' + e.features[0].properties.NAME + '</h3><p><em><strong>Kondisi: </strong>'+ e.features[0].properties.KONDISI + '<br><strong>Jenis Saluran: </strong>'+ e.features[0].properties.JENIS_SALURAN + '<br><strong>Panjang: </strong>' + e.features[0].properties.PANJANG_M + ' meter</em></p>')
          .addTo(map);
        });


        map.on('mouseleave', 'saluran-irigasi-highlighted', function() {
          map.getCanvas().style.cursor = 'pointer';
          popup.remove();
        });*/

        //---------------------------------------------------------------------------------------------------------------------------------

        // When a click event occurs on a feature in the states layer, open a popup at the
        // location of the click, with description HTML from its properties.
        // map.on('click', 'saluran-irigasi', function (e) {
        // new mapboxgl.Popup()
        // .setLngLat(e.lngLat)
        // .setHTML('<h3>' + e.features[0].properties.KONDISI + '</h3><p>' + e.features[0].properties.PANJANG_M + ' meter</p>')
        // .addTo(map);   
        // });


        // Change the cursor to a pointer when the mouse is over the states layer.
        // map.on('mouseenter', 'saluran-irigasi', function () {
        // map.getCanvas().style.cursor = 'pointer';
        // });

        // Change it back to a pointer when it leaves.
        // map.on('mouseleave', 'saluran-irigasi', function () {
        // map.getCanvas().style.cursor = '';
        // popup.remove();
        // });

        //------------------------------------------------------------------------------------------------------------------------------------
  //gak ad yang manggil... sek tunggu
        // Create a popup, but don't add it to the map yet.
        var popup = new mapboxgl.Popup({
          closeButton: false,
          closeOnClick: false
        });

        map.on('mouseenter', 'di-bm', function(e) {
          // Change the cursor style as a UI indicator.
          //DI BM Point
          map.getCanvas().style.cursor = 'pointer';

          var coordinates = e.features[0].geometry.coordinates.slice();
          var description = e.features[0].properties.NAMA;

          // Ensure that if the map is zoomed out such that multiple
          // copies of the feature are visible, the popup appears
          // over the copy being pointed to.
          while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
          }

          // Populate the popup and set its coordinates
          // based on the feature found.
          popup.setLngLat(coordinates)
              .setHTML('<h4>' + description + '</h4>')
              .addTo(map);
        });

        map.on('mouseleave', 'di-bm', function() {
          map.getCanvas().style.cursor = '';
          popup.remove();
        });


        // var layers = ['Kab. Hulu Sungai Selatan', 'Kab. Tapin', 'Kab. Tabalong', 'Kab. Balangan', 'Kab. Tanah Bumbu', 'Kab. Kota Baru', 'Kab. Banjar', 'Kota Banjarmasin', 'Kab. Hulu Sungai Tengah'];
        // var colors = ['#1b832b', '#765cc7', '#5cc7b9', '#e133de', '#406cc4', '#c7b25c', '#135350', '#790c19', '#c75c5c'];

        // for (i = 0; i < layers.length; i++) {
        //   var layer = layers[i];
        //   var color = colors[i];
        //   var item = document.createElement('div');
        //   var key = document.createElement('span');
        //   key.className = 'legend-key';
        //   key.style.backgroundColor = color;

        //   var value = document.createElement('span');
        //   value.innerHTML = layer;
        //   item.appendChild(key);
        //   item.appendChild(value);
        //   legend.appendChild(item);
        // }


        map.on('mousemove', function(e) {
          var states = map.queryRenderedFeatures(e.point, {
            layers: ['kalimantanlengkap-16wfzk']
          });

          if (states.length > 0) {
            document.getElementById('pd').innerHTML = '<p><strong><em>' + 'Wilayah ' + states[0].properties.DESA + '<br>' + 'Kec. ' + states[0].properties.KECAMATAN + '<br>' + 'Kab. ' + states[0].properties.KABKOT + '<br>' + 'Prov. ' + states[0].properties.PROVINSI + '</strong></em></p>'; 
          } else {
            document.getElementById('pd').innerHTML = '';
          }
        });

      /*  map.on('mousemove', function(e) {
          var states = map.queryRenderedFeatures(e.point, {
            layers: ['kaltim-6n3icf']
          });

          if (states.length > 0) {
            document.getElementById('pd').innerHTML = '<h3><strong>' + states[0].properties.KABKOT + '</strong></h3><p><strong><em>' + states[0].properties.NAMA_PROVINSI + '</strong></em></p>'; 
          } else {
            document.getElementById('pd').innerHTML = '';
          }
        });*/


        // The 'building' layer in the mapbox-streets vector source contains building-height
        // data from OpenStreetMap.
        map.on('load', function() {
          // Insert the layer beneath any symbol layer.
          var layers = map.getStyle().layers;

          var labelLayerId;
          for (var i = 0; i < layers.length; i++) {
            if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
              labelLayerId = layers[i].id;
              break;
            }
          }

          map.addLayer({
            'id': '3d-buildings',
            'source': 'composite',
            'source-layer': 'building',
            'filter': ['==', 'extrude', 'true'],
            'type': 'fill-extrusion',
            'minzoom': 15,
            'paint': {
              'fill-extrusion-color': '#aaa',

              // use an 'interpolate' expression to add a smooth transition effect to the
              // buildings as the user zooms in
              'fill-extrusion-height': [
              "interpolate", ["linear"], ["zoom"],
              15, 0,
              15.05, ["get", "height"]
              ],
              'fill-extrusion-base': [
              "interpolate", ["linear"], ["zoom"],
              15, 0,
              15.05, ["get", "min_height"]
              ],
              'fill-extrusion-opacity': .6
            }
          }, labelLayerId);

          // Listen for the `result` event from the MapboxGeocoder that is triggered when a user
          // makes a selection and add a symbol that matches the result.
          geocoder.on('result', function(ev) {
            map.getSource('single-point').setData(ev.result.geometry);
          });  

        });



        /*map.on('mousemove', function(e) {
          // set bbox as 5px reactangle area around clicked point
          var bbox = [[e.point.x - 5, e.point.y - 5], [e.point.x + 5, e.point.y + 5]];
          var features = map.queryRenderedFeatures(bbox, { layers: ['saluran-irigasi'] });

          // Run through the selected features and set a filter
          // to match features with unique FIPS codes to activate
          // the `counties-highlighted` layer.
          var filter = features.reduce(function(memo, feature) {
              memo.push(feature.properties.PANJANG_M);
              return memo;
          }, ['in', 'PANJANG_M']);

          map.setFilter("saluran-irigasi-highlighted", filter);
        });*/


        // zoom to home
        document.getElementById('fit').addEventListener('click', function() {
          map.fitBounds([[114.8150984,-3.4464479], 
          [114.839287, -2.684064]
          ]);
        });



        // dragable marker
        var coordinates = document.getElementById('coordinates');
        document.getElementById('dragableMarker').addEventListener('click', function() {
          var marker = new mapboxgl.Marker({
            draggable: true
          })
          .setLngLat([97.064169, 4.759977])
          .addTo(map);

          function onDragEnd() {
            var lngLat = marker.getLngLat();
            coordinates.style.display = 'block';
            coordinates.innerHTML = 'Longitude: ' + lngLat.lng + '<br />Latitude: ' + lngLat.lat;
          }

          marker.on('dragend', onDragEnd);
          marker: this
        });


        // show hide layers
        var toggleableLayerIds = [ 'Photo Survey', 'Sungai', 'Kontur-label', 'Kontur-index', 'Kontur', 'Satellite', 'Terrain' ];

        for (var i = 0; i < toggleableLayerIds.length; i++) {
          var id = toggleableLayerIds[i];

          var link = document.createElement('a');
          link.href = '#';
          link.className = 'active';
          link.textContent = id;

          link.onclick = function (e) {
            var clickedLayer = this.textContent;
            e.preventDefault();
            e.stopPropagation();

            var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

            if (visibility === 'visible') {
              map.setLayoutProperty(clickedLayer, 'visibility', 'none');
              this.className = '';
            } else {
              this.className = 'active';
              map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
            }
          };

          var layers = document.getElementById('myDropdown');
          layers.appendChild(link);
        }


        /* When the user clicks on the button, 
        toggle between hiding and showing the dropdown content */
        function myFunction() {
          document.getElementById("myDropdown").classList.toggle("show");
        }
      
        // Close the dropdown if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {
      
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
              }
            }
          }
        }

     
    /*    map.on('load', function() {
        var url = 'kecamatanBanjar.json';
        map.addSource('banjar', { type: 'geojson', data: url});
        map.addLayer({
          'id': 'banjar',
          'type': 'polygon',
          'source': 'banjar',
          'layout': {},
          'paint': {
            'fill-color': '#0000FF',
            'fill-opacity': 0.8
          }
        });
      });*/
  //    map.addControl(new MapboxMarkers(banjarpol));

      </script>
    </body>
  </html>