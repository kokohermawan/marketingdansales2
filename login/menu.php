<aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="../images/icon/logoicon.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <?php 
                            $halaman = $_GET['halaman'];
                            if($halaman=='index'){

                            ?>
                        <li class="active">
                        <?php }else{

                        ?>
                        <li>
                        <?php } ?>
                            <a class="js-arrow" href="index.php?halaman=index&now=<?php echo $now = date('Y-m-d'); ?>">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <?php 
                        if($_SESSION['level']!="Sales" && $_SESSION['level']!="Manager" && $_SESSION['level']!="Admin Sales"){
                        if($halaman=='segment' || $halaman=='jenislayanan' || $halaman=='customerexpec' || $halaman=='cusol'){

                         ?>
                        <li class="active">
                        <?php 
                            }else{
                                echo '<li>';
                            }
                         ?>
                            <a class="js-arrow" href="#">
                                <i class="fa fa-server"></i>Data Master</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <?php 
                                    if($halaman=='segment'){

                                     ?>
                                    <li class="active">
                                    <?php 
                                        }else{
                                            echo '<li>';
                                        }
                                     ?>
                                        <a href="segment_tampil.php?halaman=segment" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Segment Customer</a>
                                    </li>
                                    <?php 
                                    if($halaman=='jenislayanan'){

                                     ?>
                                    <li class="active">
                                    <?php 
                                        }else{
                                            echo '<li>';
                                        }
                                     ?>
                                        <a href="layanan_tampil.php?halaman=jenislayanan" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jenis Layanan</a>
                                    </li>
                                    <?php 
                                    if($halaman=='customerexpec'){

                                     ?>
                                    <li class="active">
                                    <?php 
                                        }else{
                                            echo '<li>';
                                        }
                                     ?> 
                                        <a href="expec_tampil.php?halaman=customerexpec" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer Expectation</a>
                                    </li>
                                    <?php 
                                    if($halaman=='cusol'){

                                     ?>
                                    <li class="active">
                                    <?php 
                                        }else{
                                            echo '<li>';
                                        }
                                     ?> 
                                        <a href="cusol.php?halaman=cusol" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer Solution</a>
                                    </li>
                                </ul>
                        </li>
                        <?php
                        } 
                        if($_SESSION['level']!="Manager"){
                        if ($halaman=='customer' || $halaman=='existing' || $halaman=='potpen') {

                         ?>
                        <li class="active">
                        <?php }else{
                            echo '<li>';
                        } ?>
                            <a class="js-arrow" href="#">
                                <i class="fa fa-list"></i>Initiation</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <?php 
                                    if ($halaman=='customer') {

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo '<li>';
                                    }
                                     ?>
                                        <a href="customer_tampil.php?halaman=customer" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data Customer</a>
                                    </li>
                                    <?php 
                                    if($halaman=='existing'){

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo '<li>';
                                    } ?>
                                        <a href="existing_tampil.php?halaman=existing" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Existing</a>
                                    </li>
                                    <?php
                                    if ($halaman=='potpen') {

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo '<li>';
                                    }
                                     ?>
                                        <a href="potpen_tampil.php?halaman=potpen" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Potensi</a>
                                    </li>
                                </ul>
                        </li>
                        <?php
                        }
                        if($_SESSION['level']!="Manager") {
                        if ($halaman=='canvasing' || $halaman=='product_pres' || $halaman=='solution_design') {

                         ?>
                        <li class="active">
                        <?php }else{
                            echo '<li>';
                        } ?>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-th"></i>Prospecting</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <?php 
                                    if ($halaman=='canvasing') {

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo '<li>';
                                    } ?>
                                        <a href="canvasing_tampil.php?halaman=canvasing" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Canvassing</a>
                                    </li>
                                    <?php 
                                    if ($halaman=='product_pres') {

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo '<li>';
                                    } ?>
                                        <a href="product_pres.php?halaman=product_pres" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Presentation</a>
                                    </li>
                                    <?php 
                                    if ($halaman=='solution_design') {

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo '<li>';
                                    } ?>
                                        <a href="solution_design.php?halaman=solution_design" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solution Design</a>
                                    </li>
                                </ul>
                        </li>
                        <?php 
                        }
                        if($_SESSION['level']!="Manager"){
                        if ($halaman=="quatation" || $halaman=="subm_quotation" || $halaman=="customer_response") {

                         ?>
                        <li class="active">
                        <?php }else{
                            echo '<li>';
                        } ?>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-align-justify"></i>Offering</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <?php 
                                    if ($halaman=="quatation") {

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo '<li>';
                                    } ?>
                                        <a href="quatation.php?halaman=quatation" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Create Quatation</a>
                                    </li>
                                    <?php 
                                    if ($halaman=="subm_quotation") {

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo "<li>";
                                    } ?>
                                        <a href="subm_quotation.php?halaman=subm_quotation" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Submit Quatation</a>
                                    </li>
                                    <?php 
                                    if ($halaman=="customer_response") {

                                     ?>
                                    <li class="active">
                                    <?php }else{
                                        echo '<li>';
                                    } ?>
                                        <a href="customer_response.php?halaman=customer_response" style="font-size: 80%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer Response</a>
                                    </li>
                                </ul>
                        </li>
                        <?php 
                        }
                        if($_SESSION['level']!="Sales" && $_SESSION['level']!="Manager" && $_SESSION['level']!="Admin Sales"){ //pembatasan hak akses
                        if ($halaman=="user") {

                         ?>
                        <li class="active">
                        <?php }else{
                            echo '<li>';
                        } ?>
                            <a class="js-arrow" href="user.php?halaman=user">
                                <i class="fas fa-user"></i>User</a>
                        </li>
                        <?php 
                        } //tutup if menu tidak tampil
                        if($_SESSION['level']!="Sales"){ //pembatasan hak akses
                        if ($halaman=="maps") {

                         ?>
                        <li class="active">
                        <?php }else{
                            echo '<li>';
                        } ?>
                            <a class="js-arrow" href="mapboxCOBAV2CUSTOMFULL/index.php?halaman=maps">
                                <i class="fas fa-map"></i>Maps</a>
                        </li>
                        <?php } //tutup if menu tidak tampil, pembatasan hak akses ?>
                    </ul>
                </nav>
            </div>
        </aside>