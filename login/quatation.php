<?php 
include '../config.php';
include 'fungsi_rupiah.php';
include 'fungsi_tgl.php';

error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Quatation</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php"; ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Quatation</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <?php 
                                                if($_SESSION['level']=="Administrator"){

                                                 ?>
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <?php } ?>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-top-campaign">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Nama Customer</th>
                                                        <th>Jumlah Layanan</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    if($_SESSION['level']=="Sales"){
                                                        $query = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND cust.kd_pengguna='$_SESSION[kd_pengguna]' GROUP BY nm_cust")or die(mysqli_error($db));   
                                                    }else{
                                                        $query = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
                                                    }
                                                        $no=0;
                                                        while ($data = mysqli_fetch_assoc($query)) {
                                                            
                                                            $no++;

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <?php 
                                                        $jumlah = mysqli_query($db,"SELECT * FROM create_quatation WHERE kd_product_pres='$data[kd_product_pres]'");
                                                        $jumlahdata = mysqli_num_rows($jumlah);

                                                         ?>
                                                        <td><?php echo $jumlahdata ?></td>
                                                        <td>
                                                             <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#detail<?php echo $data['kd_product_pres'] ?>">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_product_pres'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <a href="cetakquatation.php?kd_product_pres=<?php echo $data['kd_product_pres'] ?>" class="btn btn-primary"><i class="fa fa-print"></i></a>
                                                            <?php  ?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
    </div>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Data Layanan Customer: <?php echo $datatambahdetail['nm_cust'] ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="quatation.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <!-- PARAMETER TAMBAH DATA BERDASARKAN KD_PRODUCT_PRES -->
                            <div class="col-12 col-md-8">
                                <select name="kd_product_pres" id="select" class="form-control">
                                    <?php 

                                        $cust = mysqli_query($db,"SELECT product_pres.kd_product_pres, nm_cust FROM product_pres, canvasing, potpen, cust WHERE product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND product_pres.response_product_pres='Positif'")or die(mysqli_error($db));
                                        while ($datacust = mysqli_fetch_array($cust)) {

                                     ?>
                                    <option value="<?php echo $datacust['kd_product_pres'] ?>"><?php echo $datacust['nm_cust'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Quotation No</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Quotation No" class="form-control" name="no_quad" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Jenis Layanan</label>
                            </div>
                            <!-- PARAMETER TAMBAH DATA BERDASARKAN KD_PRODUCT_PRES -->
                            <div class="col-12 col-md-8">
                                <select name="kd_jns_layanan" id="select" class="form-control">
                                    <?php 

                                        $jns_layanan = mysqli_query($db,"SELECT * FROM jns_layanan")or die(mysqli_error($db));
                                        while ($datajnslayanan = mysqli_fetch_array($jns_layanan)) {

                                     ?>
                                    <option value="<?php echo $datajnslayanan['kd_jns_layanan'] ?>"><?php echo $datajnslayanan['nm_jns_layanan'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Bandwidth</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Bandwidth" class="form-control" name="bw_quad" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">NBR OF QTY/Volume</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Qty atau Volume" class="form-control" name="qty" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Price</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Harga" class="form-control" name="price" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Type Of Payment</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="top" id="select" class="form-control">
                                    <option value="OTC">OTC</option>
                                    <option value="Monthly/Reguler">Monthly/Reguler</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Note/Remark</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <textarea name="note" id="textarea-input" rows="7" placeholder="Masukkan Notes or Remark" class="form-control" required></textarea>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <input type="reset" class="btn btn-warning" value="Reset">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL DETAIL -->
    <?php 
    $detail = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
    while ($datadetail = mysqli_fetch_assoc($detail)) {

     ?>
        <div class="modal fade" id="detail<?php echo $datadetail['kd_product_pres'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Detail Quatation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-2">
                                    <p class="form-control-static"><hr></p>
                                </div>
                                <div class="col col-md-8" align="center">
                                    <label class=" form-control-label"><b><?php echo $datadetail['nm_cust'] ?></b></label>
                                </div>
                                <div class="col-12 col-md-2">
                                    <p class="form-control-static"><hr></p>
                                </div>
                            </div><br>
                            <button type="button" class="btn btn-secondary mb-1" data-toggle="modal"  data-target="#tambah<?php echo $datadetail['kd_product_pres'] ?>">
                                <i class="fa fa-plus"></i>
                            </button>
                            <div class="table-responsive table--no-card m-b-30">
                                <table class="table table-top-campaign">
                                    <thead>
                                        <tr>
                                            <th>No Quatation</th>
                                            <th>Nama Jenis Layanan</th>
                                            <th>Bandwidth</th>
                                            <th>Nbr of Qty/Volume</th>
                                            <th>Unit Price(Rp)</th>
                                            <th>Subtotal(Rp)</th>
                                            <th>Type Of Payment</th>
                                            <th>Note/Remark</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $detaill = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND create_quatation.kd_product_pres='$datadetail[kd_product_pres]'")or die(mysqli_error($db));
                                        while ($datadetaill = mysqli_fetch_assoc($detaill)) {

                                         ?>
                                        <tr>
                                            <td><?php echo $datadetaill['no_quad'] ?></td>
                                            <td><?php echo $datadetaill['nm_jns_layanan'] ?></td>
                                            <td><?php echo $datadetaill['bw_quad'] ?></td>
                                            <td><?php echo $datadetaill['qty'] ?></td>
                                            <td><?php echo rupiah($datadetaill['price']) ?></td>
                                            <td><?php echo rupiah($datadetaill['subtotal']) ?></td>
                                            <td><?php echo $datadetaill['top'] ?></td>
                                            <td><?php echo $datadetaill['note'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editdatadetailquad<?php echo $datadetaill['kd_quad'] ?>">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapusdatadetailquad<?php echo $datadetaill['kd_quad'] ?>" data-dismiss="modal">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <?php 
                                        $total += $datadetaill['subtotal'];
                                            }
                                            $titil = mysqli_query($db,"SELECT SUM(subtotal) AS 'total' FROM create_quatation WHERE create_quatation.kd_product_pres='$datadetail[kd_product_pres]'");
                                            $datatitil = mysqli_fetch_assoc($titil);

                                         ?>
                                        <tr>
                                            <td style="text-align: right;">Total:</td>
                                            <td colspan="8" style="text-align: left; color: red;"><b><?php echo rupiah($datatitil['total']) ?></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
     <?php } ?>
     <!-- MODAL TAMBAH DETAIL PRODUK -->
     <?php 
     $tambahdetail = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust");
     while ($datatambahdetail = mysqli_fetch_assoc($tambahdetail)) {

      ?>
    <div class="modal fade" id="tambah<?php echo $datatambahdetail['kd_product_pres'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Data Layanan Customer: <?php echo $datatambahdetail['nm_cust'] ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="quatation.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Quotation No</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Quotation No" class="form-control" name="no_quad" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Jenis Layanan</label>
                            </div>
                            <!-- PARAMETER TAMBAH DATA BERDASARKAN KD_PRODUCT_PRES -->
                            <input type="hidden" name="kd_product_pres" value="<?php echo $datatambahdetail['kd_product_pres'] ?>">
                            <div class="col-12 col-md-8">
                                <select name="kd_jns_layanan" id="select" class="form-control">
                                    <?php 

                                        $jns_layanan = mysqli_query($db,"SELECT * FROM jns_layanan")or die(mysqli_error($db));
                                        while ($datajnslayanan = mysqli_fetch_array($jns_layanan)) {

                                     ?>
                                    <option value="<?php echo $datajnslayanan['kd_jns_layanan'] ?>"><?php echo $datajnslayanan['nm_jns_layanan'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Bandwidth</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Bandwidth" class="form-control" name="bw_quad" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">NBR OF QTY/Volume</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Qty atau Volume" class="form-control" name="qty" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Price</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Harga" class="form-control" name="price" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Type Of Payment</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="top" id="select" class="form-control">
                                    <option value="OTC">OTC</option>
                                    <option value="Monthly/Reguler">Monthly/Reguler</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Note/Remark</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <textarea name="note" id="textarea-input" rows="7" placeholder="Masukkan Notes or Remark" class="form-control" required></textarea>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <input type="reset" class="btn btn-warning" value="Reset">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
     <?php } ?>
    <!-- MODAL EDIT -->
    <?php 
    $modaldetail = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust");
    while ($datamodaldetail = mysqli_fetch_assoc($modaldetail)) {
        $jnslayanan = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND create_quatation.kd_product_pres='$datamodaldetail[kd_product_pres]'");
        while ($datajnslayanan = mysqli_fetch_assoc($jnslayanan)) {

     ?>
    <div class="modal fade" id="editdatadetailquad<?php echo $datajnslayanan['kd_quad'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Edit Data Layanan Customer: <?php echo $datajnslayanan['nm_cust'] ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="quatation.php" method="POST">
                        <!-- PARAMETER UPDATE DATA BERDASARKAN KD_QUAD -->
                        <input type="hidden" name="kd_quad" value="<?php echo $datajnslayanan['kd_quad'] ?>">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Quotation No</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Quotation No" class="form-control" name="no_quad" value="<?php echo $datajnslayanan['no_quad'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Jenis Layanan</label>
                            </div>
                            <!-- PARAMETER TAMBAH DATA BERDASARKAN KD_PRODUCT_PRES -->
                            <input type="hidden" name="kd_product_pres" value="<?php echo $datajnslayanan['kd_product_pres'] ?>">
                            <input type="hidden" name="kd_jns_layanan" value="<?php echo $datajnslayanan['kd_jns_layanan'] ?>">
                            <div class="col-12 col-md-8">
                                <select name="kd_jns_layanan" id="select" class="form-control" disabled>
                                    <option value="<?php echo $datajnslayanan['kd_jns_layanan'] ?>"><?php echo $datajnslayanan['nm_jns_layanan'] ?></option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Bandwidth</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Bandwidth" class="form-control" name="bw_quad" value="<?php echo $datajnslayanan['bw_quad'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">NBR OF QTY/Volume</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Qty atau Volume" class="form-control" name="qty" value="<?php echo $datajnslayanan['qty'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Price</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Harga" class="form-control" name="price" value="<?php echo $datajnslayanan['price'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Type Of Payment</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="top" id="select" class="form-control">
                                    <option value="OTC"<?php echo ($datajnslayanan['top']=="OTC") ? "selected":"" ?>>OTC</option>
                                    <option value="Monthly/Reguler"<?php echo ($datajnslayanan['top']=="Monthly/Reguler") ? "selected":"" ?>>Monthly/Reguler</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Note/Remark</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <textarea name="note" id="textarea-input" rows="7" placeholder="Masukkan Notes or Remark" class="form-control" required><?php echo $datajnslayanan['note'] ?></textarea>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="edit" class="btn btn-primary" value="Save">
                    <input type="reset" class="btn btn-warning" value="Reset">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL HAPUS -->
    <div class="modal fade" id="hapusdatadetailquad<?php echo $datajnslayanan['kd_quad'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Hapus Data Potensi Layanan Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="quatation.php" method="POST">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="form-control-label">Yakin Ingin Menghapus Data Jenis Layanan <b><?php echo $datajnslayanan['nm_jns_layanan'] ?>?</b> Pelanggan: <b><?php echo $datajnslayanan['nm_cust'] ?></b> dengan </label>
                                <input type="hidden" name="kd_quad" value="<?php echo $datajnslayanan['kd_quad'] ?>">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php 
        }
    }

     ?>

     <!-- MODAL HAPUS CUSTOMER -->
    <?php 
    $modalhapus = mysqli_query($db,"SELECT kd_quad, cust.nm_cust, cust.kd_cust, create_quatation.kd_product_pres, no_quad, create_quatation.kd_jns_layanan, jns_layanan.nm_jns_layanan, bw_quad, qty, price, subtotal, top, note FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust");
    while ($datamodalhapus = mysqli_fetch_assoc($modalhapus)) {

     ?>
        <div class="modal fade" id="hapus<?php echo $datamodalhapus['kd_product_pres'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Hapus Data Potensi Layanan Customer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="quatation.php" method="POST">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Yakin Ingin Menghapus Data Quotation <b><?php echo $datamodalhapus['nm_cust'] ?>?</b></label>
                                    <input type="hidden" name="kd_product_pres" value="<?php echo $datamodalhapus['kd_product_pres'] ?>">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="hapuscust" class="btn btn-primary" value="Delete">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
     <?php } ?>
    <!-- QUERY PROSES -->
    <?php 
    if ($_POST['simpan']) {
    $kd_product_pres = $_POST['kd_product_pres'];
    $no_quad = $_POST['no_quad'];
    $kd_jns_layanan = $_POST['kd_jns_layanan'];
    $bw_quad = $_POST['bw_quad'];
    $qty = $_POST['qty'];
    $price = $_POST['price'];
    $subtotal = $price * $qty;
    $top = $_POST['top'];
    $note = $_POST['note'];

    $cek = mysqli_query($db,"SELECT kd_jns_layanan FROM create_quatation WHERE kd_product_pres='$kd_product_pres'");
    $datacek = mysqli_fetch_assoc($cek);

    if ($kd_jns_layanan==$datacek['kd_jns_layanan']) {
        echo "<script>alert('Data Layanan Sudah Ada, Mohon Masukkan Data Layanan Yang Berbeda')</script>";
        echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
    }else{
        $tambah = mysqli_query($db,"INSERT INTO create_quatation VALUES('','$kd_product_pres','$no_quad','$kd_jns_layanan','$bw_quad','$qty','$price','$subtotal','$top','$note')")or die(mysqli_error($db));
        if ($tambah) {
            echo "<script>alert('Berhasil Tambah Data Layanan')</script>";
            echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
        }else{
            echo "<script>alert('Gagal Tambah Data Layanan')</script>";
            echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
        }
    }
}elseif ($_POST['edit']) {
    $kd_quad = $_POST['kd_quad'];
    $kd_product_pres = $_POST['kd_product_pres'];
    $no_quad = $_POST['no_quad'];
    $kd_jns_layanan = $_POST['kd_jns_layanan'];
    $bw_quad = $_POST['bw_quad'];
    $qty = $_POST['qty'];
    $price = $_POST['price'];
    $subtotal = $qty * $price;
    $top = $_POST['top'];
    $note = $_POST['note'];

    $edit = mysqli_query($db,"UPDATE create_quatation SET kd_product_pres='$kd_product_pres', no_quad='$no_quad', kd_jns_layanan='$kd_jns_layanan', bw_quad='$bw_quad', qty='$qty', price='$price', subtotal='$subtotal', top='$top', note='$note' WHERE kd_quad='$kd_quad'")or die(mysqli_error($db));

    if ($edit) {
        echo "<script>alert('Berhasil Edit Data Layanan')</script>";
        echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
    }else{
        echo "<script>alert('Gagal Edit Data Layanan')</script>";
        echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
    }
}elseif ($_POST['hapus']) {
    $kd_quad = $_POST['kd_quad'];

    $hapus = mysqli_query($db,"DELETE FROM create_quatation WHERE kd_quad='$kd_quad'")or die(mysqli_error($db));

    if ($hapus) {
    echo "<script>alert('Berhasil Menghapus Data')</script>";
    echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
    }else{
        echo "<script>alert('Gagal Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
    }
}elseif($_POST['hapuscust']){
    $kd_product_pres = $_POST['kd_product_pres'];

    $hapuscust = mysqli_query($db,"DELETE FROM create_quatation WHERE kd_product_pres='$kd_product_pres'");

    if ($hapuscust) {
    echo "<script>alert('Berhasil Menghapus Data')</script>";
    echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
    }else{
        echo "<script>alert('Gagal Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="quatation.php?halaman=quatation"</script>';
    }
}

     ?>
     <script type="text/javascript">
         function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->