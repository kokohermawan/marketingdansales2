<?php 
include '../config.php';
error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Pengguna Aplikasi</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php"; ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Pengguna</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-top-campaign" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Nama Pengguna</th>
                                                        <th>NIP</th>
                                                        <th>Posisi</th>
                                                        <th>Inisial</th>
                                                        <th>Username</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $query = mysqli_query($db,"SELECT kd_pengguna, nm_posisi, nm_pengguna, inisial, nip, username FROM pengguna, posisi WHERE pengguna.kd_posisi=posisi.kd_posisi")or die(mysqli_error($db));
                                                        $no=0;
                                                        while ($data = mysqli_fetch_assoc($query)) {
                                                            
                                                            $no++;

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['nm_pengguna'] ?></td>
                                                        <td><?php echo $data['nip'] ?></td>
                                                        <td><?php echo $data['nm_posisi'] ?></td>
                                                        <td><?php echo $data['inisial'] ?></td>
                                                        <td><?php echo $data['username'] ?></td>
                                                        <td width="10%">
                                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit<?php echo $data['kd_pengguna'] ?>">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_pengguna'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
    </div>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Data User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- JIKA TIDAK ADA DATA CANVASING YANG RESPONSENYA POSITIF -->
                <div class="modal-body">
                    <form action="user.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Nama Pengguna</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="nm_pengguna" placeholder="Masukkan Nama Pengguna" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">NIP</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan NIP" class="form-control" name="nip" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Posisi</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="kd_posisi" id="select" class="form-control">
                                    <?php 

                                        $posisi = mysqli_query($db,"SELECT * FROM posisi")or die(mysqli_error($db));
                                        while ($dataposisi = mysqli_fetch_array($posisi)) {

                                     ?>
                                    <option value="<?php echo $dataposisi['kd_posisi'] ?>"><?php echo $dataposisi['nm_posisi'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Inisial</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" placeholder="Masukkan Inisial" name="inisial" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Username</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" placeholder="Masukkan Username" name="username" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Password</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="password" class="form-control" placeholder="Masukkan Password" name="pwd" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Konfirmasi Password</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="password" class="form-control" placeholder="Masukkan Konfirmasi Password" name="konf_pwd" required>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL EDIT -->
    <?php
    $edit = mysqli_query($db,"SELECT kd_pengguna, nm_posisi, nm_pengguna, inisial, nip, username, pwd, pengguna.kd_posisi FROM pengguna, posisi WHERE pengguna.kd_posisi=posisi.kd_posisi")or die(mysqli_error($db));
    while ($dataedit = mysqli_fetch_array($edit)){ 

    ?>
    <div class="modal fade" id="edit<?php echo $dataedit['kd_pengguna'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Edit Data Product Presentation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="user.php" method="POST">
                        <input type="hidden" name="kd_pengguna" value="<?php echo $dataedit['kd_pengguna'] ?>">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Nama Pengguna</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="nm_pengguna" placeholder="Masukkan Nama Pengguna" value="<?php echo $dataedit['nm_pengguna'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">NIP</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan NIP" class="form-control" name="nip" value="<?php echo $dataedit['nip'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Posisi</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="kd_posisi" id="select" class="form-control">
                                    <?php 

                                        $posisi = mysqli_query($db,"SELECT * FROM posisi")or die(mysqli_error($db));
                                        while ($dataposisi = mysqli_fetch_array($posisi)) {

                                     ?>
                                    <option value="<?php echo $dataposisi['kd_posisi'] ?>"<?php echo ($dataposisi['kd_posisi']==$dataedit['kd_posisi']) ? "selected":"" ?>><?php echo $dataposisi['nm_posisi'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Inisial</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" placeholder="Masukkan Inisial" name="inisial" value="<?php echo $dataedit['inisial'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Username</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" placeholder="Masukkan Username" name="username" value="<?php echo $dataedit['username'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Password</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="password" class="form-control" placeholder="Masukkan Password" name="pwd" required>
                                <!-- <input type="checkbox" id="chk">
                                <label id="showhide" class="label" >Tampilkan Password</label> -->
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Konfirmasi Password</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="password" class="form-control" placeholder="Masukkan Konfirmasi Password" name="konf_pwd" required>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="edit" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <!-- MODAL HAPUS -->
    <?php
    $hapus = mysqli_query($db,"SELECT kd_pengguna, nm_posisi, nm_pengguna, inisial, nip, username, pwd, pengguna.kd_posisi FROM pengguna, posisi WHERE pengguna.kd_posisi=posisi.kd_posisi")or die(mysqli_error($db));
    while ($datahapus = mysqli_fetch_array($hapus)){ 

    ?>
<div class="modal fade" id="hapus<?php echo $datahapus['kd_pengguna'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Hapus Akun Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="user.php" method="POST">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Yakin Ingin Menghapus Akun <b><?php echo $datahapus['nm_pengguna'] ?>?</b></label>
                            <input type="hidden" name="kd_pengguna" value="<?php echo $datahapus['kd_pengguna'] ?>">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
    <?php } ?>

    <!-- QUERY PROSES -->
    <?php 
    if ($_POST['simpan']) {
        $kd_posisi = $_POST['kd_posisi'];
        $nm_pengguna = $_POST['nm_pengguna'];
        $inisial = $_POST['inisial'];
        $nip = $_POST['nip'];
        $username = $_POST['username'];
        $pwd = $_POST['pwd'];
        $konf_pwd = $_POST['konf_pwd'];
        $enc_pwd = sha1($pwd);
        
        $cek = mysqli_query($db,"SELECT username FROM pengguna");
        $cekdata = mysqli_fetch_assoc($cek);
        if ($username==$cekdata['username']) {
            echo "<script>alert('Username Sudah Digunakan, Gunakan Username Lain')</script>";
            echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
        }else{
            if ($pwd==$konf_pwd) {

                $tambah = mysqli_query($db,"INSERT INTO pengguna VALUES('','$kd_posisi','$nm_pengguna','$inisial','$nip','$username','$enc_pwd')")or die(mysqli_error($db));
                if ($tambah) {
                    echo "<script>alert('Berhasil Menambah User')</script>";
                    echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
                }else{
                    echo "<script>alert('Gagal Tambah Data User')</script>";
                    echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
                }
            }else{
                echo "<script>alert('Password dan Konfirmasi Password Tidak Sesuai, Masukkan Password dan Konfirmasi Password Yang Sesuai')</script>";
                echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
            }
        }
    }elseif ($_POST['edit']) {
        $kd_pengguna = $_POST['kd_pengguna'];
        $kd_posisi = $_POST['kd_posisi'];
        $nm_pengguna = $_POST['nm_pengguna'];
        $inisial = $_POST['inisial'];
        $nip = $_POST['nip'];
        $username = $_POST['username'];
        $pwd = $_POST['pwd'];
        $konf_pwd = $_POST['konf_pwd'];
        $enc_pwd = sha1($pwd);
        $validasi_edit = "valid";

        $cek = mysqli_query($db,"SELECT username FROM pengguna WHERE kd_pengguna !='$kd_pengguna'");
        while ($cekdata = mysqli_fetch_assoc($cek)) {
            if ($username==$cekdata['username']) {
                echo "<script>alert('Username Sudah Digunakan, Gunakan Username Lain')</script>";
                echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
                $validasi_edit = "tidak valid";
            }
        }
        
        if($validasi_edit!="tidak valid"){
        
            $edit = mysqli_query($db,"UPDATE pengguna SET kd_posisi='$kd_posisi', nm_pengguna='$nm_pengguna', inisial='$inisial', nip='$nip', username='$username', pwd='$enc_pwd' WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error($db));
            if ($edit) {
                echo "<script>alert('Berhasil Edit Data Pengguna')</script>";
                echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
            }else{
                echo "<script>alert('Gagal Edit Data Pengguna')</script>";
                echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
            }
        }
    }elseif ($_POST['hapus']) {
        $kd_pengguna = $_POST['kd_pengguna'];

        $hapus = mysqli_query($db,"DELETE FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error($db));

        if ($hapus) {
        echo "<script>alert('Berhasil Menghapus Akun')</script>";
        echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
        }else{
            echo "<script>alert('Gagal Menghapus Akun')</script>";
            echo '<script type="text/javascript">window.location="user.php?halaman=user"</script>';
        }
    }

     ?>
     <!-- JAVASCRIPT UNTUK SHOW / HIDDEN PASSWORD -->
     <script type="text/javascript">
        // password
        var masukkanpass = document.getElementById('pwd'),
            chk = document.getElementById('chk'),
            label = document.getElementById('showhide');

            chk.onclick = function(){
                if(chk.checked){
                    masukkanpass.setAttribute('type', 'text');
                    label.textContent = 'Sembunyikan Password';
                }else{
                    masukkanpass.setAttribute('type', 'password');
                    label.textContent = 'Tampilkan Password';
                }
            }
            // konfirmasi password
            var masukkankonfir = document.getElementById('pswd2'),
                chk2 = document.getElementById('chk2'),
                label2 = document.getElementById('showhide2');

            chk2.onclick = function(){
                if(chk2.checked){
                    masukkankonfir.setAttribute('type', 'text');
                    label2.textContent = 'Sembunyikan Konfirmasi Password';
                }else{
                    masukkankonfir.setAttribute('type', 'password');
                    label2.textContent = 'Tampilkan Konfirmasi Password';
                }
            }

            function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }

    </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->