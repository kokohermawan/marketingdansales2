<?php 
include '../config.php';
include 'fungsi_rupiah.php';
include 'fungsi_tgl.php';

error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Potensi Pendapatan</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php"; ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Potensi Pendapatan</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-top-campaign" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Nama Customer</th>
                                                        <th>Status Potensi</th>
                                                        <th>Nama PPK</th>
                                                        <th>Telp. PPK</th>
                                                        <th>Tgl Progress</th>
                                                        <th>Rencana</th>
                                                        <th>Kendala</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    if($_SESSION['level']=="Sales"){
                                                        $query = mysqli_query($db,"SELECT kd_potpen, nm_cust, status_potensi, nama_ppk, telp_ppk, tgl_potpen, rencana, kendala FROM potpen, cust WHERE potpen.kd_cust=cust.kd_cust AND cust.kd_pengguna='$_SESSION[kd_pengguna]'")or die(mysqli_error($db));
                                                    }else{
                                                        $query = mysqli_query($db,"SELECT kd_potpen, nm_cust, status_potensi, nama_ppk, telp_ppk, tgl_potpen, rencana, kendala FROM potpen, cust WHERE potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
                                                    }
                                                        $no=0;
                                                        while ($data = mysqli_fetch_assoc($query)) {
                                                            
                                                            $no++;

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <td><?php echo $data['status_potensi'] ?></td>
                                                        <td><?php echo $data['nama_ppk'] ?></td>
                                                        <td><?php echo $data['telp_ppk'] ?></td>
                                                        <td><?php echo format_indo($data['tgl_potpen']) ?></td>
                                                        <td><?php echo $data['rencana'] ?></td>
                                                        <td><?php echo $data['kendala'] ?></td>
                                                        <td width="10%">
                                                        <!-- detail untuk melihat detail layanan per cust, dan bisa crud di link baru-->
                                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#detail<?php echo $data['kd_potpen'] ?>">
                                                                <i class="fa fa-info"></i>
                                                            </button>
                                                        <!-- modal tambah data layanan -->
                                                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#tambahlayanan<?php echo $data['kd_potpen'] ?>" data-dismiss="modal">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                            <!-- edit data potpen -->
                                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit<?php echo $data['kd_potpen'] ?>">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                        <!-- hapus data berdasarkan potpen -->
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_potpen'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
    </div>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Data Potensi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="potpen_tampil.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_cust" id="select" class="form-control">
                                    <?php 
                                        if($_SESSION['level']=="Sales"){
                                            $query = mysqli_query($db,"SELECT * FROM cust WHERE kd_pengguna='$_SESSION[kd_pengguna]'")or die(mysqli_error($db));    
                                        }else{
                                            $query = mysqli_query($db,"SELECT * FROM cust")or die(mysqli_error($db));
                                        }
                                        
                                        while ($data = mysqli_fetch_array($query)) {

                                     ?>
                                    <option value="<?php echo $data['kd_cust'] ?>"><?php echo $data['nm_cust'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Status Potensi</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="status_potensi" id="select" class="form-control">
                                    <option value="Merah">Merah</option>
                                    <option value="Kuning">Kuning</option>
                                    <option value="Hijau">Hijau</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Nama PPK (<i>Pejabat Pembuat Komitmen</i>)</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Nama PPK" class="form-control" name="nama_ppk" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Telp. PPK</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Nomor Telepon PPK" class="form-control" name="telp_ppk" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tanggal Prospek</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="tgl_potpen" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Rencana Tindak Lanjut</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="rencana" id="select" class="form-control">
                                    <option value="Flag Need Follow Up">Flag Need Follow Up</option>
                                    <option value="Kunjungan Kembali">Kunjungan Kembali</option>
                                    <option value="Ajak Studi Banding">Ajak Studi Banding</option>
                                    <option value="Submit Proposal Teknis dan SHP">Submit Proposal Teknis dan SHP</option>
                                    <option value="Kunjungan Vasilitas ICON+">Kunjungan Vasilitas ICON+</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Potensi Kendala</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kendala" id="select" class="form-control">
                                    <option value="Tidak Ada Kendala">Tidak Ada Kendala</option>
                                    <option value="Out Of Coverage">Out Of Coverage</option>
                                    <option value="Perizinan Kawasan">Perizinan Kawasan</option>
                                </select>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL DETAIL -->
    <?php 
    $detail = mysqli_query($db,"SELECT kd_potpen, nm_cust, status_potensi, nama_ppk, telp_ppk, tgl_potpen, rencana, kendala FROM potpen, cust WHERE potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($datadetail = mysqli_fetch_assoc($detail)) {

     ?>
     <div class="modal fade" id="detail<?php echo $datadetail['kd_potpen'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Data Detail Potensi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <p class="form-control-static"><hr></p>
                        </div>
                        <div class="col col-md-2">
                            <label class=" form-control-label"><b>General Info</b></label>
                        </div>
                        <div class="col-12 col-md-5">
                            <p class="form-control-static"><hr></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Customer</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['nm_cust'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Status Potensi</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['status_potensi'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Nama PPK</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['nama_ppk'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Telp. PPK</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['telp_ppk'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Tanggal Prospek</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['tgl_potpen'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Rencana Tindak Lanjut</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['rencana'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Potensi Kendala</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['kendala'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <p class="form-control-static"><hr></p>
                        </div>
                        <div class="col col-md-4" align="center">
                            <label class=" form-control-label"><b>Detail Info / Service</b></label>
                        </div>
                        <div class="col-12 col-md-4">
                            <p class="form-control-static"><hr></p>
                        </div>
                    </div>
                    <div class="table-responsive table--no-card m-b-30">
                        <table class="table table-top-campaign">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jenis Layanan</th>
                                    <th>Bandwidth</th>
                                    <th>Provider</th>
                                    <th>Biaya Sewa</th>
                                    <th>Est. Kontrak Berakhir</th>
                                    <th>Est. Mulai Pengadaan</th>
                                    <th>Metode Pengadaan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $layanan = mysqli_query($db,"SELECT kd_det_pot, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen='$datadetail[kd_potpen]'");
                            $noo=0;
                            
                            while ($datalayanan = mysqli_fetch_assoc($layanan)) {
                                $noo++;

                             ?>
                                <tr>
                                    <td><?php echo $noo ?></td>
                                    <td><?php echo $datalayanan['nm_jns_layanan'] ?></td>
                                    <td><?php echo $datalayanan['bw'] ?></td>
                                    <td><?php echo $datalayanan['provider'] ?></td>
                                    <td><?php echo rupiah($datalayanan['biaya_sewa']) ?></td>
                                    <td><?php echo format_indo($datalayanan['est_kont_berakhir']) ?></td>
                                    <td><?php echo format_indo($datalayanan['est_mulai_pengadaan']) ?></td>
                                    <td><?php echo $datalayanan['metode_pengadaan'] ?></td>
                                    <td>
                                        <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#dataeditlayanan<?php echo $datalayanan['kd_det_pot'] ?>" data-dismiss="modal">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapuslayanan<?php echo $datalayanan['kd_det_pot'] ?>">
                                            <i class="fa fa-trash" data-dismiss="modal"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
     <?php } ?>
     <!-- MODAL TAMBAH LAYANAN-->
     <?php 
     $tambahlayanan = mysqli_query($db,"SELECT kd_potpen, nm_cust, status_potensi, nama_ppk, telp_ppk, tgl_potpen, rencana, kendala FROM potpen, cust WHERE potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
     while ($datatambahlayanan = mysqli_fetch_assoc($tambahlayanan)) {
      ?>

    <div class="modal fade" id="tambahlayanan<?php echo $datatambahlayanan['kd_potpen'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Data Potensi Layanan Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="potpen_tampil.php" method="POST">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <p class="form-control-static"><hr></p>
                            </div>
                            <div class="col col-md-4" align="center">
                                <label class=" form-control-label"><b>Detail Info / Service</b></label>
                            </div>
                            <div class="col-12 col-md-4">
                                <p class="form-control-static"><hr></p>
                            </div>
                        </div>
                        <input type="hidden" name="kd_potpen" value="<?php echo $datatambahlayanan['kd_potpen'] ?>">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Jenis Layanan</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_jns_layanan" id="select" class="form-control">
                                    <?php 

                                        $jns_layanan = mysqli_query($db,"SELECT * FROM jns_layanan")or die(mysqli_error($db));
                                        while ($datajnslayanan = mysqli_fetch_array($jns_layanan)) {

                                     ?>
                                    <option value="<?php echo $datajnslayanan['kd_jns_layanan'] ?>"><?php echo $datajnslayanan['nm_jns_layanan'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Bandwidth</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Bandwidth" class="form-control" name="bw" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Provider</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Provider" class="form-control" name="provider" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Biaya Sewa</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Besar Biaya Sewa" class="form-control" name="biaya_sewa" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Est. Kontrak Berakhir</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="est_kont_berakhir" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Est. Mulai Pengadaan</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="est_mulai_pengadaan" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Metode Pengadaan</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="metode_pengadaan" id="select" class="form-control">
                                    <option value="Tender">Tender</option>
                                    <option value="Pengadaan Langsung">Pengadaan Langsung</option>
                                    <option value="E-Katalog">E-Katalog</option>
                                </select>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="tambahlayanan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>
    <!-- MODAL EDIT LAYANAN -->
    <?php 
    $detaileditlayanan = mysqli_query($db,"SELECT kd_potpen, nm_cust, status_potensi, nama_ppk, telp_ppk, tgl_potpen, rencana, kendala FROM potpen, cust WHERE potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($datadetaileditlayanan = mysqli_fetch_assoc($detaileditlayanan)) {
        $editlayanan = mysqli_query($db,"SELECT kd_det_pot, detail_potpen.kd_potpen, detail_potpen.kd_jns_layanan, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen='$datadetaileditlayanan[kd_potpen]'");
        while ($dataeditlayanan = mysqli_fetch_assoc($editlayanan)) {

     ?>
        <div class="modal fade" id="dataeditlayanan<?php echo $dataeditlayanan['kd_det_pot'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Data Layanan Customer Potensi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="potpen_tampil.php" method="POST">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <p class="form-control-static"><hr></p>
                                </div>
                                <div class="col col-md-4" align="center">
                                    <label class=" form-control-label"><b>Detail Info / Service</b></label>
                                </div>
                                <div class="col-12 col-md-4">
                                    <p class="form-control-static"><hr></p>
                                </div>
                            </div>
                            <input type="hidden" name="kd_det_pot" value="<?php echo $dataeditlayanan['kd_det_pot'] ?>">
                            <input type="hidden" name="kd_potpen" value="<?php echo $dataeditlayanan['kd_potpen'] ?>">
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Jenis Layanan</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <select name="kd_jns_layanan" id="select" class="form-control">
                                        <?php 

                                            $jns_layanan = mysqli_query($db,"SELECT * FROM jns_layanan")or die(mysqli_error($db));
                                            while ($datajnslayanan = mysqli_fetch_array($jns_layanan)) {

                                         ?>
                                        <option value="<?php echo $datajnslayanan['kd_jns_layanan'] ?>"<?php echo ($datajnslayanan['kd_jns_layanan']==$dataeditlayanan['kd_jns_layanan']) ? "selected":"" ?>><?php echo $datajnslayanan['nm_jns_layanan'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Bandwidth</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" placeholder="Masukkan Besar Bandwidth" class="form-control" name="bw" value="<?php echo $dataeditlayanan['bw'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Provider</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" placeholder="Masukkan Provider" class="form-control" name="provider" value="<?php echo $dataeditlayanan['provider'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Biaya Sewa</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" placeholder="Masukkan Besar Biaya Sewa" class="form-control" name="biaya_sewa" value="<?php echo $dataeditlayanan['biaya_sewa'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Est. Kontrak Berakhir</label>
                                </div>
                                <div class="col-12 col-md-3">
                                    <input type="date" class="form-control" name="est_kont_berakhir" value="<?php echo $dataeditlayanan['est_kont_berakhir'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Est. Mulai Pengadaan</label>
                                </div>
                                <div class="col-12 col-md-3">
                                    <input type="date" class="form-control" name="est_mulai_pengadaan" value="<?php echo $dataeditlayanan['est_mulai_pengadaan'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Metode Pengadaan</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <select name="metode_pengadaan" id="select" class="form-control">
                                        <option value="Tender"<?php echo ($dataeditlayanan['metode_pengadaan']=="Tender") ? "selected":"" ?>>Tender</option>
                                        <option value="Pengadaan Langsung"<?php echo ($dataeditlayanan['metode_pengadaan']=="Pengadaan Langsung") ? "selected":"" ?>>Pengadaan Langsung</option>
                                        <option value="E-Katalog"<?php echo ($dataeditlayanan['metode_pengadaan']=="E-Katalog") ? "selected":"" ?>>E-Katalog</option>
                                    </select>
                                </div>
                            </div><br>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="editlayanan" class="btn btn-primary" value="Save">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
     <?php 
         }
        }

      ?>
    <!-- MODAL HAPUS LAYANAN -->
    <?php 
    $detailhapuslayanan = mysqli_query($db,"SELECT kd_potpen, nm_cust, status_potensi, nama_ppk, telp_ppk, tgl_potpen, rencana, kendala FROM potpen, cust WHERE potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($datadetailhapuslayanan = mysqli_fetch_assoc($detailhapuslayanan)) {
        $hapuslayanan = mysqli_query($db,"SELECT kd_det_pot, detail_potpen.kd_potpen, detail_potpen.kd_jns_layanan, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen='$datadetailhapuslayanan[kd_potpen]'");
        while ($datahapuslayanan = mysqli_fetch_assoc($hapuslayanan)) {
            
    ?>
        <div class="modal fade" id="hapuslayanan<?php echo $datahapuslayanan['kd_det_pot'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Hapus Data Potensi Layanan Customer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="potpen_tampil.php" method="POST">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Yakin Ingin Menghapus Data Potensi Layanan Customer Dengan Nama Layanan <b><?php echo $datahapuslayanan['nm_jns_layanan'] ?>?</b></label>
                                    <input type="hidden" name="kd_det_pot" value="<?php echo $datahapuslayanan['kd_det_pot'] ?>">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="hapuslayanan" class="btn btn-primary" value="Delete">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php
        }
    }

     ?>
    <!-- MODAL EDIT -->
    <?php
    $edit = mysqli_query($db,"SELECT potpen.kd_cust, kd_potpen, nm_cust, status_potensi, nama_ppk, telp_ppk, tgl_potpen, rencana, kendala FROM potpen, cust WHERE potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($dataedit = mysqli_fetch_array($edit)){ 

    ?>
    <div class="modal fade" id="edit<?php echo $dataedit['kd_potpen'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Edit Data Potensi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="potpen_tampil.php" method="POST">
                        <input type="hidden" name="kd_potpen" value="<?php echo $dataedit['kd_potpen'] ?>">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_cust" id="select" class="form-control" readonly>
                                    <option value="<?php echo $dataedit['kd_cust'] ?>"><?php echo $dataedit['nm_cust'] ?></option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Status Potensi</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="status_potensi" id="select" class="form-control">
                                    <option value="Merah"<?php echo ($dataedit['status_potensi']=="Merah") ? "selected":"" ?>>Merah</option>
                                    <option value="Kuning"<?php echo ($dataedit['status_potensi']=="Kuning") ? "selected":"" ?>>Kuning</option>
                                    <option value="Hijau"<?php echo ($dataedit['status_potensi']=="Hijau") ? "selected":"" ?>>Hijau</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Nama PPK</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Nama PPK" class="form-control" name="nama_ppk" value="<?php echo $dataedit['nama_ppk'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Telp. PPK</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Nomor Telepon PPK" class="form-control" name="telp_ppk" value="<?php echo $dataedit['telp_ppk'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tanggal Prospek</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="tgl_potpen" value="<?php echo $dataedit['tgl_potpen'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Rencana Tindak Lanjut</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="rencana" id="select" class="form-control">
                                    <option value="Flag Need Follow Up"<?php echo ($dataedit['rencana']=="Flag Need Follow Up") ? "selected":"" ?>>Flag Need Follow Up</option>
                                    <option value="Kunjungan Kembali"<?php echo ($dataedit['rencana']=="Kunjungan Kembali") ? "selected":"" ?>>Kunjungan Kembali</option>
                                    <option value="Ajak Studi Banding"<?php echo ($dataedit['rencana']=="Ajak Studi Banding") ? "selected":"" ?>>Ajak Studi Banding</option>
                                    <option value="Submit Proposal Teknis dan SHP"<?php echo ($dataedit['rencana']=="Submit Proposal Teknis dan SHP") ? "selected":"" ?>>Submit Proposal Teknis dan SHP</option>
                                    <option value="Kunjungan Vasilitas ICON+"<?php echo ($dataedit['rencana']=="Kunjungan Vasilitas ICON+") ? "selected":"" ?>>Kunjungan Vasilitas ICON+</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Potensi Kendala</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kendala" id="select" class="form-control">
                                    <option value="Tidak Ada Kendala"<?php echo ($dataedit['kendala']=="Tidak Ada Kendala") ? "selected":"" ?>>Tidak Ada Kendala</option>
                                    <option value="Out Of Coverage"<?php echo ($dataedit['kendala']=="Out Of Coverage") ? "selected":"" ?>>Out Of Coverage</option>
                                    <option value="Perizinan Kawasan"<?php echo ($dataedit['kendala']=="Perizinan Kawasan") ? "selected":"" ?>>Perizinan Kawasan</option>
                                </select>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="edit" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <!-- MODAL HAPUS -->
    <?php
    $hapus = mysqli_query($db,"SELECT kd_potpen, nm_cust, status_potensi, nama_ppk, telp_ppk, tgl_potpen, rencana, kendala FROM potpen, cust WHERE potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($datahapus = mysqli_fetch_array($hapus)){ 

    ?>
<div class="modal fade" id="hapus<?php echo $datahapus['kd_potpen'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Hapus Data Potensi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="potpen_tampil.php" method="POST">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Yakin Ingin Menghapus Data Potensi Customer <b><?php echo $datahapus['nm_cust'] ?>?</b></label>
                            <input type="hidden" name="kd_potpen" value="<?php echo $datahapus['kd_potpen'] ?>">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
    <?php } ?>

    <!-- QUERY PROSES -->
    <?php 
    if ($_POST['simpan']) {
        $kd_cust = $_POST['kd_cust'];
        $status_potensi = $_POST['status_potensi'];
        $nama_ppk = $_POST['nama_ppk'];
        $telp_ppk = $_POST['telp_ppk'];
        $tgl_potpen = $_POST['tgl_potpen'];
        $now = date('Y-m-d');
        $rencana = $_POST['rencana'];
        $kendala = $_POST['kendala'];
        $validasi_tambah = "valid";
        
        $cek = mysqli_query($db,"SELECT potpen.kd_cust FROM potpen");
        while($cekdata = mysqli_fetch_assoc($cek)){
            if ($kd_cust==$cekdata['kd_cust']) {
                echo "<script>alert('Gagal! Data Potensi Customer Sudah Ada')</script>";
                echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
                $validasi_tambah = "tidak valid";
            }
        }
        if ($tgl_potpen>$now) {
            echo "<script>alert('Gagal! Tanggal Prosepek tidak boleh setelah hari ini')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            $validasi_tambah = "tidak valid";
        }

        if($validasi_tambah!="tidak valid"){

            $tambah = mysqli_query($db,"INSERT INTO potpen VALUES('','$kd_cust','$status_potensi','$nama_ppk','$telp_ppk','$tgl_potpen','$rencana','$kendala')")or die(mysqli_error($db));
            if ($tambah) {
                echo "<script>alert('Berhasil Tambah Data Potensi')</script>";
                echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            }else{
                echo "<script>alert('Gagal Tambah Data Potensi')</script>";
                echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            }
        }
    }elseif ($_POST['edit']) {
        $kd_potpen = $_POST['kd_potpen'];
        $kd_cust = $_POST['kd_cust'];
        $status_potensi = $_POST['status_potensi'];
        $nama_ppk = $_POST['nama_ppk'];
        $telp_ppk = $_POST['telp_ppk'];
        $tgl_potpen = $_POST['tgl_potpen'];
        $rencana = $_POST['rencana'];
        $kendala = $_POST['kendala'];
        
        $edit = mysqli_query($db,"UPDATE potpen SET kd_cust='$kd_cust', status_potensi='$status_potensi', nama_ppk='$nama_ppk', telp_ppk='$telp_ppk', tgl_potpen='$tgl_potpen', rencana='$rencana', kendala='$kendala' WHERE kd_potpen='$kd_potpen'")or die(mysqli_error($db));
        if ($edit) {
            echo "<script>alert('Berhasil Edit Data Potensi')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
        }else{
            echo "<script>alert('Gagal Edit Data Potpen')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
        }
    }elseif ($_POST['hapus']) {
        $kd_potpen = $_POST['kd_potpen'];

        $hapus = mysqli_query($db,"DELETE FROM potpen WHERE kd_potpen='$kd_potpen'")or die(mysqli_error($db));

        if ($hapus) {
        echo "<script>alert('Berhasil Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
        }else{
            echo "<script>alert('Gagal Menghapus Data')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
        }
    }elseif ($_POST['tambahlayanan']) {
        $kd_potpen = $_POST['kd_potpen'];
        $kd_jns_layanan = $_POST['kd_jns_layanan'];
        $bw = $_POST['bw'];
        $provider = $_POST['provider'];
        $biaya_sewa = $_POST['biaya_sewa'];
        $est_kont_berakhir = $_POST['est_kont_berakhir'];
        $est_mulai_pengadaan = $_POST['est_mulai_pengadaan'];
        $metode_pengadaan = $_POST['metode_pengadaan'];
        $now = date('Y-m-d');
        $validasi_tambah = "valid";

        // jika tanggal mulai pengadaan mendahului tanggal kontrak berakhir
        if ($est_mulai_pengadaan > $est_kont_berakhir) {
            echo "<script>alert('Gagal! Data kontrak berakhir dan mulai pengadaan tidak relevan')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            $validasi_tambah = "tidak valid";
        }elseif($est_mulai_pengadaan > $now){
            echo "<script>alert('Gagal! Data mulai pengadaan tidak valid')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            $validasi_tambah = "tidak valid";
        }else{

            $ceklayanan = mysqli_query($db,"SELECT kd_potpen, kd_jns_layanan FROM detail_potpen WHERE kd_potpen='$kd_potpen'");
            while ($dataceklayanan = mysqli_fetch_assoc($ceklayanan)) {
                if ($dataceklayanan['kd_potpen']==$kd_potpen && $dataceklayanan['kd_jns_layanan']==$kd_jns_layanan) {
                    echo "<script>alert('Data Jenis Layanan Customer Sudah Ada, Tambahkan Jenis Layanan Lainnya')</script>";
                    echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
                    $validasi_tambah = "tidak valid";
                }
            }
            if ($validasi_tambah!="tidak valid") {
                $simpanlayanan = mysqli_query($db, "INSERT INTO detail_potpen VALUES('','$kd_potpen','$kd_jns_layanan','$bw','$provider','$biaya_sewa','$est_kont_berakhir','$est_mulai_pengadaan','$metode_pengadaan')")or die(mysqli_error($db));
                if ($simpanlayanan) {
                    echo "<script>alert('Berhasil Tambah Data Layanan Customer')</script>";
                    echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
                }else{
                    echo "<script>alert('Gagal Tambah Data Layanan Customer')</script>";
                    echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
                }
            }
        }
    }elseif ($_POST['editlayanan']) {
        $kd_det_pot = $_POST['kd_det_pot'];
        $kd_potpen = $_POST['kd_potpen'];
        $kd_jns_layanan = $_POST['kd_jns_layanan'];
        $bw = $_POST['bw'];
        $provider = $_POST['provider'];
        $biaya_sewa = $_POST['biaya_sewa'];
        $est_kont_berakhir = $_POST['est_kont_berakhir'];
        $est_mulai_pengadaan = $_POST['est_mulai_pengadaan'];
        $now = date('Y-m-d');
        $metode_pengadaan = $_POST['metode_pengadaan'];
        $validasi_edit = "valid";

        if ($est_mulai_pengadaan > $est_kont_berakhir) {
            echo "<script>alert('Gagal! Data kontrak berakhir dan mulai pengadaan tidak relevan')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            $validasi_edit = "tidak valid";
        }elseif($est_mulai_pengadaan > $now){
            echo "<script>alert('Gagal! Data mulai pengadaan tidak valid')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            $validasi_edit = "tidak valid";
        }else{

            $cek = mysqli_query($db,"SELECT * FROM detail_potpen WHERE kd_det_pot!='$kd_det_pot'");
            while ($datacek = mysqli_fetch_array($cek)) {
                if ($kd_potpen==$datacek['kd_potpen'] && $kd_jns_layanan==$datacek['kd_jns_layanan']) {
                    echo "<script>alert('Gagal! Data jenis layanan sudah ada')</script>";
                    echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
                    $validasi_edit = "tidak valid";
                }
            }
        }

        if($validasi_edit!="tidak valid"){
            $layananedit = mysqli_query($db,"UPDATE detail_potpen SET kd_potpen='$kd_potpen', kd_jns_layanan='$kd_jns_layanan', bw='$bw', provider='$provider', biaya_sewa='$biaya_sewa', est_kont_berakhir='$est_kont_berakhir', est_mulai_pengadaan='$est_mulai_pengadaan', metode_pengadaan='$metode_pengadaan' WHERE kd_det_pot='$kd_det_pot'")or die(mysqli_error($db));
            if ($layananedit) {
                echo "<script>alert('Berhasil Edit Data Potensi Layanan Customer')</script>";
                echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            }else{
                echo "<script>alert('Gagal Edit Data Potensi Layanan Customer')</script>";
                echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
            }
        }
    }elseif ($_POST['hapuslayanan']) {
        $kd_det_pot = $_POST['kd_det_pot'];

        $layananhapus = mysqli_query($db,"DELETE FROM detail_potpen WHERE kd_det_pot='$kd_det_pot'")or die(mysqli_error($db));
        if ($layananhapus) {
        echo "<script>alert('Berhasil Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
        }else{
            echo "<script>alert('Gagal Menghapus Data')</script>";
            echo '<script type="text/javascript">window.location="potpen_tampil.php?halaman=potpen"</script>';
        }
    }

     ?>
     <script type="text/javascript">
         function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->