<?php 
include '../config.php';
include 'fungsi_tgl.php';
error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Canvasing</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />
    

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php"; ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Canvasing</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-top-campaign" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Nama Customer</th>
                                                        <th>Contact Event Number</th>
                                                        <th>Progress</th>
                                                        <th>Tanggal Progress</th>
                                                        <th>Respon Customer</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    if($_SESSION['level']=="Sales"){
                                                        $query = mysqli_query($db,"SELECT kd_canv, canvasing.kd_potpen, cen, prog_canv, tgl_canv, response_canv, nm_cust, reason_neg_canv FROM canvasing, cust, potpen WHERE canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND cust.kd_pengguna='$_SESSION[kd_pengguna]'")or die(mysqli_error($db));
                                                    }else{
                                                        $query = mysqli_query($db,"SELECT kd_canv, canvasing.kd_potpen, cen, prog_canv, tgl_canv, response_canv, nm_cust, reason_neg_canv FROM canvasing, cust, potpen WHERE canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
                                                    }
                                                        $no=0;
                                                        while ($data = mysqli_fetch_assoc($query)) {
                                                            
                                                            $no++;

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <td><?php echo $data['cen'] ?></td>
                                                        <td><?php echo $data['prog_canv'] ?></td>
                                                        <td><?php echo format_indo($data['tgl_canv']) ?></td>
                                                        <td><?php echo $data['response_canv'] ?></td>
                                                        <td width="20%">
                                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit<?php echo $data['kd_canv'] ?>">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_canv'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <?php 
                                                            if($data['response_canv']=="Negatif"){

                                                             ?>
                                                             <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#reason<?php echo $data['kd_canv'] ?>">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                             <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
    </div>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Data Canvasing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="canvasing_tampil.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_potpen" id="select" class="form-control">
                                    <?php 
                                        if($_SESSION['level']=="Sales"){
                                            $query = mysqli_query($db,"SELECT potpen.kd_cust, potpen.kd_potpen, nm_cust FROM cust, potpen WHERE potpen.kd_cust=cust.kd_cust AND cust.kd_pengguna='$_SESSION[kd_pengguna]'")or die(mysqli_error($db));
                                        }else{
                                            $query = mysqli_query($db,"SELECT potpen.kd_cust, potpen.kd_potpen, nm_cust FROM cust, potpen WHERE potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
                                        }
                                        while ($data = mysqli_fetch_array($query)) {

                                     ?>
                                    <option value="<?php echo $data['kd_potpen'] ?>"><?php echo $data['nm_cust'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Contact Event Number</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <!-- AUTO GENERATE -->
                                <?php 
                                // $date = mysqli_query($db,"SELECT tgl_potpen FROM potpen");
                                // $datadate = mysqli_fetch_assoc($date);
                                // $datee = $datadate['tgl_potpen'];
                                $datee = date("Y-m-d");
                                $tahun = substr($datee, 0, 4);
                                $bulan = substr($datee, 5, 2);
                                $tanggal = substr($datee, 8, 2);
                                $inisial = mysqli_query($db,"SELECT inisial FROM pengguna WHERE kd_pengguna='$kd_pengguna'");
                                $datainisial = mysqli_fetch_assoc($inisial);
                                // $inisiall = $datainisial['inisial'];
                                $inisiall = substr($datainisial['inisial'], 0);
                                $kdcanv = mysqli_fetch_assoc(mysqli_query($db,"SELECT kd_canv FROM canvasing ORDER BY kd_canv DESC LIMIT 1"));
                                $datakdcanv = $kdcanv['kd_canv'];
                                $cenn = $tahun.$bulan.$tanggal.$inisiall.sprintf("%03s", $datakdcanv+1);

                                 ?>
                                <input type="text" placeholder="Masukkan Contract Event Number" class="form-control" name="cen" value="<?php echo $cenn ?>" readonly>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Progress</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" value="Initial Meeting" class="form-control" name="prog_canv" readonly>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tanggal Progress</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="tgl_canv" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Response Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <div class="form-check-inline form-check">
                                    <select name="response_canv" id="resp" class="form-control">
                                        <option value="Positif">Positif</option>
                                        <option value="Negatif">Negatif</option>
                                    </select>
                                </div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Reason For Negatif?</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="reason_neg_canv" id="reason_canv" class="form-control" disabled>
                                    <option value="Technical Specification">Technical Specification</option>
                                    <option value="Price">Price</option>
                                    <option value="Administratif Requirement">Administratif Requirement</option>
                                    <option value="Customer Preference">Customer Preference</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL EDIT -->
    <?php
    $edit = mysqli_query($db,"SELECT kd_canv, canvasing.kd_potpen, cen, prog_canv, tgl_canv, response_canv, nm_cust FROM canvasing, cust, potpen WHERE canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($dataedit = mysqli_fetch_array($edit)){ 

    ?>
    <div class="modal fade" id="edit<?php echo $dataedit['kd_canv'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Edit Data Canvasing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="canvasing_tampil.php" method="POST">
                        <input type="hidden" name="kd_canv" value="<?php echo $dataedit['kd_canv'] ?>">
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Customer</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <select name="kd_potpen" id="select" class="form-control" readonly>
                                        <option value="<?php echo $dataedit['kd_potpen'] ?>"><?php echo $dataedit['nm_cust'] ?></option>
                                    </select>    
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Contact Event Number</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" class="form-control" name="cen" value="<?php echo $dataedit['cen'] ?>" readonly>    
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Progress</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" class="form-control" name="prog_canv" value="<?php echo $dataedit['prog_canv'] ?>" readonly>   
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Tanggal Progress</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="date" class="form-control" name="tgl_canv" value="<?php echo $dataedit['tgl_canv'] ?>" required>   
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Response Customer</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <select name="response_canv" id="respedit<?php echo $dataedit['kd_canv'] ?>" class="form-control">
                                        <option value="Negatif"<?php echo ($dataedit['response_canv']=="Negatif") ? "selected":"" ?>>Negatif</option>
                                        <option value="Positif"<?php echo ($dataedit['response_canv']=="Positif") ? "selected":"" ?>>Positif</option>
                                    </select>  
                                </div>
                            </div><br>
                            <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Reason For Negatif?</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="reason_neg_canv" id="reason_canvedit<?php echo $dataedit['kd_canv'] ?>" class="form-control" disabled>
                                    <option value="Technical Specification">Technical Specification</option>
                                    <option value="Price">Price</option>
                                    <option value="Administratif Requirement">Administratif Requirement</option>
                                    <option value="Customer Preference">Customer Preference</option>
                                </select>
                            </div>
                            <script type="text/javascript">
                                var sel<?php echo $dataedit['kd_canv'] ?> = document.getElementById("respedit<?php echo $dataedit['kd_canv'] ?>"), text<?php echo $dataedit['kd_canv'] ?> = document.getElementById("reason_canvedit<?php echo $dataedit['kd_canv'] ?>");

                                sel<?php echo $dataedit['kd_canv'] ?>.onchange = function(e) {
                                    text<?php echo $dataedit['kd_canv'] ?>.disabled = (sel<?php echo $dataedit['kd_canv'] ?>.value != "Negatif");
                                    text<?php echo $dataedit['kd_canv'] ?>.enabled = (sel<?php echo $dataedit['kd_canv'] ?>.value != "Positif");
                                };
                            </script>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="edit" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <!-- MODAL HAPUS -->
    <?php
    $hapus = mysqli_query($db,"SELECT kd_canv, canvasing.kd_potpen, cen, prog_canv, tgl_canv, response_canv, nm_cust FROM canvasing, cust, potpen WHERE canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($datahapus = mysqli_fetch_array($hapus)){ 

    ?>
<div class="modal fade" id="hapus<?php echo $datahapus['kd_canv'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Hapus Data Canvasing</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="canvasing_tampil.php" method="POST">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Yakin Ingin Menghapus Data Canvasing Atas Pelanggan <b><?php echo $datahapus['nm_cust'] ?>?</b></label>
                            <input type="hidden" name="kd_canv" value="<?php echo $datahapus['kd_canv'] ?>">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
    <?php } ?>

    <!-- MODAL REASON -->
    <?php 
    $reason = mysqli_query($db,"SELECT kd_canv, canvasing.kd_potpen, cen, prog_canv, tgl_canv, response_canv, nm_cust, reason_neg_canv FROM canvasing, cust, potpen WHERE canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($datareason = mysqli_fetch_assoc($reason)) {

     ?>
        <div class="modal fade" id="reason<?php echo $datareason['kd_canv'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Reason For Negative</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Alasan Cutomer <b><?php echo $datareason['nm_cust'] ?></b> Memiliki Respon Negatif Pada Tahap Canvasing Adalah Karna <b><?php echo $datareason['reason_neg_canv'] ?></b> </label>
                                    <input type="hidden" name="kd_canv" value="<?php echo $datahapus['kd_canv'] ?>">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
     <?php } ?>

    <!-- QUERY PROSES -->
    <?php 
    if ($_POST['simpan']) {
    $kd_potpen = $_POST['kd_potpen'];
    $cen = $_POST['cen'];
    $prog_canv = $_POST['prog_canv'];
    $tgl_canv = $_POST['tgl_canv'];
    $now = date('Y-m-d');
    $response_canv = $_POST['response_canv'];
    $validasi_tambah = "valid";
    if(isset($_POST['reason_neg_canv'])){
        $reason_neg_canv = $_POST['reason_neg_canv'];
    }
    //cek data sudah ada
    $cek = mysqli_query($db,"SELECT canvasing.kd_potpen FROM canvasing");
    while($datacek = mysqli_fetch_assoc($cek)){
        if ($kd_potpen==$datacek['kd_potpen']) {
            echo "<script>alert('Gagal! Data Customer Sudah Ada')</script>";
            echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
            $validasi_tambah = "tidak valid";
        }
    }
    if($tgl_canv > $now){
        echo "<script>alert('Gagal! Tanggal Pelaksanaan Progress tidak boleh setelah hari ini')</script>";
        echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
        $validasi_tambah = "tidak valid";
    }
    if($validasi_tambah!="tidak valid"){
        if(!isset($_POST['reason_neg_canv'])){ //jika tidak ada inputan alasan negatif
            $tambah = mysqli_query($db,"INSERT INTO canvasing VALUES('','$kd_potpen','$cen','$prog_canv','$tgl_canv','$response_canv','')")or die(mysqli_error($db));
            if ($response_canv=="Positif") {
                $tampildatapotpen = mysqli_query($db,"SELECT * FROM canvasing ORDER BY kd_canv DESC LIMIT 1")or die(mysqli_query($db));
                $datatampilpotpen = mysqli_fetch_array($tampildatapotpen);
                $datakd_canv = $datatampilpotpen['kd_canv'];
                $tambahproduct_pres = mysqli_query($db,"INSERT INTO product_pres VALUES('','$datakd_canv','Presentasi Produk','-','-','-','-')")or die(mysqli_query($db));
            }
        }else{ //jika ada inputan alasan negatif
            $tambah = mysqli_query($db,"INSERT INTO canvasing VALUES('','$kd_potpen','$cen','$prog_canv','$tgl_canv','$response_canv','$reason_neg_canv')")or die(mysqli_error($db));
        }
        if ($tambah) {
            echo "<script>alert('Berhasil Tambah Data Canvasing')</script>";
            echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
        }else{
            echo "<script>alert('Gagal Tambah Data Canvasing')</script>";
            echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
        }
    }
}elseif ($_POST['edit']) {
    $kd_canv = $_POST['kd_canv'];
    $kd_potpen = $_POST['kd_potpen'];
    $cen = $_POST['cen'];
    $prog_canv = $_POST['prog_canv'];
    $tgl_canv = $_POST['tgl_canv'];
    $now = date('Y-m-d');
    $response_canv = $_POST['response_canv'];
    if (isset($_POST['reason_neg_canv'])) {
        $reason_neg_canv = $_POST['reason_neg_canv'];
    }
    

   if($tgl_canv>$now){
        echo "<script>alert('Gagal! Tanggal Progress tidak boleh setalah tanggal sekarang')</script>";
        echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
   }else{
        $cekproductpres = mysqli_query($db,"SELECT * FROM product_pres WHERE kd_canv='$kd_canv'");
        $datacekproductpres = mysqli_num_rows($cekproductpres);
        if ($response_canv=="Negatif") { //jika response customer negatif
            //menghapus data di form setelahnya, yaitu di table product_pres
            $hapuspresentation = mysqli_query($db,"DELETE FROM product_pres WHERE kd_canv='$kd_canv'");
            // mengedit data dengan data reason
            $editcanv = mysqli_query($db,"UPDATE canvasing SET kd_potpen='$kd_potpen', cen='$cen', prog_canv='$prog_canv', tgl_canv='$tgl_canv', response_canv='$response_canv', reason_neg_canv='$reason_neg_canv' WHERE kd_canv='$kd_canv'");
            //mengedit jika response customer positif, dan data tidak ada di form setelahnya
            //atau tidak ada di table product_pres
        }elseif($response_canv=="Positif" && $datacekproductpres<1){
            //menambahkan data di form selanjunya, atau di table product_pres
            $tambahpresentation = mysqli_query($db,"INSERT INTO product_pres VALUES('','$kd_canv','Presentasi Produk','-','-','-','-')")or die(mysqli_error($db));
            // mengedit data tanpa data reason
            $editcanv = mysqli_query($db,"UPDATE canvasing SET kd_potpen='$kd_potpen', cen='$cen', prog_canv='$prog_canv', tgl_canv='$tgl_canv', response_canv='$response_canv', reason_neg_canv='' WHERE kd_canv='$kd_canv'");
        }elseif ($response_canv=="Positif" && $datacekproductpres>0) {
            //mengedit ketika data di form selanjutnya masih ada, atau di table product_pres
            $editcanv = mysqli_query($db,"UPDATE canvasing SET kd_potpen='$kd_potpen', cen='$cen', prog_canv='$prog_canv', tgl_canv='$tgl_canv', response_canv='$response_canv', reason_neg_canv='' WHERE kd_canv='$kd_canv'");
        }

        if ($editcanv) {
            echo "<script>alert('Berhasil Edit Data Canvasing')</script>";
            echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
        }else{
            echo "<script>alert('Gagal Edit Data Canvasing')</script>";
            echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
        }
    }
}elseif ($_POST['hapus']) {
    $kd_canv = $_POST['kd_canv'];

    $hapus = mysqli_query($db,"DELETE FROM canvasing WHERE kd_canv='$kd_canv'")or die(mysqli_error($db));

    if ($hapus) {
    echo "<script>alert('Berhasil Menghapus Data')</script>";
    echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
    }else{
        echo "<script>alert('Gagal Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="canvasing_tampil.php?halaman=canvasing"</script>';
    }
}

     ?>
    <!-- DISABLE ENABLE REASON CUSTOMER NEGATIF -->
I     <script type="text/javascript">
        var sel = document.getElementById("resp"), text = document.getElementById("reason_canv");

        sel.onchange = function(e) {
            text.disabled = (sel.value != "Negatif");
        };
        function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->