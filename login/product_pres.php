<?php 
include '../config.php';
include 'fungsi_tgl.php';
error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Product Presentation</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php"; ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Product Presentation</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <?php 
                                                if($_SESSION['level']=="Administrator"){

                                                 ?>
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <?php } ?>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-top-campaign" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Nama Customer</th>
                                                        <th>Tahap Progress</th>
                                                        <th>Meeting Plan Date</th>
                                                        <th>Actual Meeting Date</th>
                                                        <th>Response Customer</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    if($_SESSION['level']=="Sales"){
                                                        $query = mysqli_query($db,"SELECT nm_cust, kd_product_pres, product_pres.kd_canv, progres_product_pres, meeting_plan_date, actual_meeting_date, response_product_pres FROM product_pres, canvasing, potpen, cust WHERE product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND cust.kd_pengguna='$_SESSION[kd_pengguna]'")or die(mysqli_error($db));
                                                    }else{
                                                        $query = mysqli_query($db,"SELECT nm_cust, kd_product_pres, product_pres.kd_canv, progres_product_pres, meeting_plan_date, actual_meeting_date, response_product_pres FROM product_pres, canvasing, potpen, cust WHERE product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
                                                    }
                                                        $no=0;
                                                        while ($data = mysqli_fetch_assoc($query)) {
                                                            
                                                            $no++;

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <td><?php echo $data['progres_product_pres'] ?></td>
                                                        <td><?php echo format_indo($data['meeting_plan_date']) ?></td>
                                                        <td><?php echo format_indo($data['actual_meeting_date']) ?></td>
                                                        <td><?php echo $data['response_product_pres'] ?></td>
                                                        <td width="10%">
                                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit<?php echo $data['kd_product_pres'] ?>">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <?php 
                                                            if($data['response_product_pres']=="Negatif"){

                                                             ?>
                                                             <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#reason<?php echo $data['kd_product_pres'] ?>">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                             <?php } ?>
                                                             <?php 
                                                             if($_SESSION['level']=="Administrator"){

                                                              ?>
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_product_pres'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
    </div>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Edit Data Product Presentation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="product_pres.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_canv" id="select" class="form-control">
                                    <?php 

                                        $cust = mysqli_query($db,"SELECT nm_cust, canvasing.kd_canv FROM canvasing, potpen, cust WHERE canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND canvasing.response_canv='Positif'")or die(mysqli_error($db));
                                        while ($datacust = mysqli_fetch_array($cust)) {

                                     ?>
                                    <option value="<?php echo $datacust['kd_canv'] ?>"><?php echo $datacust['nm_cust'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tahap Progress</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="progres_product_pres" value="Presentasi Produk" readonly>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Meeting Plan Date</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="meeting_plan_date" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Actual Meeting Date</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="actual_meeting_date" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Respon Customer</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="response_product_pres" id="respedit" class="form-control">
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Reason For Negatif?</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="reason_neg_propres" id="reason_propesedit" class="form-control" disabled>
                                    <option value="Technical Specification">Technical Specification</option>
                                    <option value="Price">Price</option>
                                    <option value="Administratif Requirement">Administratif Requirement</option>
                                    <option value="Customer Preference">Customer Preference</option>
                                </select>
                            </div>
                            <script type="text/javascript">
                                var sel = document.getElementById("respedit"), text = document.getElementById("reason_propesedit");

                                sel.onchange = function(e) {
                                    text.disabled = (sel.value != "Negatif");
                                };
                            </script>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL EDIT -->
    <?php
    $edit = mysqli_query($db,"SELECT nm_cust, kd_product_pres, product_pres.kd_canv, progres_product_pres, meeting_plan_date, actual_meeting_date, response_product_pres FROM product_pres, canvasing, potpen, cust WHERE product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($dataedit = mysqli_fetch_array($edit)){ 

    ?>
    <div class="modal fade" id="edit<?php echo $dataedit['kd_product_pres'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Edit Data Product Presentation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="product_pres.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <input type="hidden" name="kd_product_pres" value="<?php echo $dataedit['kd_product_pres'] ?>">
                            <div class="col-12 col-md-8">
                                <select name="kd_canv" id="select" class="form-control" readonly>
                                    <option value="<?php echo $dataedit['kd_canv'] ?>"><?php echo $dataedit['nm_cust'] ?></option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tahap Progress</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="progres_product_pres" value="Presentasi Produk" value="<?php echo $dataedit['progres_product_pres'] ?>" readonly>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Meeting Plan Date</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="meeting_plan_date" value="<?php echo $dataedit['meeting_plan_date'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Actual Meeting Date</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="actual_meeting_date" value="<?php echo $dataedit['actual_meeting_date'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Respon Customer</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="response_product_pres" id="respedit<?php echo $dataedit['kd_product_pres'] ?>" class="form-control">
                                    <option value="Positif"<?php echo ($dataedit['response_product_pres']=="Positif") ? "selected":"" ?>>Positif</option>
                                    <option value="Negatif"<?php echo ($dataedit['response_product_pres']=="Negatif") ? "selected":"" ?>>Negatif</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Reason For Negatif?</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="reason_neg_propres" id="reason_propesedit<?php echo $dataedit['kd_product_pres'] ?>" class="form-control" <?php if($dataedit['response_product_pres']=="Positif"){ ?> disabled <?php } ?>>
                                    <option value="Technical Specification">Technical Specification</option>
                                    <option value="Price">Price</option>
                                    <option value="Administratif Requirement">Administratif Requirement</option>
                                    <option value="Customer Preference">Customer Preference</option>
                                </select>
                            </div>
                            <script type="text/javascript">
                                var sel<?php echo $dataedit['kd_product_pres'] ?> = document.getElementById("respedit<?php echo $dataedit['kd_product_pres'] ?>"), text<?php echo $dataedit['kd_product_pres'] ?> = document.getElementById("reason_propesedit<?php echo $dataedit['kd_product_pres'] ?>");

                                sel<?php echo $dataedit['kd_product_pres'] ?>.onchange = function(e) {
                                    text<?php echo $dataedit['kd_product_pres'] ?>.disabled = (sel<?php echo $dataedit['kd_product_pres'] ?>.value != "Negatif");
                                };
                            </script>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="edit" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <!-- MODAL HAPUS -->
    <?php
    $hapus = mysqli_query($db,"SELECT nm_cust, kd_product_pres, product_pres.kd_canv, progres_product_pres, meeting_plan_date, actual_meeting_date, response_product_pres FROM product_pres, canvasing, potpen, cust WHERE product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust")or die(mysqli_error($db));
    while ($datahapus = mysqli_fetch_array($hapus)){ 

    ?>
<div class="modal fade" id="hapus<?php echo $datahapus['kd_product_pres'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Hapus Data Product Presentasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="product_pres.php" method="POST">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Yakin Ingin Menghapus Data Product Presentasi <b><?php echo $datahapus['nm_cust'] ?>?</b></label>
                            <input type="hidden" name="kd_product_pres" value="<?php echo $datahapus['kd_product_pres'] ?>">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
    <?php } ?>

    <!-- MODAL REASON -->
    <?php 
    $reason = mysqli_query($db,"SELECT nm_cust, kd_product_pres, product_pres.kd_canv, progres_product_pres, meeting_plan_date, actual_meeting_date, response_product_pres, reason_neg_propres FROM product_pres, canvasing, potpen, cust WHERE product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust");
    while ($datareason = mysqli_fetch_assoc($reason)) {

     ?>
        <div class="modal fade" id="reason<?php echo $datareason['kd_product_pres'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Reason For Negative</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Alasan Cutomer <b><?php echo $datareason['nm_cust'] ?></b> Memiliki Respon Negatif Pada Tahap Product Presentation Adalah Karna <b><?php echo $datareason['reason_neg_propres'] ?></b> </label>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
     <?php } ?>

    <!-- QUERY PROSES -->
    <?php 
if ($_POST['simpan']) {
    $kd_canv = $_POST['kd_canv'];
    $progres_product_pres = $_POST['progres_product_pres'];
    $meeting_plan_date = $_POST['meeting_plan_date'];
    $actual_meeting_date = $_POST['actual_meeting_date'];
    $response_product_pres = $_POST['response_product_pres'];
    if (isset($_POST['reason_neg_propres'])) {
        $reason_neg_propres = $_POST['reason_neg_propres'];
    }
    
    $cek = mysqli_query($db,"SELECT kd_canv FROM product_pres");
    $cekdata = mysqli_fetch_assoc($cek);
    if ($kd_canv==$cekdata['kd_canv']) {
        echo "<script>alert('Data Potensi Customer Sudah Ada, Tambahkan Data Customer Lainnya')</script>";
        echo '<script type="text/javascript">window.location="product_pres.php?halaman=product_pres"</script>';
    }else{
        if(isset($_POST['reason_neg_propres'])){ //query jika ada inputan negatif(negatif)
            $tambah = mysqli_query($db,"INSERT INTO product_pres VALUES('','$kd_canv','$progres_product_pres','$meeting_plan_date','$actual_meeting_date','$response_product_pres','$reason_neg_propres')")or die(mysqli_error($db));
        }else{ //query jika tidak ada inputan negatif(positif)
            $tambah = mysqli_query($db,"INSERT INTO product_pres(kd_product_pres, kd_canv, progres_product_pres, meeting_plan_date, actual_meeting_date, response_product_pres) VALUES('','$kd_canv','$progres_product_pres','$meeting_plan_date','$actual_meeting_date','$response_product_pres')")or die(mysqli_error($db));
            // tambah data ke solution design
            $terakhir = mysqli_query($db,"SELECT * FROM product_pres ORDER BY kd_product_pres DESC LIMIT 1");
            $dataterakhir = mysqli_fetch_array($terakhir);
            $tambahsolutiondesign = mysqli_query($db,"INSERT INTO solution_design VALUES('','$dataterakhir[kd_product_pres]','Solution Design','1','','','1','','')");
            // tambah ke create quotation
            $ruet = mysqli_query($db,"SELECT potpen.kd_potpen, product_pres.kd_product_pres, kd_det_pot, jns_layanan.kd_jns_layanan, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, potpen, jns_layanan, product_pres, canvasing WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_potpen=canvasing.kd_potpen AND canvasing.kd_canv=product_pres.kd_canv AND product_pres.response_product_pres='Positif' AND product_pres.kd_product_pres='$dataterakhir[kd_product_pres]'");
                while ($dataruet = mysqli_fetch_assoc($ruet)) {
                    //memanggil semua data di potensi, kemudian di tambahkan ke table create_quatation
                    $tambahquatation = mysqli_query($db,"INSERT INTO create_quatation VALUES('','$dataruet[kd_product_pres]','','$dataruet[kd_jns_layanan]','$dataruet[bw]','','$dataruet[biaya_sewa]','$dataruet[biaya_sewa]','','')");
                }
        }
        
        if ($tambah) {
            echo "<script>alert('Berhasil Tambah Data Product Presentation')</script>";
            echo '<script type="text/javascript">window.location="product_pres.php?halaman=product_pres"</script>';
        }else{
            echo "<script>alert('Gagal Tambah Data Product Presentation')</script>";
            echo '<script type="text/javascript">window.location="product_pres.php?halaman=product_pres"</script>';
        }
    }
}elseif ($_POST['edit']) {
        $kd_product_pres = $_POST['kd_product_pres'];
        $kd_canv = $_POST['kd_canv'];
        $progres_product_pres = $_POST['progres_product_pres'];
        $meeting_plan_date = $_POST['meeting_plan_date'];
        $actual_meeting_date = $_POST['actual_meeting_date'];
        $response_product_pres = $_POST['response_product_pres'];
        if (isset($_POST['reason_neg_propres'])) {
            $reason_neg_propres = $_POST['reason_neg_propres'];
        }
        if ($meeting_plan_date>$actual_meeting_date) {
            echo "<script>alert('Gagal! Data renana meeting dan terlaksana meeting tidak relevan')</script>";
            echo '<script type="text/javascript">window.location="product_pres.php?halaman=product_pres"</script>';
        }else{

            // MENGEDIT TANPA REASON, LANJUT TAHAP SOLUTION DAN CREATE QUATATION
            if ($response_product_pres=="Positif") {
                $tambahsolutiondesign = mysqli_query($db,"INSERT INTO solution_design VALUES('','$kd_product_pres','Solution Design','1','-','-','1','-','-')")or die(mysqli_error($db));
                $editproductpres = mysqli_query($db,"UPDATE product_pres SET kd_canv='$kd_canv', progres_product_pres='$progres_product_pres', meeting_plan_date='$meeting_plan_date', actual_meeting_date='$actual_meeting_date', response_product_pres='$response_product_pres', reason_neg_propres='' WHERE kd_product_pres='$kd_product_pres'")or die(mysqli_error($db));
                $ruet = mysqli_query($db,"SELECT potpen.kd_potpen, product_pres.kd_product_pres, kd_det_pot, jns_layanan.kd_jns_layanan, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, potpen, jns_layanan, product_pres, canvasing WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_potpen=canvasing.kd_potpen AND canvasing.kd_canv=product_pres.kd_canv AND product_pres.response_product_pres='Positif' AND product_pres.kd_product_pres='$kd_product_pres'");
                while ($dataruet = mysqli_fetch_assoc($ruet)) {
                    //memanggil semua data di potensi, kemudian di tambahkan ke table create_quatation
                    $tambahquatation = mysqli_query($db,"INSERT INTO create_quatation VALUES('','$dataruet[kd_product_pres]','','$dataruet[kd_jns_layanan]','$dataruet[bw]','','$dataruet[biaya_sewa]','$dataruet[biaya_sewa]','','')");
                }
                
            }elseif ($response_product_pres=="Negatif") { //MENGEDIT DENGAN REASON, BERHENTI DISINI
                $hapussolutiondesign = mysqli_query($db,"DELETE FROM solution_design WHERE kd_product_pres='$kd_product_pres'")or die(mysqli_error($db));
                $editproductpres = mysqli_query($db,"UPDATE product_pres SET kd_canv='$kd_canv', progres_product_pres='$progres_product_pres', meeting_plan_date='$meeting_plan_date', actual_meeting_date='$actual_meeting_date', response_product_pres='$response_product_pres', reason_neg_propres='$reason_neg_propres' WHERE kd_product_pres='$kd_product_pres'")or die(mysqli_error($db));
                $dataruet = mysqli_fetch_assoc($ruet);
                    $hapusquatation = mysqli_query($db,"DELETE FROM create_quatation WHERE kd_product_pres='$kd_product_pres'")or die(mysqli_error($db));
            }
            
            if ($editproductpres) {
                echo "<script>alert('Berhasil Edit Data Product Presentasi')</script>";
                echo '<script type="text/javascript">window.location="product_pres.php?halaman=product_pres"</script>';
            }else{
                echo "<script>alert('Gagal Edit Data Product Presentasi')</script>";
                echo '<script type="text/javascript">window.location="product_pres.php?halaman=product_pres"</script>';
            }
        } //ELSE PENGKONDISIAN RELEVAN TGL PLAN MEETING & ACTUAL MEETING
    }elseif ($_POST['hapus']) {
        $kd_product_pres = $_POST['kd_product_pres'];

        $hapus = mysqli_query($db,"DELETE FROM product_pres WHERE kd_product_pres='$kd_product_pres'")or die(mysqli_error($db));

        if ($hapus) {
        echo "<script>alert('Berhasil Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="product_pres.php?halaman=product_pres"</script>';
        }else{
            echo "<script>alert('Gagal Menghapus Data')</script>";
            echo '<script type="text/javascript">window.location="product_pres.php?halaman=product_pres"</script>';
        }
    }

     ?>
     <script type="text/javascript">
         function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->