<?php 
include '../config.php';
include 'fungsi_rupiah.php';
include 'fungsi_tgl.php';

session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard</title>
    <!-- Fontfaces CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.2.0/css/font-awesome.min.css">
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <!-- MAIN MENU -->
        <?php include 'menu.php' ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        
                                    </div>
                                </div> -->
                            <div class="au-card recent-report">
                                <div class="au-card-inner">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <h2 class="title-1 m-b-25">Pelanggan Kontrak Berakhir H-6 Bulan</h2>
                                        </div>
                                        <div class="col-lg-4">
                                            <!-- <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control"> -->
                                        </div><br>
                                        
                                    </div>
                                        <div class="table-responsive table--no-card m-b-30">
                                            <!-- <table class="table table-top-campaign" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Customer</th>
                                                        <th>Jenis Layanan</th>
                                                        <th>Nilai Pekerjaan</th>
                                                        <th><a href="index.php?halaman=index&orderby=est_kont_berakhir&pola=<?php echo $polabaru ?>">Kontrak Berakhir</a></th>
                                                        <th>Berakhir Kontrak</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                        <?php 
                                                        $pelanggan = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust GROUP BY nm_cust");
                                                        while ($datapelanggan = mysqli_fetch_assoc($pelanggan)) {
                                                            $byklyn = mysqli_query($db,"SELECT nm_cust, kd_det_pot, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND detail_potpen.kd_potpen='$datapelanggan[kd_potpen]'");
                                                            $banyaklyn = mysqli_num_rows($byklyn); //mencari banyaknya layanan per pelanggan
                                                            //query dan proses untuk mencari rowspan
                                                            $row = mysqli_query($db,"SELECT nm_cust, kd_det_pot, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND detail_potpen.kd_potpen='$datapelanggan[kd_potpen]'");
                                                            $noww = new DateTime();
                                                            $no = 0;
                                                            while ($datarow = mysqli_fetch_assoc($row)) {
                                                                $cobaa = new DateTime($datarow['est_kont_berakhir']); //mengubah format est_kont_berakhir menjadi datetime
                                                                $difff = $noww->diff($cobaa); //menghitung selisih
                                                                $bulann = $difff->m; //mengambil selisih dalam bulan
                                                                $tahunn = $difff->y; //mengambil selisih dalam tahun
                                                                $tanggall = $difff->d; //mengambil selisih dalam tanggal
                                                                if($tahunn==0 && $bulann<=6){
                                                                    $no++;
                                                                }
                                                            }

                                                         ?>
                                                     <tr>
                                                        <td rowspan="<?php echo $banyaklyn-($banyaklyn-$no) ?>"><?php echo $datapelanggan['nm_cust'] ?></td>
                                                        <?php 
                                                        $layanan = mysqli_query($db,"SELECT nm_cust, kd_det_pot, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND detail_potpen.kd_potpen='$datapelanggan[kd_potpen]'");
                                                        $now = new DateTime(); //mengambil tanggal sekarang lengkap dengan jam
                                                        while ($datalayanan = mysqli_fetch_assoc($layanan)) {
                                                            $coba = new DateTime($datalayanan['est_kont_berakhir']); //mengubah format est_kont_berakhir menjadi datetime
                                                            $diff = $now->diff($coba); //menghitung selisih
                                                            $bulan = $diff->m; //mengambil selisih dalam bulan
                                                            $tahun = $diff->y; //mengambil selisih dalam tahun
                                                            $tanggal = $diff->d; //mengambil selisih dalam tanggal
                                                            if($tahun==0 && $bulan<=6){

                                                         ?>
                                                        <td class="text-left"><?php echo $datalayanan['nm_jns_layanan'] ?></td>
                                                        <td class="text-left"><?php echo $datalayanan['biaya_sewa'] ?></td>
                                                        <td class="text-left"><?php echo $datalayanan['est_kont_berakhir'] ?></td>
                                                        <td style="color: red">
                                                            <?php 
                                                            if($tahun>0){
                                                                echo $tahun." Tahun, ";
                                                            }
                                                            if($bulan>0){
                                                                echo $bulan." Bulan, ";
                                                            }
                                                            if($tanggal>0){
                                                                echo $tanggal." Hari.";
                                                            }

                                                             ?>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php 
                                                            }
                                                        }

                                                     ?>
                                                </tbody>
                                            </table> -->
                                            <?php $tgl = date('Y-m-d'); ?>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <a href="cetakdashboard.php?now=<?php echo $tgl; ?>" class="btn btn-primary" target="_blank">
                                                        <i class="fa fa-print"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-4">
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    <input style="border-radius: 15px" type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                                </div>
                                            </div>
                                            <?php 
                                            $pola = "asc";
                                            $polabaru = "asc";
                                            if($_SESSION['level']=="Sales"){
                                                $query1 = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND est_kont_berakhir>'$_GET[now]' AND cust.kd_pengguna='$_SESSION[kd_pengguna]'");
                                            }else{
                                                $query1 = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND est_kont_berakhir>'$_GET[now]'");
                                            }
                                            if(isset($_GET['orderby'])){
                                                $orderby = $_GET['orderby'];
                                                $pola = $_GET['polabaru'];
                                                
                                                if($_SESSION['level']=="Sales"){
                                                    $query1 = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND est_kont_berakhir>'$_GET[now]' AND cust.kd_pengguna='$_SESSION[kd_pengguna]' ORDER BY $orderby $pola");    
                                                    if ($pola=="asc") {
                                                        $polabaru="desc";
                                                    }else{
                                                        $polabaru="asc";
                                                    }
                                                }else{
                                                    $query1 = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND est_kont_berakhir>'$_GET[now]' ORDER BY $orderby $pola");
                                                    if ($pola=="asc") {
                                                        $polabaru="desc";
                                                    }else{
                                                        $polabaru="asc";
                                                    }
                                                }
                                            }

                                             ?>
                                            <table class="table table-top-campaign" id="table"">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Customer</th>
                                                        <th>Jenis Layanan</th>
                                                        <th>Nilai Pekerjaan <a href="index.php?halaman=index&orderby=biaya_sewa&polabaru=<?php echo $polabaru ?>&now=<?php echo $tgl; ?>"><li class="fa fa-sort-amount-asc"></li></a></th>
                                                        <th>Kontrak Berakhir <a href="index.php?halaman=index&orderby=est_kont_berakhir&polabaru=<?php echo $polabaru ?>&now=<?php echo $tgl; ?>"><li class="fa fa-sort-amount-asc"></li></a></th>
                                                        <th>Jangka Waktu</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    // $query = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust");
                                                    while ($data = mysqli_fetch_assoc($query1)) {
                                                        $coba = new DateTime($data['est_kont_berakhir']); //mengubah format est_kont_berakhir menjadi datetime
                                                        $diff = $now->diff($coba); //menghitung selisih
                                                        $bulan = $diff->m; //mengambil selisih dalam bulan
                                                        $tahun = $diff->y; //mengambil selisih dalam tahun
                                                        $tanggal = $diff->d; //mengambil selisih dalam tanggal
                                                        $tinggil = $tanggal+1;
                                                        if($tahun==0 && $bulan<=6){

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <td><?php echo $data['nm_jns_layanan'] ?></td>
                                                        <td><?php echo rupiah($data['biaya_sewa']) ?></td>
                                                        <td><?php echo format_indo($data['est_kont_berakhir']) ?></td>
                                                        <td>
                                                            <?php 
                                                            if($tahun>0){
                                                                echo $tahun." Tahun, ";
                                                            }
                                                            if($bulan>0){
                                                                echo $bulan." Bulan, ";
                                                            }
                                                            if($tanggal>0){
                                                                echo $tinggil." Hari.";
                                                            }else{
                                                                echo "NOW!";
                                                            }

                                                             ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                        }
                                                    }
                                                     ?>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
                                        
                                    
                                
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>
    <script type="text/javascript">
         function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>

    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->