<?php 
include '../config.php';
error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Customer Response</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php"; ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Customer Response</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-top-campaign" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Customer</th>
                                                        <th>Response Customer</th>
                                                        <th>Reason Response Negatif</th>
                                                        <th>Procurement Method</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    if($_SESSION['level']=="Sales"){
                                                        $query = mysqli_query($db,"SELECT kd_response, submit_quot.kd_subm_quat, nm_cust, cust_resp, negatif, customer_response.procurement_method FROM customer_response, submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE customer_response.kd_subm_quat=submit_quot.kd_subm_quat AND submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND cust.kd_pengguna='$_SESSION[kd_pengguna]' GROUP BY nm_cust")or die(mysqli_error($db));   
                                                    }else{
                                                        $query = mysqli_query($db,"SELECT kd_response, submit_quot.kd_subm_quat, nm_cust, cust_resp, negatif, customer_response.procurement_method FROM customer_response, submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE customer_response.kd_subm_quat=submit_quot.kd_subm_quat AND submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
                                                    }
                                                        $no=0;
                                                        while ($data = mysqli_fetch_assoc($query)) {
                                                            
                                                            $no++;

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <td><?php echo $data['cust_resp'] ?></td>
                                                        <td><?php echo $data['negatif'] ?></td>
                                                        <td><?php echo $data['procurement_method'] ?></td>
                                                        <td width="10%">
                                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit<?php echo $data['kd_response'] ?>">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_response'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
    </div>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Create Customer Response</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="customer_response.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_subm_quat" id="select" class="form-control">
                                    <?php 
                                        if($_SESSION['level']=="Sales"){
                                            $query = mysqli_query($db,"SELECT create_quatation.kd_product_pres, nm_cust FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND cust.kd_pengguna='$_SESSION[kd_pengguna]' GROUP BY nm_cust")or die(mysqli_error($db));    
                                        }else{
                                            $query = mysqli_query($db,"SELECT create_quatation.kd_product_pres, nm_cust FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
                                        }
                                        while ($data = mysqli_fetch_array($query)) {

                                     ?>
                                    <option value="<?php echo $data['kd_product_pres'] ?>"><?php echo $data['nm_cust'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer Response</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="cust_resp" id="cust_resp" class="form-control">
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Reason For Negatif?</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="negatif" id="negatif" class="form-control" disabled>
                                    <option value="Technical Specification">Technical Specification</option>
                                    <option value="Price">Price</option>
                                    <option value="Administratif Requirement">Administratif Requirement</option>
                                    <option value="Customer Preference">Customer Preference</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Procurement Method</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="procurement_method" id="procurement_method" class="form-control">
                                    <option value="Lelang">Lelang</option>
                                    <option value="Pembelian Langsung">Pembelian Langsung</option>
                                    <option value="E-Katalog">E-Katalog</option>
                                </select>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL EDIT -->
    <?php
    $edit = mysqli_query($db,"SELECT kd_response, submit_quot.kd_subm_quat, nm_cust, cust_resp, negatif, customer_response.procurement_method FROM customer_response, submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE customer_response.kd_subm_quat=submit_quot.kd_subm_quat AND submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
    while ($dataedit = mysqli_fetch_array($edit)){ 

    ?>
    <div class="modal fade" id="edit<?php echo $dataedit['kd_response'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Create Customer Response</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="customer_response.php" method="POST">
                        <input type="hidden" name="kd_response" value="<?php echo $dataedit['kd_response'] ?>">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_subm_quat" id="select" class="form-control" disabled>
                                    <option value="<?php echo $dataedit['kd_subm_quat'] ?>"><?php echo $dataedit['nm_cust'] ?></option>
                                </select>
                                <input type="hidden" name="kd_subm_quat" value="<?php echo $dataedit['kd_subm_quat'] ?>">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer Response</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="cust_resp" id="cust_resp<?php echo $dataedit['kd_response'] ?>" class="form-control">
                                    <option value="Positif"<?php echo ($dataedit['cust_resp']=="Positif") ? "selected":"" ?>>Positif</option>
                                    <option value="Negatif"<?php echo ($dataedit['cust_resp']=="Negatif") ? "selected":"" ?>>Negatif</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Reason For Negatif?</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="negatif" id="negatif<?php echo $dataedit['kd_response'] ?>" class="form-control" <?php if($dataedit['cust_resp']=="Positif"){ ?> disabled <?php } ?>>
                                    <option value="Technical Specification"<?php echo ($dataedit['negatif']=="Technical Specification") ? "selected":"" ?>>Technical Specification</option>
                                    <option value="Price"<?php echo ($dataedit['negatif']=="Price") ? "selected":"" ?>>Price</option>
                                    <option value="Administratif Requirement"<?php echo ($dataedit['negatif']=="Administratif Requirement") ? "selected":"" ?>>Administratif Requirement</option>
                                    <option value="Customer Preference"<?php echo ($dataedit['negatif']=="Customer Preference") ? "selected":"" ?>>Customer Preference</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Procurement Method</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="procurement_method" id="procurement_method<?php echo $dataedit['kd_response'] ?>" class="form-control" <?php if($dataedit['cust_resp']=="Negatif"){ ?> disabled <?php } ?>>
                                    <option value="Lelang"<?php echo ($dataedit['procurement_method']=="Lelang") ? "selected":"" ?>>Lelang</option>
                                    <option value="Pembelian Langsung"<?php echo ($dataedit['procurement_method']=="Pembelian Langsung") ? "selected":"" ?>>Pembelian Langsung</option>
                                    <option value="E-Katalog"<?php echo ($dataedit['procurement_method']=="E-Katalog") ? "selected":"" ?>>E-Katalog</option>
                                </select>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="edit" class="btn btn-primary" value="Change">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <!-- MODAL HAPUS -->
    <?php
    $hapus = mysqli_query($db,"SELECT kd_response, submit_quot.kd_subm_quat, nm_cust, cust_resp, negatif, customer_response.procurement_method FROM customer_response, submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE customer_response.kd_subm_quat=submit_quot.kd_subm_quat AND submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
    while ($datahapus = mysqli_fetch_array($hapus)){ 

    ?>
        <div class="modal fade" id="hapus<?php echo $datahapus['kd_response'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Delete Customer Response</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="customer_response.php" method="POST">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Yakin Ingin Menghapus Data Submit Quotation Atas Pelanggan <b><?php echo $datahapus['nm_cust'] ?>?</b></label>
                                    <input type="hidden" name="kd_response" value="<?php echo $datahapus['kd_response'] ?>">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- QUERY PROSES -->
    <?php 
    // TAK BERFUNGSI
    if ($_POST['simpan']) {
    $kd_subm_quat = $_POST['kd_subm_quat'];
    $cust_resp = $_POST['cust_resp'];
    if(isset($_POST['negatif'])){
        $negatif = $_POST['negatif'];
    }else{
        $negatif = "";
    }
    if(isset($_POST['procurement_method'])){
        $procurement_method = $_POST['procurement_method'];
    }else{
        $procurement_method = "";
    }
    $validasi_tambah = "valid";
    

    $cek = mysqli_query($db,"SELECT kd_subm_quat FROM customer_response");
    while ($datacek = mysqli_fetch_assoc($cek)) {
        if($datacek['kd_subm_quat']==$kd_subm_quat){
            echo "<script>alert('Data Customer Sudah Ada, Mohon Masukkan Data Customer Yang Berbeda')</script>";
            echo '<script type="text/javascript">window.location="customer_response.php?halaman=customer_response"</script>';
            $validasi_tambah = "tidak valid";
        }
    }
    if ($validasi_tambah!="tidak valid") {

        $tambah = mysqli_query($db,"INSERT INTO customer_response VALUES('','$kd_subm_quat','$cust_resp','$negatif','$procurement_method')")or die(mysqli_error($db));

        if ($tambah) {
            echo "<script>alert('Berhasil Tambah Data')</script>";
            echo '<script type="text/javascript">window.location="customer_response.php?halaman=customer_response"</script>';
        }else{
            echo "<script>alert('Gagal Tambah Data')</script>";
            echo '<script type="text/javascript">window.location="customer_response.php?halaman=customer_response"</script>';
        }
    }
}elseif ($_POST['edit']) {
    $kd_response = $_POST['kd_response'];
    $kd_subm_quat = $_POST['kd_subm_quat'];
    $cust_resp = $_POST['cust_resp'];
    if(isset($_POST['negatif'])){
        $negatif = $_POST['negatif'];
    }else{
        $negatif = "";
    }
    if(isset($_POST['procurement_method'])){
        $procurement_method = $_POST['procurement_method'];
    }else{
        $procurement_method = "";
    }

    $edit = mysqli_query($db,"UPDATE customer_response SET kd_subm_quat='$kd_subm_quat', cust_resp='$cust_resp', negatif='$negatif', procurement_method='$procurement_method' WHERE kd_response='$kd_response'")or die(mysqli_error($db));

    if ($edit) {
        echo "<script>alert('Berhasil Edit Data')</script>";
        echo '<script type="text/javascript">window.location="customer_response.php?halaman=customer_response"</script>';
    }else{
        echo "<script>alert('Gagal Tambah Data')</script>";
        echo '<script type="text/javascript">window.location="customer_response.php?halaman=customer_response"</script>';
    }
}elseif ($_POST['hapus']) {
    $kd_response = $_POST['kd_response'];

    $hapus = mysqli_query($db,"DELETE FROM customer_response WHERE kd_response='$kd_response'")or die(mysqli_error($db));

    if ($hapus) {
    echo "<script>alert('Berhasil Menghapus Data')</script>";
    echo '<script type="text/javascript">window.location="customer_response.php?halaman=customer_response"</script>';
    }else{
        echo "<script>alert('Gagal Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="customer_response.php?halaman=customer_response"</script>';
    }
}

     ?>
    <!-- DISABLE ENABLE REASON CUSTOMER NEGATIF -->
I     <script type="text/javascript">
        var cust_resp = document.getElementById("cust_resp"), negatif = document.getElementById("negatif"), procurement_method = document.getElementById("procurement_method");

        cust_resp.onchange = function(e) {
            negatif.disabled = (cust_resp.value == "Positif");
            procurement_method.disabled = (cust_resp.value == "Negatif");
        };

        // EDIT
        <?php 
        $ddedit = mysqli_query($db,"SELECT kd_response, submit_quot.kd_subm_quat, nm_cust, cust_resp, negatif, customer_response.procurement_method FROM customer_response, submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE customer_response.kd_subm_quat=submit_quot.kd_subm_quat AND submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
        while ($dataddedit = mysqli_fetch_assoc($ddedit)) {

         ?>
        var cust_resp<?php echo $dataddedit['kd_response'] ?> = document.getElementById("cust_resp<?php echo $dataddedit['kd_response'] ?>"), negatif<?php echo $dataddedit['kd_response'] ?> = document.getElementById("negatif<?php echo $dataddedit['kd_response'] ?>"), procurement_method<?php echo $dataddedit['kd_response'] ?> = document.getElementById("procurement_method<?php echo $dataddedit['kd_response'] ?>");

            cust_resp<?php echo $dataddedit['kd_response'] ?>.onchange = function(e) {
                negatif<?php echo $dataddedit['kd_response'] ?>.disabled = (cust_resp<?php echo $dataddedit['kd_response'] ?>.value == "Positif");
                procurement_method<?php echo $dataddedit['kd_response'] ?>.disabled = (cust_resp<?php echo $dataddedit['kd_response'] ?>.value == "Negatif");
            };
         <?php } ?>
         function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->