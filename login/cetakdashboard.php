<?php 
include '../config.php';
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Print Data Pelanggan Kontrak Berakhir H-6 Bulan</title>
	<style type="text/css">
		body{
			font-family: sans-serif;
		}
		table{
			margin: 20px auto;
			border-collapse: collapse;
		}
		table th,
		table td{
			border: 1px solid #3c3c3c;
			padding: 3px 8px;

		}
		a{
			background: blue;
			color: #fff;
			padding: 8px 10px;
			text-decoration: none;
			border-radius: 2px;
		}
	</style>
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />
    
	<?php
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Data Pelanggan Kontrak Berakhir H-6 Bulan.xls");
	?>
</head>
<body><br>
	<h1 align="center">Print Data Pelanggan Kontrak Berakhir H-6 Bulan</h1>                                                    
<!--                                                         <?php 
                                                        $pelanggan = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust GROUP BY nm_cust");
                                                        while ($datapelanggan = mysqli_fetch_assoc($pelanggan)) {
                                                            $byklyn = mysqli_query($db,"SELECT nm_cust, kd_det_pot, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND detail_potpen.kd_potpen='$datapelanggan[kd_potpen]'");
                                                            $banyaklyn = mysqli_num_rows($byklyn); //mencari banyaknya layanan per pelanggan
                                                            //query dan proses untuk mencari rowspan
                                                            $row = mysqli_query($db,"SELECT nm_cust, kd_det_pot, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND detail_potpen.kd_potpen='$datapelanggan[kd_potpen]'");
                                                            $noww = new DateTime();
                                                            $no = 0;
                                                            while ($datarow = mysqli_fetch_assoc($row)) {
                                                                $cobaa = new DateTime($datarow['est_kont_berakhir']); //mengubah format est_kont_berakhir menjadi datetime
                                                                $difff = $noww->diff($cobaa); //menghitung selisih
                                                                $bulann = $difff->m; //mengambil selisih dalam bulan
                                                                $tahunn = $difff->y; //mengambil selisih dalam tahun
                                                                $tanggall = $difff->d; //mengambil selisih dalam tanggal
                                                                if($tahunn==0 && $bulann<=6){
                                                                    $no++;
                                                                }
                                                            }

                                                         ?>
                                                     <tr>
                                                        <td rowspan="<?php echo $banyaklyn-($banyaklyn-$no) ?>"><?php echo $datapelanggan['nm_cust'] ?></td>
                                                        <?php 
                                                        $layanan = mysqli_query($db,"SELECT nm_cust, kd_det_pot, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND detail_potpen.kd_potpen='$datapelanggan[kd_potpen]'");
                                                        $now = new DateTime(); //mengambil tanggal sekarang lengkap dengan jam
                                                        while ($datalayanan = mysqli_fetch_assoc($layanan)) {
                                                            $coba = new DateTime($datalayanan['est_kont_berakhir']); //mengubah format est_kont_berakhir menjadi datetime
                                                            $diff = $now->diff($coba); //menghitung selisih
                                                            $bulan = $diff->m; //mengambil selisih dalam bulan
                                                            $tahun = $diff->y; //mengambil selisih dalam tahun
                                                            $tanggal = $diff->d; //mengambil selisih dalam tanggal
                                                            if($tahun==0 && $bulan<=6){

                                                         ?>
                                                        <td class="text-left"><?php echo $datalayanan['nm_jns_layanan'] ?></td>
                                                        <td class="text-left"><?php echo $datalayanan['biaya_sewa'] ?></td>
                                                        <td class="text-left"><?php echo $datalayanan['est_kont_berakhir'] ?></td>
                                                        <td style="color: red">
                                                            <?php 
                                                            if($tahun>0){
                                                                echo $tahun." Tahun, ";
                                                            }
                                                            if($bulan>0){
                                                                echo $bulan." Bulan, ";
                                                            }
                                                            if($tanggal>0){
                                                                echo $tanggal." Hari.";
                                                            }

                                                             ?>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php 
                                                            }
                                                        }

                                                     ?>
                                                </tbody>
                                            </table> -->
                                            <?php $tgl = date('Y-m-d'); ?>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    
                                                </div>
                                            </div>
                                            <?php 
                                            $pola = "asc";
                                            $polabaru = "asc";
                                            if($_SESSION['level']=="Sales"){
                                            	$query1 = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND est_kont_berakhir>'$_GET[now]' AND cust.kd_pengguna='$_SESSION[kd_pengguna]' ORDER BY est_kont_berakhir ASC");
                                            }else{
                                            	$query1 = mysqli_query($db,"SELECT nm_cust, kd_det_pot, detail_potpen.kd_potpen, nm_jns_layanan, bw, provider, biaya_sewa, est_kont_berakhir, est_mulai_pengadaan, metode_pengadaan FROM detail_potpen, jns_layanan, potpen, cust WHERE detail_potpen.kd_jns_layanan=jns_layanan.kd_jns_layanan AND detail_potpen.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND est_kont_berakhir>'$_GET[now]' ORDER BY est_kont_berakhir ASC");
                                            }

                                             ?>
                                            <table class="table table-top-campaign" id="table"">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Customer</th>
                                                        <th>Jenis Layanan</th>
                                                        <th>Nilai Pekerjaan</th>
                                                        <th>Kontrak Berakhir</th>
                                                        <th>Jangka Waktu</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    while ($data = mysqli_fetch_assoc($query1)) {
                                                        $coba = new DateTime($data['est_kont_berakhir']); //mengubah format est_kont_berakhir menjadi datetime
                                                        $diff = $now->diff($coba); //menghitung selisih
                                                        $bulan = $diff->m; //mengambil selisih dalam bulan
                                                        $tahun = $diff->y; //mengambil selisih dalam tahun
                                                        $tanggal = $diff->d; //mengambil selisih dalam tanggal
                                                        if($tahun==0 && $bulan<=6){

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <td><?php echo $data['nm_jns_layanan'] ?></td>
                                                        <td>Rp. <?php echo $data['biaya_sewa'] ?></td>
                                                        <td><?php echo $data['est_kont_berakhir'] ?></td>
                                                        <td style="color: red">
                                                            <?php 
                                                            if($tahun>0){
                                                                echo $tahun." Tahun, ";
                                                            }
                                                            if($bulan>0){
                                                                echo $bulan." Bulan, ";
                                                            }
                                                            if($tanggal>0){
                                                                echo $tanggal." Hari.";
                                                            }

                                                             ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                        }
                                                    }
                                                     ?>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
</body>
</html>