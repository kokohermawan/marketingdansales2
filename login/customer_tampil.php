<?php 
include '../config.php';
error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Customer</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php" ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Customer</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>
                                        

                                        <div class="table-responsive table--no-card m-b-30">
                                        <table class="table table-top-campaign" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Nama Customer</th>
                                                        <th>Alamat Customer</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                if($_SESSION['level']=="Sales"){
                                                    $query = mysqli_query($db, "SELECT kd_cust, nm_seg, nm_cust, alm_cust, latitude, longitude, pic_cust, no_telp, jabatan_pic, tgl_pelaksanaan_prospek FROM cust, segment_cust WHERE cust.kd_seg=segment_cust.kd_seg AND cust.kd_pengguna='$_SESSION[kd_pengguna]'")or die(mysqli_error($db));
                                                }else{
                                                    $query = mysqli_query($db, "SELECT kd_cust, nm_seg, nm_cust, alm_cust, latitude, longitude, pic_cust, no_telp, jabatan_pic, tgl_pelaksanaan_prospek FROM cust, segment_cust WHERE cust.kd_seg=segment_cust.kd_seg")or die(mysqli_error($db));
                                                }
                                                
                                                $no = 0;
                                                while ($data = mysqli_fetch_array($query)) {
                                                $no++;
                                                 ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><xmp><?php echo $data['nm_cust'] ?></xmp></td>
                                                        <td><xmp><?php echo $data['alm_cust'] ?></xmp></td>
                                                        <td>
                                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit<?php echo $data['kd_cust'] ?>">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_cust'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#detail<?php echo $data['kd_cust'] ?>">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Data Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="customer_tampil.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Nama Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Nama Customer" class="form-control" name="nm_cust" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Segment Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_seg" id="select" class="form-control">
                                    <?php 

                                        $query = mysqli_query($db,"SELECT * FROM segment_cust")or die(mysqli_error($db));
                                        while ($data = mysqli_fetch_array($query)) {

                                     ?>
                                    <option value="<?php echo $data['kd_seg'] ?>"><?php echo $data['nm_seg'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Alamat Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <textarea name="alm_cust" id="textarea-input" rows="4" placeholder="Masukkan Alamat Customer" class="form-control" required></textarea>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Koordinat Customer(Latitude)</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Koordinat Customer(Latitude)" class="form-control" name="latitude" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Koordinat Customer(Longitude)</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Koordinat Customer(Longitude)" class="form-control" name="longitude" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Nama PIC Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Nama PIC Customer" class="form-control" name="pic_cust" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">No. Telp PIC</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Nomor Telepon PIC" class="form-control" name="no_telp" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Jabatan PIC Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" placeholder="Masukkan Jabatan PIC Customer" class="form-control" name="jabatan_pic" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tanggal Pelaksanaan Prospek</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="date" placeholder="Masukkan Jabatan PIC Customer" class="form-control" name="tgl_pelaksanaan_prospek" required>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL DETAIL -->
    <?php 
    $detail = mysqli_query($db, "SELECT nm_pengguna, kd_cust, nm_seg, nm_cust, alm_cust, latitude, longitude, pic_cust, no_telp, jabatan_pic, tgl_pelaksanaan_prospek FROM pengguna, cust, segment_cust WHERE cust.kd_seg=segment_cust.kd_seg AND pengguna.kd_pengguna=cust.kd_pengguna")or die(mysqli_error($db));
    while($datadetail = mysqli_fetch_array($detail)){

     ?>
        <div class="modal fade" id="detail<?php echo $datadetail['kd_cust'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Data Detail Customer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table>
                            <tr>
                                <td>Sales Penginput</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><xmp><?php echo $datadetail['nm_pengguna'] ?></xmp></b></td>
                            </tr>
                            <tr>
                                <td>Nama Customer</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><xmp><?php echo $datadetail['nm_cust'] ?></xmp></b></td>
                            </tr>
                            <tr>
                                <td>Alamat Customer</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><xmp><?php echo $datadetail['alm_cust'] ?></xmp></b></td>
                            </tr>
                            <tr>
                                <td>Koordinat Customer (Latitude)</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><?php echo $datadetail['latitude'] ?></b></td>
                            </tr>
                            <tr>
                                <td>Koordinat Customer (Longitude)</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><?php echo $datadetail['longitude'] ?></b></td>
                            </tr>
                            <tr>
                                <td>PIC Customer</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><xmp><?php echo $datadetail['pic_cust'] ?></xmp></b></td>
                            </tr>
                            <tr>
                                <td>No. Telp</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><?php echo $datadetail['no_telp'] ?></b></td>
                            </tr>
                            <tr>
                                <td>Jabatan PIC</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><?php echo $datadetail['jabatan_pic'] ?></b></td>
                            </tr>
                            <tr>
                                <td>Tanggal Pelaksanaan Prospek</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><?php echo $datadetail['tgl_pelaksanaan_prospek'] ?></b></td>
                            </tr>
                            <tr>
                                <td>Segmen Cust</td>
                                <td align="center" style="width: 5%">:</td>
                                <td><b><?php echo $datadetail['nm_seg'] ?></b></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- MODAL EDIT -->
    <?php 
    $edit = mysqli_query($db, "SELECT kd_cust, cust.kd_seg, nm_seg, nm_cust, alm_cust, latitude, longitude, pic_cust, no_telp, jabatan_pic, tgl_pelaksanaan_prospek FROM cust, segment_cust WHERE cust.kd_seg=segment_cust.kd_seg")or die(mysqli_error($db));
    while ($dataedit = mysqli_fetch_array($edit)) {

     ?>
        <div class="modal fade" id="edit<?php echo $dataedit['kd_cust'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Edit Data Customer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="customer_tampil.php" method="POST">
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Nama Customer</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="hidden" name="kd_cust" value="<?php echo $dataedit['kd_cust'] ?>">
                                    <input type="text" placeholder="Masukkan Nama Customer" class="form-control" name="nm_cust" value="<?php echo $dataedit['nm_cust'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Segment Customer</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <select name="kd_seg" id="select" class="form-control">
                                        <?php 
                                            $segment = $dataedit['kd_seg'];
                                            $query = mysqli_query($db,"SELECT * FROM segment_cust")or die(mysqli_error($db));
                                            while ($data = mysqli_fetch_array($query)) {

                                         ?>
                                        <option value="<?php echo $data['kd_seg'] ?>"<?php echo ($segment==$data['kd_seg']) ? "selected":"" ?>><?php echo $data['nm_seg'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Alamat Customer</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <textarea name="alm_cust" id="textarea-input" rows="4" placeholder="Masukkan Alamat Customer" class="form-control" required><?php echo $dataedit['alm_cust'] ?></textarea>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Koordinat Customer(Latitude)</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" placeholder="Masukkan Koordinat Customer(Latitude)" class="form-control" name="latitude" value="<?php echo $dataedit['latitude'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Koordinat Customer(Longitude)</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" placeholder="Masukkan Koordinat Customer(Longitude)" class="form-control" name="longitude" value="<?php echo $dataedit['longitude'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Nama PIC Customer</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" placeholder="Masukkan Nama PIC Customer" class="form-control" name="pic_cust" value="<?php echo $dataedit['pic_cust'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">No. Telp PIC</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" placeholder="Masukkan Nomor Telepon PIC" class="form-control" name="no_telp" value="<?php echo $dataedit['no_telp'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Jabatan PIC Customer</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="text" placeholder="Masukkan Jabatan PIC Customer" class="form-control" name="jabatan_pic" value="<?php echo $dataedit['jabatan_pic'] ?>" required>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Tanggal Pelaksanaan Prospek</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <input type="date" placeholder="Masukkan Jabatan PIC Customer" class="form-control" name="tgl_pelaksanaan_prospek" value="<?php echo $dataedit['tgl_pelaksanaan_prospek'] ?>" required>
                                </div>
                            </div><br>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="edit" class="btn btn-primary" value="Change">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
     <?php } ?>
     <!-- MODAL HAPUS -->
    <?php 
        $hapus = mysqli_query($db, "SELECT kd_cust, cust.kd_seg, nm_seg, nm_cust, alm_cust, latitude, longitude, pic_cust, no_telp, jabatan_pic, tgl_pelaksanaan_prospek FROM cust, segment_cust WHERE cust.kd_seg=segment_cust.kd_seg")or die(mysqli_error($db));
        while ($datahapus = mysqli_fetch_array($hapus)) {

    ?>
        <div class="modal fade" id="hapus<?php echo $datahapus['kd_cust'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Hapus Data Expectation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="customer_tampil.php" method="POST">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Yakin Ingin Menghapus Data Customer <b><xmp><?php echo $datahapus['nm_cust'] ?></xmp> ?</b></label>
                                    <input type="hidden" name="kd_cust" value="<?php echo $datahapus['kd_cust'] ?>">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- PROSES -->
    <?php 
    if ($_POST['simpan']) {
        $nm_cust = $_POST['nm_cust'];
        $alm_cust = $_POST['alm_cust'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $pic_cust = $_POST['pic_cust'];
        $no_telp = $_POST['no_telp'];
        $jabatan_pic = $_POST['jabatan_pic'];
        $tgl_pelaksanaan_prospek = $_POST['tgl_pelaksanaan_prospek'];
        $now = date('Y-m-d');
        $kd_seg = $_POST['kd_seg'];

        $cek = mysqli_query($db,"SELECT nm_cust FROM cust");
        while ($datacek = mysqli_fetch_assoc($cek)) {
            if ($datacek['nm_cust']==$nm_cust) {
                echo "<script>alert('Gagal! Data Customer sudah ada')</script>";
                echo '<script type="text/javascript">window.location="customer_tampil.php?halaman=customer"</script>';
                $_SESSION['validasi_tambah']="tidak valid";
            }
        }
        if ($tgl_pelaksanaan_prospek>$now) {
            echo "<script>alert('Gagal! Tanggal Pelaksanaan Prospek tidak boleh setelah hari sekarang')</script>";
            echo '<script type="text/javascript">window.location="customer_tampil.php?halaman=customer"</script>';
            $_SESSION['validasi_tambah']="tidak valid";
        }
        if ($_SESSION['validasi_tambah']!="tidak valid") {

            $tambah = mysqli_query($db,"INSERT INTO cust VALUES('','$kd_seg','$kd_pengguna','$nm_cust','$alm_cust','$latitude','$longitude','$pic_cust','$no_telp','$jabatan_pic','$tgl_pelaksanaan_prospek')")or die(mysqli_error($db));

            if ($tambah) {
                echo "<script>alert('Berhasil Tambah Data Customer')</script>";
                echo '<script type="text/javascript">window.location="customer_tampil.php?halaman=customer"</script>';
            }else{
                echo "<script>alert('Gagal Tambah Data Customer')</script>";
                echo '<script type="text/javascript">window.location="customer_tampil.php?halaman=customer"</script>';
            }
        }
    }elseif ($_POST['edit']) {
        $kd_cust = $_POST['kd_cust'];
        $nm_cust = $_POST['nm_cust'];
        $alm_cust = $_POST['alm_cust'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $pic_cust = $_POST['pic_cust'];
        $no_telp = $_POST['no_telp'];
        $jabatan_pic = $_POST['jabatan_pic'];
        $tgl_pelaksanaan_prospek = $_POST['tgl_pelaksanaan_prospek'];
        $kd_seg = $_POST['kd_seg'];

        $edit = mysqli_query($db,"UPDATE cust SET kd_seg='$kd_seg', nm_cust='$nm_cust', alm_cust='$alm_cust', latitude='$latitude', longitude='$longitude', pic_cust='$pic_cust', no_telp='$no_telp', jabatan_pic='$jabatan_pic', tgl_pelaksanaan_prospek='$tgl_pelaksanaan_prospek' WHERE kd_cust='$kd_cust'")or die(mysqli_error($db));

        if ($edit) {
            echo "<script>alert('Berhasil Edit Data Customer')</script>";
            echo '<script type="text/javascript">window.location="customer_tampil.php?halaman=customer"</script>';
        }else{
            echo "<script>alert('Gagal Edit Data Customer')</script>";
            echo '<script type="text/javascript">window.location="customer_tampil.php?halaman=customer"</script>';
        }
    }elseif ($_POST['hapus']) {
        $kd_cust = $_POST['kd_cust'];

        $hapus = mysqli_query($db,"DELETE FROM cust WHERE kd_cust='$kd_cust'")or die(mysqli_error($db));

        if ($hapus) {
        echo "<script>alert('Berhasil Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="customer_tampil.php?halaman=customer"</script>';
        }else{
            echo "<script>alert('Gagal Menghapus Data')</script>";
            echo '<script type="text/javascript">window.location="customer_tampil.php?halaman=customer"</script>';
        }
    }

     ?>

     <script type="text/javascript">
         function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->