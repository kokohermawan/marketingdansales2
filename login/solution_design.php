<?php 
include '../config.php';
include 'fungsi_tgl.php';
error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Solution Design</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php"; ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Solution Design</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <?php 
                                                if($_SESSION['level']=="Administrator"){

                                                 ?>
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <?php } ?>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-top-campaign" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Nama Customer</th>
                                                        <!-- <th>Tahap Progress</th>
                                                        <th>Customer Expectation</th>
                                                        <th>Tindak Lanjut</th> -->
                                                        <th>Special Solution Design</th>
                                                        <th>Customer Solution</th>
                                                        <th>Expected Finish Date</th>
                                                        <!-- <th>Notes</th> -->
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    if($_SESSION['level']=="Sales"){
                                                        $query = mysqli_query($db,"SELECT kd_sol, nm_cust, progres_sol, nm_expec, tindak_lanjut, special_sol, nm_cusol, finish_date, notes FROM solution_design, product_pres, canvasing, potpen, cust, expec, cusol WHERE solution_design.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND solution_design.kd_expec=expec.kd_expec AND solution_design.kd_cusol=cusol.kd_cusol AND cust.kd_pengguna='$_SESSION[kd_pengguna]'")or die(mysqli_error($db));   
                                                    }else{
                                                        $query = mysqli_query($db,"SELECT kd_sol, nm_cust, progres_sol, nm_expec, tindak_lanjut, special_sol, nm_cusol, finish_date, notes FROM solution_design, product_pres, canvasing, potpen, cust, expec, cusol WHERE solution_design.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND solution_design.kd_expec=expec.kd_expec AND solution_design.kd_cusol=cusol.kd_cusol")or die(mysqli_error($db));
                                                    }
                                                        $no=0;
                                                        while ($data = mysqli_fetch_assoc($query)) {
                                                            
                                                            $no++;

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <!-- <td><?php echo $data['progres_sol'] ?></td>
                                                        <td><?php echo $data['nm_expec'] ?></td>
                                                        <td><?php echo $data['tindak_lanjut'] ?></td> -->
                                                        <td><?php echo $data['special_sol'] ?></td> 
                                                        <td><?php echo $data['nm_cusol'] ?></td>
                                                        <td><?php echo format_indo($data['finish_date']) ?></td>
                                                        <!-- <td><?php echo $data['notes'] ?></td> -->
                                                        <td width="15%">
                                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#detail<?php echo $data['kd_sol'] ?>">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit<?php echo $data['kd_sol'] ?>">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <?php 
                                                            if($_SESSION['level']=="Administrator"){

                                                             ?>
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_sol'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
    </div>
    <!-- MODAL DETAIL -->
    <?php 
    $detail = mysqli_query($db,"SELECT kd_sol, nm_cust, progres_sol, nm_expec, tindak_lanjut, special_sol, nm_cusol, finish_date, notes FROM solution_design, product_pres, canvasing, potpen, cust, expec, cusol WHERE solution_design.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND solution_design.kd_expec=expec.kd_expec AND solution_design.kd_cusol=cusol.kd_cusol")or die(mysqli_error($db));
    while ($datadetail = mysqli_fetch_assoc($detail)) {

     ?>
    <div class="modal fade" id="detail<?php echo $datadetail['kd_sol'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Data Detail Solution Design</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Customer</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['nm_cust'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Tahap Progress</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['progres_sol'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Expectation Customer</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['nm_expec'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Tindak Lanjut</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['tindak_lanjut'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Special Solution Design</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['special_sol'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Customer Solution</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['nm_cusol'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Expected Finish Date</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['finish_date'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-4">
                            <label class=" form-control-label">Notes</label>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="form-control-static">: <?php echo $datadetail['notes'] ?></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Data Solution Design</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="tes.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_product_pres" id="select" class="form-control">
                                    <?php 

                                        $cust = mysqli_query($db,"SELECT nm_cust, product_pres.kd_product_pres FROM product_pres, canvasing, potpen, cust WHERE product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND product_pres.response_product_pres='Positif'")or die(mysqli_error($db));
                                        while ($datacust = mysqli_fetch_array($cust)) {

                                     ?>
                                    <option value="<?php echo $datacust['kd_product_pres'] ?>"><?php echo $datacust['nm_cust'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tahap Progress</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="progres_sol" value="Solution Design" readonly>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer Expectation</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_expec" id="select" class="form-control">
                                    <?php 

                                        $expec = mysqli_query($db,"SELECT * FROM expec")or die(mysqli_error($db));
                                        while ($dataexpec = mysqli_fetch_array($expec)) {

                                     ?>
                                    <option value="<?php echo $dataexpec['kd_expec'] ?>"><?php echo $dataexpec['nm_expec'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tindak Lanjut</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="tindak_lanjut" id="select" class="form-control">
                                    <option value="Flag Need Follow Up">Flag Need Follow Up</option>
                                    <option value="Kunjungan Kembali">Kunjungan Kembali</option>
                                    <option value="Ajak Studi Banding">Ajak Studi Banding</option>
                                    <option value="Submit Proposal Teknis dan SHP">Submit Proposal Teknis dan SHP</option>
                                    <option value="Kunjungan Fasilitas ICON+">Kunjungan Fasilitas ICON+</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Special Solution Design</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="special_sol" id="respedit" class="form-control">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer Solution</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_cusol" id="reason_soldesedit" class="form-control">
                                    <?php 
                                    $cusol = mysqli_query($db,"SELECT * FROM cusol WHERE kd_cusol!='1'");
                                    while ($datacusol = mysqli_fetch_array($cusol)) {

                                     ?>
                                    <option value="<?php echo $datacusol['kd_cusol'] ?>"><?php echo $datacusol['nm_cusol'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <script type="text/javascript">
                            var sel = document.getElementById("respedit"), text = document.getElementById("reason_soldesedit");

                            sel.onchange = function(e) {
                                text.disabled = (sel.value != "Yes");
                            };
                        </script>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Expected Finish Date</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="finish_date" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Notes</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <textarea name="notes" id="textarea-input" rows="7" placeholder="Masukkan Notes" class="form-control" required></textarea>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL EDIT -->
    <?php
    $edit = mysqli_query($db,"SELECT kd_sol, nm_cust, solution_design.kd_product_pres, progres_sol, solution_design.kd_expec, nm_expec, tindak_lanjut, special_sol, solution_design.kd_cusol, nm_cusol, finish_date, notes FROM solution_design, product_pres, canvasing, potpen, cust, expec, cusol WHERE solution_design.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND solution_design.kd_expec=expec.kd_expec AND solution_design.kd_cusol=cusol.kd_cusol")or die(mysqli_error($db));
    while ($dataedit = mysqli_fetch_array($edit)){ 

    ?>
    <div class="modal fade" id="edit<?php echo $dataedit['kd_sol'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Edit Data Solution Design</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="solution_design.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="hidden" name="kd_sol" value="<?php echo $dataedit['kd_sol'] ?>">
                                <select name="kd_product_pres" id="select" class="form-control" readonly>
                                    <option value="<?php echo $dataedit['kd_product_pres'] ?>"><?php echo $dataedit['nm_cust'] ?></option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tahap Progress</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="progres_sol" value="Solution Design" readonly>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer Expectation</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_expec" id="select" class="form-control">
                                    <?php 

                                        $expec = mysqli_query($db,"SELECT * FROM expec")or die(mysqli_error($db));
                                        while ($dataexpec = mysqli_fetch_array($expec)) {

                                     ?>
                                    <option value="<?php echo $dataexpec['kd_expec'] ?>"<?php echo ($dataexpec['kd_expec']==$dataedit['kd_expec']) ? "selected":"" ?>><?php echo $dataexpec['nm_expec'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Tindak Lanjut</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="tindak_lanjut" id="select" class="form-control">
                                    <option value="Flag Need Follow Up"<?php echo ($dataedit['tindak_lanjut']=="Flag Need Follow Up") ? "selected":"" ?>>Flag Need Follow Up</option>
                                    <option value="Kunjungan Kembali"<?php echo ($dataedit['tindak_lanjut']=="Kunjungan Kembali") ? "selected":"" ?>>Kunjungan Kembali</option>
                                    <option value="Ajak Studi Banding"<?php echo ($dataedit['tindak_lanjut']=="Ajak Studi Banding") ? "selected":"" ?>>Ajak Studi Banding</option>
                                    <option value="Submit Proposal Teknis dan SHP"<?php echo ($dataedit['tindak_lanjut']=="Submit Proposal Teknis dan SHP") ? "selected":"" ?>>Submit Proposal Teknis dan SHP</option>
                                    <option value="Kunjungan Fasilitas ICON+"<?php echo ($dataedit['tindak_lanjut']=="Kunjungan Fasilitas ICON+") ? "selected":"" ?>>Kunjungan Fasilitas ICON+</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Special Solution Design</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="special_sol" id="respedit<?php echo $dataedit['kd_sol'] ?>" class="form-control">
                                    <option value="Yes"<?php echo ($dataedit['special_sol']=="Yes") ? "selected":"" ?>>Yes</option>
                                    <option value="No"<?php echo ($dataedit['special_sol']=="No") ? "selected":"" ?>>No</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer Solution</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_cusol" id="reason_soldesedit<?php echo $dataedit['kd_sol'] ?>" class="form-control" <?php if($dataedit['special_sol']=="No"){ ?> disabled <?php } ?>>
                                    <?php 
                                    $cusol = mysqli_query($db,"SELECT * FROM cusol");
                                    while ($datacusol = mysqli_fetch_array($cusol)) {

                                     ?>
                                    <option value="<?php echo $datacusol['kd_cusol'] ?>"<?php echo ($dataedit['kd_cusol']==$datacusol['kd_cusol']) ? "selected":"" ?>><?php echo $datacusol['nm_cusol'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <script type="text/javascript">
                            var sel<?php echo $dataedit['kd_sol'] ?> = document.getElementById("respedit<?php echo $dataedit['kd_sol'] ?>"), text<?php echo $dataedit['kd_sol'] ?> = document.getElementById("reason_soldesedit<?php echo $dataedit['kd_sol'] ?>");

                            sel<?php echo $dataedit['kd_sol'] ?>.onchange = function(e) {
                                text<?php echo $dataedit['kd_sol'] ?>.disabled = (sel<?php echo $dataedit['kd_sol'] ?>.value != "Yes");
                            // sel<?php echo $dataedit['kd_sol'] ?>.onchange = function(e) {
                            // text<?php echo $dataedit['kd_sol'] ?>.enabled = (sel<?php echo $dataedit['kd_sol'] ?>.value != "Yes");
                            };
                        </script>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Expected Finish Date</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" class="form-control" name="finish_date" value="<?php echo $dataedit['finish_date'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Notes</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <textarea name="notes" id="textarea-input" rows="7" placeholder="Masukkan Notes" class="form-control" required><?php echo $dataedit['notes'] ?></textarea>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="edit" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <!-- MODAL HAPUS -->
    <?php
    $hapus = mysqli_query($db,"SELECT kd_sol, nm_cust, progres_sol, nm_expec, tindak_lanjut, special_sol, nm_cusol, finish_date, notes FROM solution_design, product_pres, canvasing, potpen, cust, expec, cusol WHERE solution_design.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND solution_design.kd_expec=expec.kd_expec AND solution_design.kd_cusol=cusol.kd_cusol")or die(mysqli_error($db));
    while ($datahapus = mysqli_fetch_array($hapus)){ 

    ?>
<div class="modal fade" id="hapus<?php echo $datahapus['kd_sol'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Hapus Data Solution Design</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="solution_design.php" method="POST">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Yakin Ingin Menghapus Data Product Presentasi <b><?php echo $datahapus['nm_cust'] ?>?</b></label>
                            <input type="hidden" name="kd_sol" value="<?php echo $datahapus['kd_sol'] ?>">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
    <?php } ?>

    <!-- QUERY PROSES -->
    <?php 
    if ($_POST['simpan']) {
        $kd_product_pres = $_POST['kd_product_pres'];
        $progres_sol = $_POST['progres_sol'];
        $kd_expec = $_POST['kd_expec'];
        $tindak_lanjut = $_POST['tindak_lanjut'];
        $special_sol = $_POST['special_sol'];
        $kd_cusol = $_POST['kd_cusol'];
        $finish_date = $_POST['finish_date'];
        $notes = $_POST['notes'];
        
        $cek = mysqli_query($db,"SELECT kd_product_pres FROM solution_design");
        $cekdata = mysqli_fetch_assoc($cek);
        if ($kd_product_pres==$cekdata['kd_product_pres']) {
            echo "<script>alert('Data Customer Sudah Ada, Tambahkan Data Customer Lain')</script>";
            echo '<script type="text/javascript">window.location="solution_design.php?halaman=solution_design"</script>';
        }else{

            $tambah = mysqli_query($db,"INSERT INTO solution_design VALUES('','$kd_product_pres','$progres_sol','$kd_expec','$tindak_lanjut','$special_sol','$kd_cusol','$finish_date','$notes')")or die(mysqli_error($db));
            if ($tambah) {
                echo "<script>alert('Berhasil Tambah Data Solution Design')</script>";
                echo '<script type="text/javascript">window.location="solution_design.php?halaman=solution_design"</script>';
            }else{
                echo "<script>alert('Gagal Tambah Data Solution Design')</script>";
                echo '<script type="text/javascript">window.location="solution_design.php?halaman=solution_design"</script>';
            }
        }
    }elseif ($_POST['edit']) {
        $kd_sol = $_POST['kd_sol'];
        $kd_product_pres = $_POST['kd_product_pres'];
        $progres_sol = $_POST['progres_sol'];
        $kd_expec = $_POST['kd_expec'];
        $tindak_lanjut = $_POST['tindak_lanjut'];
        $special_sol = $_POST['special_sol'];
        $finish_date = $_POST['finish_date'];
        $now = date('Y-m-d');
        $notes = $_POST['notes'];
        if($finish_date>=$now){
            if (isset($_POST['kd_cusol'])) {
                // mengedit jika ada solution design
                $kd_cusol = $_POST['kd_cusol'];
                $edit = mysqli_query($db,"UPDATE solution_design SET kd_product_pres='$kd_product_pres', progres_sol='$progres_sol', kd_expec='$kd_expec', tindak_lanjut='$tindak_lanjut', special_sol='$special_sol', kd_cusol='$kd_cusol', finish_date='$finish_date', notes='$notes' WHERE kd_sol='$kd_sol'")or die(mysqli_error($db));
            }else{
                // mengedit jika tidak ada solution design, auto 1
                $edit = mysqli_query($db,"UPDATE solution_design SET kd_product_pres='$kd_product_pres', progres_sol='$progres_sol', kd_expec='$kd_expec', tindak_lanjut='$tindak_lanjut', special_sol='$special_sol', kd_cusol='1', finish_date='$finish_date', notes='$notes' WHERE kd_sol='$kd_sol'")or die(mysqli_error($db));
            }
            if ($edit) {
                echo "<script>alert('Berhasil Edit Data Solution Design')</script>";
                echo '<script type="text/javascript">window.location="solution_design.php?halaman=solution_design"</script>';
            }else{
                echo "<script>alert('Gagal Edit Data Solution Design')</script>";
                echo '<script type="text/javascript">window.location="solution_design.php?halaman=solution_design"</script>';
            }
        }else{
            echo "<script>alert('Gagal! Tanggal finish tidak boleh sebelum dari hari ini')</script>";
            echo '<script type="text/javascript">window.location="solution_design.php?halaman=solution_design"</script>';
        }    
    }elseif ($_POST['hapus']) {
        $kd_sol = $_POST['kd_sol'];

        $hapus = mysqli_query($db,"DELETE FROM solution_design WHERE kd_sol='$kd_sol'")or die(mysqli_error($db));

        if ($hapus) {
        echo "<script>alert('Berhasil Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="solution_design.php?halaman=solution_design"</script>';
        }else{
            echo "<script>alert('Gagal Menghapus Data')</script>";
            echo '<script type="text/javascript">window.location="solution_design.php?halaman=solution_design"</script>';
        }
    }

     ?>
     <script type="text/javascript">
         function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->