-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26 Mar 2020 pada 11.56
-- Versi Server: 10.1.37-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketingdansales2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `canvasing`
--

CREATE TABLE `canvasing` (
  `kd_canv` int(11) NOT NULL,
  `kd_potpen` int(11) NOT NULL,
  `cen` varchar(100) NOT NULL,
  `prog_canv` varchar(50) NOT NULL,
  `tgl_canv` date NOT NULL,
  `response_canv` varchar(20) NOT NULL,
  `reason_neg_canv` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `canvasing`
--

INSERT INTO `canvasing` (`kd_canv`, `kd_potpen`, `cen`, `prog_canv`, `tgl_canv`, `response_canv`, `reason_neg_canv`) VALUES
(1, 5, '20200104ADM001', 'Initial Meeting', '2020-01-03', 'Positif', ''),
(2, 6, '20200104ADM002', 'Initial Meeting', '2020-01-02', 'Positif', ''),
(3, 7, '20200104ADM003', 'Initial Meeting', '2020-01-02', 'Negatif', 'Administratif Requirement'),
(4, 12, '20200105MUL004', 'Initial Meeting', '2020-01-05', 'Positif', ''),
(5, 8, '20200105MUL005', 'Initial Meeting', '2020-01-02', 'Positif', ''),
(6, 9, '20200105MUL006', 'Initial Meeting', '2020-01-01', 'Positif', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `create_quatation`
--

CREATE TABLE `create_quatation` (
  `kd_quad` int(11) NOT NULL,
  `kd_product_pres` int(11) NOT NULL,
  `no_quad` varchar(50) NOT NULL,
  `kd_jns_layanan` int(11) NOT NULL,
  `bw_quad` varchar(10) NOT NULL,
  `qty` varchar(20) NOT NULL,
  `price` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `top` varchar(50) NOT NULL,
  `note` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `create_quatation`
--

INSERT INTO `create_quatation` (`kd_quad`, `kd_product_pres`, `no_quad`, `kd_jns_layanan`, `bw_quad`, `qty`, `price`, `subtotal`, `top`, `note`) VALUES
(1, 1, '', 1, '5000', '', 1000000, 1000000, '', ''),
(2, 1, '', 2, '5', '', 10000000, 10000000, '', ''),
(8, 3, '', 1, '5', '', 10000000, 10000000, '', ''),
(9, 3, '', 5, '5000', '', 1000000, 1000000, '', ''),
(10, 3, '', 4, '5000', '', 1000000, 1000000, '', ''),
(16, 5, 'we12', 1, '11', '3', 5000000, 15000000, 'OTC', 'hm'),
(18, 5, 'wiiiii', 2, '11', '1', 1000000, 1000000, 'OTC', 'hm'),
(19, 1, '12', 2, '11', '3', 5000000, 15000000, 'OTC', 'sip');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cusol`
--

CREATE TABLE `cusol` (
  `kd_cusol` int(11) NOT NULL,
  `nm_cusol` varchar(100) NOT NULL,
  `email_cusol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cusol`
--

INSERT INTO `cusol` (`kd_cusol`, `nm_cusol`, `email_cusol`) VALUES
(1, 'Tidak Ada', 'Tidak Ada'),
(2, 'Muhammad Thayib Ramadhan', 'muhammad.thayib@gmail.com'),
(3, 'Andri', 'andri@gmail.com'),
(4, 'Rendy', 'rendy@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cust`
--

CREATE TABLE `cust` (
  `kd_cust` int(11) NOT NULL,
  `kd_seg` int(11) NOT NULL,
  `kd_pengguna` int(11) NOT NULL,
  `nm_cust` varchar(255) NOT NULL,
  `alm_cust` varchar(255) NOT NULL,
  `latitude` varchar(15) NOT NULL,
  `longitude` varchar(15) NOT NULL,
  `pic_cust` varchar(50) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `jabatan_pic` varchar(50) NOT NULL,
  `tgl_pelaksanaan_prospek` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cust`
--

INSERT INTO `cust` (`kd_cust`, `kd_seg`, `kd_pengguna`, `nm_cust`, `alm_cust`, `latitude`, `longitude`, `pic_cust`, `no_telp`, `jabatan_pic`, `tgl_pelaksanaan_prospek`) VALUES
(15, 8, 9, 'CUST_2BJB039_ Baristand Industri Banjarbaru', ' Jl. Panglima Batur Barat No. 2 Banjarbaru - Kalsel (70711)', '-3.438746', '114.829584', 'Aslan', '081251305111', 'Staf IT', '2017-12-13'),
(16, 5, 9, 'CUST_2BJB073_DISKOMINFO KALSEL', 'JL. DHARMA PRAJA II KAWASAN PERKANTORAN PEMERINTAH PROVINSI KALIMANTAN SELATANKAWASAN PERKANTORAN PEMERINTAH Kota Banjarbaru Kalimantan Selatan Indone', '-3.438746', '114.829584', 'H. Bahrum', '081348276662', 'Kasie E-Government', '2018-08-03'),
(17, 5, 9, 'CUST_2BJB070 BADAN KEPEGAWAIAN DAERAH PROVINSI KAL', 'Jl. Pangeran Antasari No.5\r\nKota Banjarbaru\r\nKalimantan Selatan\r\n70711 ', '-3.438746', '114.829584', 'Yoma', '082220007779', 'Staf Infrastruktur', '2018-07-30'),
(19, 5, 9, 'CUST_2BJB054_LAPAS_III_BANJARBARU', 'Bangkal Cempaka Kota Banjar Baru Kalimantan Selatan 70732Banjarbaru Kota Banjarbaru Kalimantan Selatan Indonesia', '-3.438746', '114.829584', 'Hutan', '081345032227', 'Kepala Administrasi', '2019-12-17'),
(20, 5, 9, 'CUST_2BJB093 BALAI PELATIHAN KESEHATAN PROVINSI KA', 'Jl. Mistar tjokrokusumo no 5a banjarbaru kotabanjarbaru kalimantan selatan 70714', '-3.438746', '114.829584', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-01-22'),
(21, 5, 9, 'CUST_2BJB068_PT. ANGKASA PURA LOGISTIK', 'Kantor Cabang Banjarmasin - Terminal Kargo Bandara Syamsudin Noor Landasan Ulin Banjarbaru Banjarmasin 70724A', '-3.438746', '114.829584', 'Syaiful', '082247657017', 'Spv Parkir IT', '2018-08-06'),
(22, 5, 9, 'BAPPEDA PROVINSI KALIMANTAN SELATAN', 'Tidak ada', '-3.438746', '114.829584', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-12-17'),
(23, 1, 9, 'Badan Keuangan Daerah Provinsi Kalimantan Selatan', 'Tidak ada', '-3.438746', '114.829584', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-12-17'),
(24, 3, 9, 'Akademi Teknik Pertambangan Banjarbaru / Mining En', 'Tidak ada', '-3.438746', '114.829584', 'Rafii', '081351122326', 'Staf IT', '2019-12-19'),
(25, 3, 9, ' Universitas Achmad Yani Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-12-18'),
(26, 5, 9, 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu P', 'tidak ada', '-3.438746', '114.829584', 'Nasir', '05116749344', 'Staf IT', '2019-12-12'),
(27, 3, 9, 'Politeknik Kesehatan Banjarmasin', 'Tidak ada', '-3.438746', '114.829584', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-12-19'),
(28, 5, 9, 'BBTKLPP Banjarbaru', 'tidak ada', '-3.438746', '114.829584', 'Bachtiar', 'Tidak ada', 'Kepala TU/Perencanaan', '2019-12-10'),
(29, 5, 9, 'Dinas Pendidikan Dan Kebudayaan Provinsi Kalimanta', 'Tidak ada', '-3.438746', '114.829584', 'Rani', 'Tidak ada', 'Tidak ada', '2019-12-24'),
(30, 1, 9, 'PDAM Intan Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Fahri', '081348217013', 'Kasubag IT', '2019-12-19'),
(31, 3, 9, 'Balai Pelatihan Kesehatan(BAPELKES) Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-12-24'),
(32, 5, 9, 'AirNav Banjarbaru', 'Tidak ada', '-3.438746', '114.829584a', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-12-18'),
(33, 7, 9, 'RS Ciputra Mitra Banjarmasin', 'tidak ada', '-3.438746', '114.829584', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-12-25'),
(34, 7, 9, 'RS Sari Mulia Banjarmasin', 'tidak ada', '-3.438746', '114.829584', 'Pak Lek', '082151854545', 'Staf IT', '2019-12-11'),
(36, 3, 9, 'Balai Bahasa Kalimantan Selatan', 'Tidak ada', '-3.438746', '114.829584', 'Edwin', '085346409991', 'Staf IT', '2019-12-12'),
(37, 7, 9, 'RS Syifa Medika Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Fajrin', '085753156162', 'Staf IT', '2019-12-11'),
(38, 7, 10, 'RS Aveciena Medika Martapura', 'Tidak ada', '-3.438746', '114.829584', 'Rahman', '081253970599', 'Staf IT', '2019-12-11'),
(39, 7, 10, 'RS Pelita Insani Martapura', 'Tidak ada', '-3.438746', '114.829584', 'Gusti', '081250141117', 'Staf IT', '2019-12-10'),
(40, 9, 10, 'Q MALL Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Rusdi', 'Tidak ada', 'Staf IT', '2019-12-02'),
(41, 5, 9, 'Balai Pengkajian Teknologi Pangan(BPTP) KALSEL', 'Tidak ada', '-3.438746', '114.829584', 'Sumarmi', 'Tidak ada', 'Staf IT', '2019-12-12'),
(42, 5, 9, 'Balai Riset dan Standarisasi Mutu Barang/Industri ', 'Tidak ada', '-3.438746', '114.829584', 'Aslan', '081251305111', 'Staf IT', '2019-12-18'),
(43, 3, 9, 'Balai Besar Teknik Lingkungan dan Pengujian Penyak', 'Tidak ada', '-3.438746', '114.829584', 'Ahmad Ridha', '08125123772', 'Staf IT', '2019-12-25'),
(44, 5, 9, 'Balai Pengujian dan Sertifikasi Mutu Barang', 'Tidak ada', '-3.438746', '114.829584', 'Tidak Ada', 'Tidak ada', 'Tidak ada', '2019-12-17'),
(45, 5, 9, 'Biro Umum Pemerintah Kota(PEMKO) Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Fahmi', 'Tidak ada', 'Staf Biro Umum', '2019-12-17'),
(46, 3, 9, 'Akademi Analis Kesehatan Borneo Lestari', 'Tidak ada', '-3.438746', '114.829584', 'Arsyad', '082252545385', 'Staf IT', '2019-12-10'),
(47, 3, 9, 'STIKES Borneo Lestari', 'Tidak ada', '-3.438746', '114.829584', 'Rinto', '082153065045', 'Staf yayasan borneo lestari', '2019-12-25'),
(48, 3, 9, 'SMK Kesehatan Borneo Lestari', 'Tidak ada', '-3.438746', '114.829584', 'Ruli', '082158376457', 'Staf IT/TU', '2019-12-13'),
(49, 5, 9, 'Pengadilan Negeri Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Aska', '085780892150', 'Staf Umum/Keuangan', '2019-12-18'),
(50, 5, 9, 'Kejaksaan Negeri Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Agus', '085109073539', 'Kepala IT', '2019-06-18'),
(51, 5, 9, 'Pengadilan Militer Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Agus', '081344322211', 'Sekretaris Pengadilan', '2019-12-17'),
(52, 5, 9, 'Stasiun Klimatologi Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Agus', '081348435212', 'Staf IT', '2019-12-17'),
(53, 5, 9, 'Biro Pusat Statistik Provinsi KALSEL', 'Tidak ada', '-3.438746', '114.829584', 'Tito Raditya A.W', '08121589824', 'Staf IT', '2019-12-20'),
(54, 7, 9, 'RSUD Idaman Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Sopian', '089501646227', 'Staf IT', '2019-12-19'),
(55, 5, 9, 'Badan Narkotika Nasional (BNN) Banjarbaru', 'Tidak ada', '-3.438746', '114.829584', 'Fahri', '085393084440', 'Staf IT', '2019-12-11'),
(56, 5, 9, 'Balai Konservasi Sumber Daya Alam', 'Tidak ada', '-3.438746', '114.829584', 'Usman', '081349708422', 'Perencanaan', '2019-12-18'),
(57, 5, 9, 'Balai Pengelolaan DAS Barito', 'Tidak ada', '-3.438746', '114.829584', 'Joko', '08125081010', 'Perencanaan', '2019-12-23'),
(58, 5, 9, 'Badan Kepegawaian Negara Regional VIII', 'Tidak ada', '-3.438746', '114.829584', 'Surya AS', '081348616365', 'Staf Umum', '2019-12-24'),
(59, 5, 9, 'Balai Pemantapan Hutan Produksi Wilayah V', 'Tidak ada', '-3.438746', '114.829584', 'Andi', '0811505729', 'Staf Perencanaan', '2019-12-11'),
(60, 5, 9, 'Pengadilan Tinggi Banjarmasin', 'Tidak ada', '-3.438746', '114.829584', 'Andre', '081314099990', 'Staf IT', '2019-12-17'),
(61, 5, 9, 'Kementrian Pekerjaan Umum Balai Rawa', 'Tidak ada', '-3.438746', '114.829584', 'Ari Widya', '081251969990', 'Operasional Kantor', '2019-12-12'),
(62, 3, 9, 'UIN Antasari Banjarmasin', 'Tidak ada', '-3.438746', '114.829584', 'Andi', '081348555696', 'Staf IT', '2019-12-25'),
(63, 3, 9, 'Pengadilan Tinggi Agama Banjarmasin', 'Tidak ada', '-3.438746', '114.829584', 'Aan', '081349615065', 'Staf Perencanaan', '2019-12-17'),
(64, 5, 9, 'BPJS Ketenagakerjaan Banjarmasin', 'Tidak ada', '-3.438746', '114.829584', 'Dwi', '087880163869', 'Staf IT', '2019-12-17'),
(65, 5, 9, 'Loka Monitor Spektrum Frekuensi Radio', 'Jl. Pramuka No.51 Pengambangan Kec. Banjarmasin Tim. Kota Banjarmasin Kalimantan Selatan 70654', '-3.438746', '114.829584', 'Sunardi', '08115014165', 'Kasubag TU', '2019-12-12'),
(66, 5, 9, 'PDAM Bandarmasih', 'Tidak ada', '-3.438746', '114.829584', 'Zubaldi', 'Tidak ada', 'Kasubag IT', '2019-12-12'),
(67, 3, 9, 'Universitas Muhammadiyah Banjarmasin', 'Tidak ada', '-3.438746', '114.829584', 'Nanda', '085392063524', 'Kasubag Puskom', '2019-12-18'),
(68, 5, 9, 'Balai Arkeologi Banjarmasin', 'Tidak ada', '-3.438746', 'd114.829584', 'Restu', '081802649432', 'Staf IT', '2019-12-18'),
(69, 5, 9, 'CUST_2BJB074_LEMBAGA PENJAMIN MUTU PENDIDIKAN PROV', 'JL GOTONG ROYONG NO 85 MENTAOS  Banjarbaru utara  Kota Banjarbaru  Kalimantan Selatan  Indonesia  70711', '-3.438746', '114.829584', 'Fadlan', '082350369443', 'Kasubag Pelaksana', '2019-12-03'),
(70, 5, 9, 'Balai Besar Pendidikan dan Pelatihan Kesejahteraan', 'Tidak ada', '-3.438746', '114.829584', 'Syabdar', '085397999203', 'Kasubag IT', '2019-12-11'),
(71, 5, 9, 'Balai Pendapatan dan Retribusi Daerah Kota Banjarb', 'Tidak ada', '-3.438746', '114.829584', 'Fauzan', '082156206666', 'Staf IT', '2019-12-18'),
(72, 4, 9, 'PT. PAMA Persada Nusandara', 'Tidak ada', '-3.438746', '114.829584', 'Budianto', '081254020057', 'Staf IT', '2019-12-18'),
(73, 5, 9, 'Balai Diklat Kementrian Lingkungan Hidup Banjarmas', 'Tidak ada', '-3.438746', '114.829584', 'Wini', '082158921722', 'Staf Perencanaan', '2019-12-10'),
(74, 5, 9, 'Dinas Kominfo Kabupaten Banjar', 'Tidak ada', '-3.438746', '114.829584', 'Kris', '085228001697', 'Kepala TU', '2019-12-11'),
(75, 1, 9, 'TVRI Kalimantan Selatan', 'Tidak ada', '-3.3468963', '114.6249184', 'Rijal', 'Tidak ada', 'LPU', '2019-12-17'),
(78, 5, 9, 'AGU_17_W1_CUST_2BJB073_DISKOMINFO KALSEL', 'Jl. A. Yani No 3\r\n', '-', '-', 'Kris', '085228001697', 'Kepala TU', '2017-08-01'),
(79, 5, 9, 'AGU_17_W2_CUST_2BJB073_DISKOMINFO KALSEL', 'Kompleks Gubernur\r\n', '-', '-', 'Rian', '089666213213', 'Staf IT', '2017-08-07'),
(82, 5, 9, 'AGU_17_W3_CUST_2BJB074_LEMBAGA PENJAMIN MUTU PENDI', '-', '-', '-', 'Gapuri', '085249345399', 'Staf Perencaanaan', '2017-08-14'),
(83, 5, 9, 'AGU_17_W3_CUST_2BJB048_ BADAN PENGEMBANGAN SUMBER ', '-', '-', '-', 'Fahriad', '08125023825', 'Staf IT', '2017-08-14'),
(84, 5, 9, 'AGU_17_W3_CUST_2RTA025 PT. ANTANG GUNUNG MERATUS (', '-', '-', '-', 'Hary', '082350512808', 'Staf IT', '2017-08-14'),
(85, 5, 9, 'AGU_17_W3_CUST_2BJB012_BALAI PERHUTANAN SOSIAL DAN', '-', '-', '-', 'Santo', '085345924199', 'Staf IT', '2017-08-14'),
(87, 5, 9, 'AGU_17_W5_CUST_2BJB048_ BADAN PENGEMBANGAN SUMBER ', 'JL. Panglima Batur Timur, Banjarbaru, South Kalimantan, 70711', '-', '-', 'Fahriad', '08125023825', 'Staf IT', '2017-08-28'),
(88, 5, 9, 'AGU_17_W5_CUST_2BJB074_LEMBAGA PENJAMIN MUTU PENDI', 'Jl. Gotong Royong', '-', '-', 'Gafuri', '085249345399', 'Staf Perencanaan', '2017-08-28'),
(89, 5, 9, 'AGU_17_W5_CUST_2BJB070 BADAN KEPEGAWAIAN DAERAH PR', 'Lapangan Murjani', '-', '-', 'Rifki', '08115031909', 'Staf IT', '2017-08-28'),
(90, 5, 9, 'AGU_17_W5_CUST_2BJM169 PT PLN Area Banjarmasin', '-', '-', '-', 'Sigit', '-', '-', '2017-08-28'),
(91, 5, 9, 'AGU_17_W5_CUST_2BJB012_BALAI PERHUTANAN SOSIAL DAN', 'Jl. Sei Salak', '-', '-', 'Santo', '085345924199', 'Staf IT', '2017-08-28'),
(92, 1, 6, 'tes', 'tes', '234', '325', 'tes', '34', 'tes', '2020-01-04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer_response`
--

CREATE TABLE `customer_response` (
  `kd_response` int(11) NOT NULL,
  `kd_subm_quat` int(11) NOT NULL,
  `cust_resp` varchar(20) NOT NULL,
  `negatif` varchar(50) NOT NULL,
  `procurement_method` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer_response`
--

INSERT INTO `customer_response` (`kd_response`, `kd_subm_quat`, `cust_resp`, `negatif`, `procurement_method`) VALUES
(8, 1, 'Positif', '', 'Pembelian Langsung'),
(9, 3, 'Positif', '', 'Pembelian Langsung'),
(10, 4, 'Negatif', 'Administratif Requirement', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_potpen`
--

CREATE TABLE `detail_potpen` (
  `kd_det_pot` int(11) NOT NULL,
  `kd_potpen` int(11) NOT NULL,
  `kd_jns_layanan` int(11) NOT NULL,
  `bw` varchar(10) NOT NULL,
  `provider` varchar(50) NOT NULL,
  `biaya_sewa` int(11) NOT NULL,
  `est_kont_berakhir` date NOT NULL,
  `est_mulai_pengadaan` date NOT NULL,
  `metode_pengadaan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_potpen`
--

INSERT INTO `detail_potpen` (`kd_det_pot`, `kd_potpen`, `kd_jns_layanan`, `bw`, `provider`, `biaya_sewa`, `est_kont_berakhir`, `est_mulai_pengadaan`, `metode_pengadaan`) VALUES
(21, 5, 1, '5000', 'PT. Telkom', 1000000, '2020-04-08', '2020-01-01', 'Tender'),
(22, 5, 2, '5', 'PT. ICON+', 10000000, '2021-01-14', '2020-01-01', 'Tender'),
(23, 5, 3, '4', 'PT. Telkom', 5000000, '2020-01-05', '2020-01-02', 'Tender'),
(24, 6, 2, '2', 'PT. ICON+', 4000000, '2020-01-05', '2020-01-02', 'Pengadaan Langsung'),
(25, 7, 1, '10', 'PT. ICON+', 15000000, '2020-01-05', '2020-01-03', 'Pengadaan Langsung'),
(26, 7, 5, '2', 'PT. ICON+', 12000000, '2020-01-23', '2020-01-01', 'Pengadaan Langsung'),
(27, 8, 1, '5', 'ICON+', 20000000, '2020-03-12', '2020-01-02', 'Pengadaan Langsung'),
(28, 8, 2, '2', 'ICON+', 5000000, '2020-01-15', '2020-01-04', 'Pengadaan Langsung'),
(29, 8, 3, '3', 'PT. Telkom', 1000000, '2020-01-08', '2019-12-31', 'Pengadaan Langsung'),
(30, 9, 1, '5000', 'PT. Telkom', 1000000, '2020-01-16', '2020-01-02', 'Pengadaan Langsung'),
(31, 9, 2, '5000', 'PT. Telkom', 1000000, '2020-02-05', '2020-01-02', 'Pengadaan Langsung'),
(32, 12, 1, '5', 'ICON+', 10000000, '2020-01-30', '2020-01-02', 'Pengadaan Langsung'),
(33, 12, 5, '5000', 'PT. Telkom', 1000000, '2020-01-08', '2020-01-02', 'Pengadaan Langsung'),
(34, 12, 4, '5000', 'PT. Telkom', 1000000, '2020-01-30', '2020-01-01', 'Tender');

-- --------------------------------------------------------

--
-- Struktur dari tabel `existing`
--

CREATE TABLE `existing` (
  `kd_ex` int(11) NOT NULL,
  `kd_cust` int(11) NOT NULL,
  `kd_jns_layanan` int(11) NOT NULL,
  `sid` varchar(50) NOT NULL,
  `bw` varchar(100) NOT NULL,
  `pa_number` varchar(100) NOT NULL,
  `harga_sewa` int(11) NOT NULL,
  `service_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `existing`
--

INSERT INTO `existing` (`kd_ex`, `kd_cust`, `kd_jns_layanan`, `sid`, `bw`, `pa_number`, `harga_sewa`, `service_status`) VALUES
(5, 15, 1, '0935859', '12', '121212', 5000000, 'Active'),
(6, 16, 2, '284721', '3', '4287523', 10000000, 'Active'),
(7, 17, 4, '83569833', '1', '845789322', 1000000, 'Active'),
(8, 39, 1, '83569833', '5000', '121212', 10000, 'Active'),
(13, 40, 1, '00000', '5000', '121212', 10000000, 'Active'),
(15, 38, 1, '34523', '5000', '121212', 1000000, 'Active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `expec`
--

CREATE TABLE `expec` (
  `kd_expec` int(11) NOT NULL,
  `nm_expec` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expec`
--

INSERT INTO `expec` (`kd_expec`, `nm_expec`) VALUES
(5, 'Higher Specification'),
(2, 'Lower Price'),
(1, 'Tidak Ada'),
(4, 'Training'),
(3, 'Value Added Services');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jns_layanan`
--

CREATE TABLE `jns_layanan` (
  `kd_jns_layanan` int(11) NOT NULL,
  `nm_jns_layanan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jns_layanan`
--

INSERT INTO `jns_layanan` (`kd_jns_layanan`, `nm_jns_layanan`) VALUES
(2, 'Clear Channel'),
(5, 'Data Center & Cloud Service'),
(14, 'ICONApps'),
(10, 'IIX'),
(7, 'Infrastruktur Telekomunikasi'),
(8, 'Internet Coorporate'),
(11, 'IP Transit'),
(4, 'IP VSAT'),
(1, 'IPVPN'),
(12, 'ISEE'),
(13, 'IWON'),
(9, 'IX'),
(6, 'Managed Service'),
(3, 'Metro Ethernet'),
(18, 'tes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `kd_pengguna` int(11) NOT NULL,
  `kd_posisi` int(11) NOT NULL,
  `nm_pengguna` varchar(50) NOT NULL,
  `inisial` varchar(10) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`kd_pengguna`, `kd_posisi`, `nm_pengguna`, `inisial`, `nip`, `username`, `pwd`) VALUES
(1, 1, 'koko', 'KH', '128485', 'koko', 'dcd7c6ef54d01e3e3a4cc96508ff0bca57a3b771'),
(5, 1, 'Aan', 'AAN', '93746523', 'aan', '64362232b560f1d6775fb4e9a0548968f739fc6e'),
(6, 1, 'admin2', 'ADM', '4444', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
(8, 4, 'Erwin', 'EPP', 'A1317052', 'erwin', '6ab6f808523817d722936ddd29076d9d5134de1d'),
(9, 3, 'Maria Lourdes Wiranti', 'MLW', 'belum ada', 'lilo', '4dcfbe281eb9537d391f36edc1893b8928a4102b'),
(10, 3, 'Maria Ursula Mai Cruz', 'MUL', 'belum ada', 'ursula', 'ff416555c9c4e1cebf9939af6e182dcce1d08670'),
(11, 3, 'Muhammad Rozi Septian', 'MRS', 'belum ada', 'rozi', '7cb212ef3dc6387389ad7b5d0373c5d8cb8b484f');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posisi`
--

CREATE TABLE `posisi` (
  `kd_posisi` int(11) NOT NULL,
  `nm_posisi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posisi`
--

INSERT INTO `posisi` (`kd_posisi`, `nm_posisi`) VALUES
(1, 'Administrator'),
(3, 'Sales'),
(4, 'Manager');

-- --------------------------------------------------------

--
-- Struktur dari tabel `potpen`
--

CREATE TABLE `potpen` (
  `kd_potpen` int(11) NOT NULL,
  `kd_cust` int(11) NOT NULL,
  `status_potensi` varchar(30) NOT NULL,
  `nama_ppk` varchar(100) NOT NULL,
  `telp_ppk` varchar(20) NOT NULL,
  `tgl_potpen` date NOT NULL,
  `rencana` varchar(40) NOT NULL,
  `kendala` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `potpen`
--

INSERT INTO `potpen` (`kd_potpen`, `kd_cust`, `status_potensi`, `nama_ppk`, `telp_ppk`, `tgl_potpen`, `rencana`, `kendala`) VALUES
(5, 15, 'Merah', 'Ujang', '082341', '2020-01-02', 'Flag Need Follow Up', 'Tidak Ada Kendala'),
(6, 16, 'Kuning', 'Ari', '082287553422', '2020-01-03', 'Ajak Studi Banding', 'Tidak Ada Kendala'),
(7, 17, 'Hijau', 'Bowo', '085322678895', '2020-01-02', 'Flag Need Follow Up', 'Tidak Ada Kendala'),
(8, 39, 'Hijau', 'Ujang', '082234445666', '2020-01-04', 'Kunjungan Kembali', 'Tidak Ada Kendala'),
(9, 40, 'Hijau', 'Roji', '082341', '2020-01-04', 'Kunjungan Kembali', 'Tidak Ada Kendala'),
(12, 38, 'Hijau', 'Nata', '0836422222', '2020-01-04', 'Kunjungan Kembali', 'Tidak Ada Kendala');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_pres`
--

CREATE TABLE `product_pres` (
  `kd_product_pres` int(11) NOT NULL,
  `kd_canv` int(11) NOT NULL,
  `progres_product_pres` varchar(20) NOT NULL,
  `meeting_plan_date` date NOT NULL,
  `actual_meeting_date` date NOT NULL,
  `response_product_pres` varchar(10) NOT NULL,
  `reason_neg_propres` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_pres`
--

INSERT INTO `product_pres` (`kd_product_pres`, `kd_canv`, `progres_product_pres`, `meeting_plan_date`, `actual_meeting_date`, `response_product_pres`, `reason_neg_propres`) VALUES
(1, 1, 'Presentasi Produk', '2020-01-07', '2020-01-07', 'Positif', ''),
(3, 4, 'Presentasi Produk', '2020-01-09', '2020-01-09', 'Positif', ''),
(4, 5, 'Presentasi Produk', '2020-01-15', '2020-01-15', 'Positif', ''),
(5, 6, 'Presentasi Produk', '2020-01-16', '2020-01-16', 'Negatif', 'Price'),
(13, 2, 'Presentasi Produk', '2020-01-28', '2020-01-30', 'Positif', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `segment_cust`
--

CREATE TABLE `segment_cust` (
  `kd_seg` int(11) NOT NULL,
  `nm_seg` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `segment_cust`
--

INSERT INTO `segment_cust` (`kd_seg`, `nm_seg`) VALUES
(1, 'Banking & Financial'),
(2, 'Consultant, Contract'),
(3, 'Education'),
(4, 'Energy Utility Mining'),
(5, 'Government'),
(6, 'Healthcare'),
(7, 'Hospitality'),
(8, 'Manufacture'),
(9, 'Media & Entertain'),
(10, 'Natural Resources'),
(11, 'PLN Group'),
(12, 'Professional Association'),
(13, 'Property'),
(14, 'Retail Distribution'),
(19, 'Ritel'),
(15, 'Telecommunication'),
(16, 'Transportation');

-- --------------------------------------------------------

--
-- Struktur dari tabel `solution_design`
--

CREATE TABLE `solution_design` (
  `kd_sol` int(11) NOT NULL,
  `kd_product_pres` int(11) NOT NULL,
  `progres_sol` varchar(20) NOT NULL,
  `kd_expec` int(11) NOT NULL,
  `tindak_lanjut` varchar(50) NOT NULL,
  `special_sol` varchar(10) NOT NULL,
  `kd_cusol` int(11) NOT NULL,
  `finish_date` date NOT NULL,
  `notes` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `solution_design`
--

INSERT INTO `solution_design` (`kd_sol`, `kd_product_pres`, `progres_sol`, `kd_expec`, `tindak_lanjut`, `special_sol`, `kd_cusol`, `finish_date`, `notes`) VALUES
(1, 1, 'Solution Design', 1, 'Submit Proposal Teknis dan SHP', 'No', 1, '2020-01-29', '-'),
(4, 3, 'Solution Design', 1, 'Flag Need Follow Up', 'No', 1, '2020-01-05', '-'),
(5, 4, 'Solution Design', 2, 'Kunjungan Kembali', 'Yes', 2, '2020-01-29', '-'),
(14, 13, 'Solution Design', 1, 'Flag Need Follow Up', 'Yes', 1, '2020-01-30', 'okeh');

-- --------------------------------------------------------

--
-- Struktur dari tabel `submit_quot`
--

CREATE TABLE `submit_quot` (
  `kd_subm_quat` int(11) NOT NULL,
  `kd_product_pres` int(11) NOT NULL,
  `date_subm` date NOT NULL,
  `nm_subm` varchar(100) NOT NULL,
  `position_subm` varchar(50) NOT NULL,
  `phone_subm` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `submit_quot`
--

INSERT INTO `submit_quot` (`kd_subm_quat`, `kd_product_pres`, `date_subm`, `nm_subm`, `position_subm`, `phone_subm`) VALUES
(1, 1, '2020-01-24', 'Dodo', 'IT Support', '082233443211'),
(3, 3, '2020-04-01', 'Didi', 'IT Support', '082356447822'),
(4, 4, '2020-12-07', 'Angga', 'IT Support', '082356447822');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `canvasing`
--
ALTER TABLE `canvasing`
  ADD PRIMARY KEY (`kd_canv`),
  ADD KEY `kd_potpen` (`kd_potpen`);

--
-- Indexes for table `create_quatation`
--
ALTER TABLE `create_quatation`
  ADD PRIMARY KEY (`kd_quad`),
  ADD KEY `kd_jns_layanan` (`kd_jns_layanan`),
  ADD KEY `kd_product_pres` (`kd_product_pres`);

--
-- Indexes for table `cusol`
--
ALTER TABLE `cusol`
  ADD PRIMARY KEY (`kd_cusol`),
  ADD UNIQUE KEY `nm_cusol` (`nm_cusol`);

--
-- Indexes for table `cust`
--
ALTER TABLE `cust`
  ADD PRIMARY KEY (`kd_cust`),
  ADD KEY `kd_seg` (`kd_seg`),
  ADD KEY `kd_pengguna` (`kd_pengguna`);

--
-- Indexes for table `customer_response`
--
ALTER TABLE `customer_response`
  ADD PRIMARY KEY (`kd_response`),
  ADD KEY `kd_subm_quat` (`kd_subm_quat`);

--
-- Indexes for table `detail_potpen`
--
ALTER TABLE `detail_potpen`
  ADD PRIMARY KEY (`kd_det_pot`),
  ADD KEY `kd_potpen` (`kd_potpen`),
  ADD KEY `kd_jns_layanan` (`kd_jns_layanan`);

--
-- Indexes for table `existing`
--
ALTER TABLE `existing`
  ADD PRIMARY KEY (`kd_ex`),
  ADD KEY `kd_cust` (`kd_cust`),
  ADD KEY `kd_jns_layanan` (`kd_jns_layanan`);

--
-- Indexes for table `expec`
--
ALTER TABLE `expec`
  ADD PRIMARY KEY (`kd_expec`),
  ADD UNIQUE KEY `nm_expec` (`nm_expec`);

--
-- Indexes for table `jns_layanan`
--
ALTER TABLE `jns_layanan`
  ADD PRIMARY KEY (`kd_jns_layanan`),
  ADD UNIQUE KEY `nm_jns_layanan` (`nm_jns_layanan`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`kd_pengguna`),
  ADD KEY `kd_posisi` (`kd_posisi`);

--
-- Indexes for table `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`kd_posisi`);

--
-- Indexes for table `potpen`
--
ALTER TABLE `potpen`
  ADD PRIMARY KEY (`kd_potpen`),
  ADD KEY `kd_cust` (`kd_cust`);

--
-- Indexes for table `product_pres`
--
ALTER TABLE `product_pres`
  ADD PRIMARY KEY (`kd_product_pres`),
  ADD KEY `kd_canv` (`kd_canv`);

--
-- Indexes for table `segment_cust`
--
ALTER TABLE `segment_cust`
  ADD PRIMARY KEY (`kd_seg`),
  ADD UNIQUE KEY `nm_seg` (`nm_seg`);

--
-- Indexes for table `solution_design`
--
ALTER TABLE `solution_design`
  ADD PRIMARY KEY (`kd_sol`),
  ADD KEY `kd_product_prees` (`kd_product_pres`),
  ADD KEY `kd_cusol` (`kd_cusol`),
  ADD KEY `kd_expec` (`kd_expec`);

--
-- Indexes for table `submit_quot`
--
ALTER TABLE `submit_quot`
  ADD PRIMARY KEY (`kd_subm_quat`),
  ADD KEY `kd_quad` (`kd_product_pres`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `canvasing`
--
ALTER TABLE `canvasing`
  MODIFY `kd_canv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `create_quatation`
--
ALTER TABLE `create_quatation`
  MODIFY `kd_quad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cusol`
--
ALTER TABLE `cusol`
  MODIFY `kd_cusol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cust`
--
ALTER TABLE `cust`
  MODIFY `kd_cust` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `customer_response`
--
ALTER TABLE `customer_response`
  MODIFY `kd_response` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `detail_potpen`
--
ALTER TABLE `detail_potpen`
  MODIFY `kd_det_pot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `existing`
--
ALTER TABLE `existing`
  MODIFY `kd_ex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `expec`
--
ALTER TABLE `expec`
  MODIFY `kd_expec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jns_layanan`
--
ALTER TABLE `jns_layanan`
  MODIFY `kd_jns_layanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `kd_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `posisi`
--
ALTER TABLE `posisi`
  MODIFY `kd_posisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `potpen`
--
ALTER TABLE `potpen`
  MODIFY `kd_potpen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_pres`
--
ALTER TABLE `product_pres`
  MODIFY `kd_product_pres` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `segment_cust`
--
ALTER TABLE `segment_cust`
  MODIFY `kd_seg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `solution_design`
--
ALTER TABLE `solution_design`
  MODIFY `kd_sol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `submit_quot`
--
ALTER TABLE `submit_quot`
  MODIFY `kd_subm_quat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `canvasing`
--
ALTER TABLE `canvasing`
  ADD CONSTRAINT `canvasing_ibfk_1` FOREIGN KEY (`kd_potpen`) REFERENCES `potpen` (`kd_potpen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `create_quatation`
--
ALTER TABLE `create_quatation`
  ADD CONSTRAINT `create_quatation_ibfk_1` FOREIGN KEY (`kd_product_pres`) REFERENCES `product_pres` (`kd_product_pres`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `create_quatation_ibfk_2` FOREIGN KEY (`kd_jns_layanan`) REFERENCES `jns_layanan` (`kd_jns_layanan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `cust`
--
ALTER TABLE `cust`
  ADD CONSTRAINT `cust_ibfk_1` FOREIGN KEY (`kd_seg`) REFERENCES `segment_cust` (`kd_seg`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cust_ibfk_2` FOREIGN KEY (`kd_pengguna`) REFERENCES `pengguna` (`kd_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `customer_response`
--
ALTER TABLE `customer_response`
  ADD CONSTRAINT `customer_response_ibfk_1` FOREIGN KEY (`kd_subm_quat`) REFERENCES `submit_quot` (`kd_subm_quat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_potpen`
--
ALTER TABLE `detail_potpen`
  ADD CONSTRAINT `detail_potpen_ibfk_1` FOREIGN KEY (`kd_potpen`) REFERENCES `potpen` (`kd_potpen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_potpen_ibfk_2` FOREIGN KEY (`kd_jns_layanan`) REFERENCES `jns_layanan` (`kd_jns_layanan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `existing`
--
ALTER TABLE `existing`
  ADD CONSTRAINT `existing_ibfk_1` FOREIGN KEY (`kd_cust`) REFERENCES `cust` (`kd_cust`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD CONSTRAINT `pengguna_ibfk_1` FOREIGN KEY (`kd_posisi`) REFERENCES `posisi` (`kd_posisi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `potpen`
--
ALTER TABLE `potpen`
  ADD CONSTRAINT `potpen_ibfk_1` FOREIGN KEY (`kd_cust`) REFERENCES `cust` (`kd_cust`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `product_pres`
--
ALTER TABLE `product_pres`
  ADD CONSTRAINT `product_pres_ibfk_1` FOREIGN KEY (`kd_canv`) REFERENCES `canvasing` (`kd_canv`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `solution_design`
--
ALTER TABLE `solution_design`
  ADD CONSTRAINT `solution_design_ibfk_1` FOREIGN KEY (`kd_cusol`) REFERENCES `cusol` (`kd_cusol`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `solution_design_ibfk_2` FOREIGN KEY (`kd_product_pres`) REFERENCES `product_pres` (`kd_product_pres`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `solution_design_ibfk_3` FOREIGN KEY (`kd_expec`) REFERENCES `expec` (`kd_expec`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `submit_quot`
--
ALTER TABLE `submit_quot`
  ADD CONSTRAINT `submit_quot_ibfk_1` FOREIGN KEY (`kd_product_pres`) REFERENCES `product_pres` (`kd_product_pres`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
