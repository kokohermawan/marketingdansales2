-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Des 2019 pada 11.55
-- Versi server: 10.1.40-MariaDB
-- Versi PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketingdansales2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `canvasing`
--

CREATE TABLE `canvasing` (
  `kd_canv` int(11) NOT NULL,
  `kd_potpen` int(11) NOT NULL,
  `cen` varchar(100) NOT NULL,
  `prog_canv` varchar(50) NOT NULL,
  `tgl_canv` date NOT NULL,
  `response_canv` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `canvasing`
--

INSERT INTO `canvasing` (`kd_canv`, `kd_potpen`, `cen`, `prog_canv`, `tgl_canv`, `response_canv`) VALUES
(4, 1, '20191118AAN001', 'Initial Meeting', '2019-11-07', 'Positif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cusol`
--

CREATE TABLE `cusol` (
  `kd_cusol` int(11) NOT NULL,
  `nm_cusol` varchar(100) NOT NULL,
  `email_cusol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cusol`
--

INSERT INTO `cusol` (`kd_cusol`, `nm_cusol`, `email_cusol`) VALUES
(1, 'Muhammad Thayib Ramadhan', 'muhammad.thayib@gmail.com'),
(3, 'Andri', 'andri@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cust`
--

CREATE TABLE `cust` (
  `kd_cust` int(11) NOT NULL,
  `kd_seg` int(11) NOT NULL,
  `kd_pengguna` int(11) NOT NULL,
  `nm_cust` varchar(50) NOT NULL,
  `alm_cust` varchar(150) NOT NULL,
  `latitude` varchar(15) NOT NULL,
  `longitude` varchar(15) NOT NULL,
  `pic_cust` varchar(50) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `jabatan_pic` varchar(50) NOT NULL,
  `tgl_pelaksanaan_prospek` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cust`
--

INSERT INTO `cust` (`kd_cust`, `kd_seg`, `kd_pengguna`, `nm_cust`, `alm_cust`, `latitude`, `longitude`, `pic_cust`, `no_telp`, `jabatan_pic`, `tgl_pelaksanaan_prospek`) VALUES
(7, 15, 1, 'PT. Indosat', 'Jl. A. Yani', '-193', '2112984', 'Ujang', '09824r', 'Kepala IT', '2019-10-09'),
(9, 11, 1, 'PLN ULP Cempaka', 'Cempaka', '-10248', '9235', 'Mas Agus', '082344321', 'Kepala IT', '2019-11-05'),
(10, 14, 1, 'PT. Tirta Sukses Perkasa', 'Bati-bati KM.37', '-282', '238', 'Fuad PPT', '0834293572', 'IT Support', '2019-11-10'),
(12, 1, 1, 'BRIS KCP Kayutangi', 'Kayutangi', '-193', '22384', 'Mulya', '083234', 'Ketua Tim Sales', '2019-11-22'),
(13, 5, 5, 'Bess Finance Pelaihari', 'JL. A. Yani', '234', '324', 'Mamang', '0832345', 'Kepala IT', '2019-11-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_potpen`
--

CREATE TABLE `detail_potpen` (
  `kd_det_pot` int(11) NOT NULL,
  `kd_potpen` int(11) NOT NULL,
  `kd_jns_layanan` int(11) NOT NULL,
  `bw` varchar(10) NOT NULL,
  `provider` varchar(50) NOT NULL,
  `biaya_sewa` int(11) NOT NULL,
  `est_kont_berakhir` date NOT NULL,
  `est_mulai_pengadaan` date NOT NULL,
  `metode_pengadaan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_potpen`
--

INSERT INTO `detail_potpen` (`kd_det_pot`, `kd_potpen`, `kd_jns_layanan`, `bw`, `provider`, `biaya_sewa`, `est_kont_berakhir`, `est_mulai_pengadaan`, `metode_pengadaan`) VALUES
(1, 1, 1, '12', 'PT. Telekomunikasi', 15000000, '2019-11-01', '2019-11-01', 'Langsung'),
(2, 1, 12, '2', 'PT. Telekomunikasi', 5000000, '2019-11-02', '2019-11-02', 'Langsung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `existing`
--

CREATE TABLE `existing` (
  `kd_ex` int(11) NOT NULL,
  `kd_cust` int(11) NOT NULL,
  `kd_jns_layanan` int(11) NOT NULL,
  `sid` varchar(50) NOT NULL,
  `bw` int(11) NOT NULL,
  `pa_number` varchar(100) NOT NULL,
  `harga_sewa` int(11) NOT NULL,
  `service_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `expec`
--

CREATE TABLE `expec` (
  `kd_expec` int(11) NOT NULL,
  `nm_expec` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `expec`
--

INSERT INTO `expec` (`kd_expec`, `nm_expec`) VALUES
(1, 'Lower Price'),
(2, 'Value Added Services'),
(3, 'Training'),
(4, 'Higher Specification');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jns_layanan`
--

CREATE TABLE `jns_layanan` (
  `kd_jns_layanan` int(11) NOT NULL,
  `nm_jns_layanan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jns_layanan`
--

INSERT INTO `jns_layanan` (`kd_jns_layanan`, `nm_jns_layanan`) VALUES
(1, 'IPVPN'),
(2, 'Clear Channel'),
(3, 'Metro Ethernet'),
(4, 'IP VSAT'),
(5, 'Data Center & Cloud Service'),
(6, 'Managed Service'),
(7, 'Infrastruktur Telekomunikasi'),
(8, 'Internet Coorporate'),
(9, 'IX'),
(10, 'IIX'),
(11, 'IP Transit'),
(12, 'ISEE'),
(13, 'IWON'),
(14, 'ICONApps');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `kd_pengguna` int(11) NOT NULL,
  `kd_posisi` int(11) NOT NULL,
  `nm_pengguna` varchar(50) NOT NULL,
  `inisial` varchar(10) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`kd_pengguna`, `kd_posisi`, `nm_pengguna`, `inisial`, `nip`, `username`, `pwd`) VALUES
(1, 1, 'koko', 'KH', '128485', 'koko', 'koko'),
(5, 1, 'Aan', 'AAN', '93746523', 'aan', 'aan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posisi`
--

CREATE TABLE `posisi` (
  `kd_posisi` int(11) NOT NULL,
  `nm_posisi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posisi`
--

INSERT INTO `posisi` (`kd_posisi`, `nm_posisi`) VALUES
(1, 'Administrator'),
(2, 'Admin Sales'),
(3, 'Sales'),
(4, 'Manager');

-- --------------------------------------------------------

--
-- Struktur dari tabel `potpen`
--

CREATE TABLE `potpen` (
  `kd_potpen` int(11) NOT NULL,
  `kd_cust` int(11) NOT NULL,
  `status_potensi` varchar(30) NOT NULL,
  `nama_ppk` varchar(100) NOT NULL,
  `telp_ppk` varchar(20) NOT NULL,
  `tgl_potpen` date NOT NULL,
  `rencana` varchar(40) NOT NULL,
  `kendala` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `potpen`
--

INSERT INTO `potpen` (`kd_potpen`, `kd_cust`, `status_potensi`, `nama_ppk`, `telp_ppk`, `tgl_potpen`, `rencana`, `kendala`) VALUES
(1, 9, 'Kuning', 'Ujang', '082341', '2019-11-16', 'Kunjungan Kembali', 'Tidak Ada Kendala');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_pres`
--

CREATE TABLE `product_pres` (
  `kd_product_pres` int(11) NOT NULL,
  `kd_canv` int(11) NOT NULL,
  `progres_product_pres` varchar(20) NOT NULL,
  `meeting_plan_date` date NOT NULL,
  `actual_meeting_date` date NOT NULL,
  `response_product_pres` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_pres`
--

INSERT INTO `product_pres` (`kd_product_pres`, `kd_canv`, `progres_product_pres`, `meeting_plan_date`, `actual_meeting_date`, `response_product_pres`) VALUES
(1, 4, 'Presentasi Produk', '2019-11-01', '2019-11-01', 'Positif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `segment_cust`
--

CREATE TABLE `segment_cust` (
  `kd_seg` int(11) NOT NULL,
  `nm_seg` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `segment_cust`
--

INSERT INTO `segment_cust` (`kd_seg`, `nm_seg`) VALUES
(1, 'Banking & Financial'),
(2, 'Consultant, Contract'),
(3, 'Education'),
(4, 'Energy Utility Mining'),
(5, 'Government'),
(6, 'Healthcare'),
(7, 'Hospitality'),
(8, 'Manufacture'),
(9, 'Media & Entertain'),
(10, 'Natural Resources'),
(11, 'PLN Group'),
(12, 'Professional Association'),
(13, 'Property'),
(14, 'Retail Distribution'),
(15, 'Telecommunication'),
(16, 'Transportation'),
(19, 'Ritel');

-- --------------------------------------------------------

--
-- Struktur dari tabel `solution_design`
--

CREATE TABLE `solution_design` (
  `kd_sol` int(11) NOT NULL,
  `kd_product_pres` int(11) NOT NULL,
  `progres_sol` varchar(20) NOT NULL,
  `kd_expec` int(11) NOT NULL,
  `tindak_lanjut` varchar(50) NOT NULL,
  `special_sol` varchar(10) NOT NULL,
  `kd_cusol` int(11) NOT NULL,
  `finish_date` date NOT NULL,
  `notes` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `solution_design`
--

INSERT INTO `solution_design` (`kd_sol`, `kd_product_pres`, `progres_sol`, `kd_expec`, `tindak_lanjut`, `special_sol`, `kd_cusol`, `finish_date`, `notes`) VALUES
(5, 1, 'Solution Design', 3, 'Kunjungan Kembali', 'Yes', 1, '2019-11-01', 'tambahe lage');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `canvasing`
--
ALTER TABLE `canvasing`
  ADD PRIMARY KEY (`kd_canv`),
  ADD KEY `kd_potpen` (`kd_potpen`);

--
-- Indeks untuk tabel `cusol`
--
ALTER TABLE `cusol`
  ADD PRIMARY KEY (`kd_cusol`);

--
-- Indeks untuk tabel `cust`
--
ALTER TABLE `cust`
  ADD PRIMARY KEY (`kd_cust`),
  ADD KEY `kd_seg` (`kd_seg`),
  ADD KEY `kd_pengguna` (`kd_pengguna`);

--
-- Indeks untuk tabel `detail_potpen`
--
ALTER TABLE `detail_potpen`
  ADD PRIMARY KEY (`kd_det_pot`),
  ADD KEY `kd_potpen` (`kd_potpen`),
  ADD KEY `kd_jns_layanan` (`kd_jns_layanan`);

--
-- Indeks untuk tabel `existing`
--
ALTER TABLE `existing`
  ADD PRIMARY KEY (`kd_ex`),
  ADD KEY `kd_cust` (`kd_cust`),
  ADD KEY `kd_jns_layanan` (`kd_jns_layanan`);

--
-- Indeks untuk tabel `expec`
--
ALTER TABLE `expec`
  ADD PRIMARY KEY (`kd_expec`);

--
-- Indeks untuk tabel `jns_layanan`
--
ALTER TABLE `jns_layanan`
  ADD PRIMARY KEY (`kd_jns_layanan`);

--
-- Indeks untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`kd_pengguna`),
  ADD KEY `kd_posisi` (`kd_posisi`);

--
-- Indeks untuk tabel `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`kd_posisi`);

--
-- Indeks untuk tabel `potpen`
--
ALTER TABLE `potpen`
  ADD PRIMARY KEY (`kd_potpen`),
  ADD KEY `kd_cust` (`kd_cust`);

--
-- Indeks untuk tabel `product_pres`
--
ALTER TABLE `product_pres`
  ADD PRIMARY KEY (`kd_product_pres`),
  ADD KEY `kd_canv` (`kd_canv`);

--
-- Indeks untuk tabel `segment_cust`
--
ALTER TABLE `segment_cust`
  ADD PRIMARY KEY (`kd_seg`);

--
-- Indeks untuk tabel `solution_design`
--
ALTER TABLE `solution_design`
  ADD PRIMARY KEY (`kd_sol`),
  ADD KEY `kd_product_prees` (`kd_product_pres`),
  ADD KEY `kd_cusol` (`kd_cusol`),
  ADD KEY `kd_expec` (`kd_expec`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `canvasing`
--
ALTER TABLE `canvasing`
  MODIFY `kd_canv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `cusol`
--
ALTER TABLE `cusol`
  MODIFY `kd_cusol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `cust`
--
ALTER TABLE `cust`
  MODIFY `kd_cust` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `detail_potpen`
--
ALTER TABLE `detail_potpen`
  MODIFY `kd_det_pot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `existing`
--
ALTER TABLE `existing`
  MODIFY `kd_ex` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `expec`
--
ALTER TABLE `expec`
  MODIFY `kd_expec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `jns_layanan`
--
ALTER TABLE `jns_layanan`
  MODIFY `kd_jns_layanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `kd_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `posisi`
--
ALTER TABLE `posisi`
  MODIFY `kd_posisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `potpen`
--
ALTER TABLE `potpen`
  MODIFY `kd_potpen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `product_pres`
--
ALTER TABLE `product_pres`
  MODIFY `kd_product_pres` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `segment_cust`
--
ALTER TABLE `segment_cust`
  MODIFY `kd_seg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `solution_design`
--
ALTER TABLE `solution_design`
  MODIFY `kd_sol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `canvasing`
--
ALTER TABLE `canvasing`
  ADD CONSTRAINT `canvasing_ibfk_1` FOREIGN KEY (`kd_potpen`) REFERENCES `potpen` (`kd_potpen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `cust`
--
ALTER TABLE `cust`
  ADD CONSTRAINT `cust_ibfk_1` FOREIGN KEY (`kd_seg`) REFERENCES `segment_cust` (`kd_seg`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cust_ibfk_2` FOREIGN KEY (`kd_pengguna`) REFERENCES `pengguna` (`kd_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_potpen`
--
ALTER TABLE `detail_potpen`
  ADD CONSTRAINT `detail_potpen_ibfk_1` FOREIGN KEY (`kd_potpen`) REFERENCES `potpen` (`kd_potpen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_potpen_ibfk_2` FOREIGN KEY (`kd_jns_layanan`) REFERENCES `jns_layanan` (`kd_jns_layanan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `existing`
--
ALTER TABLE `existing`
  ADD CONSTRAINT `existing_ibfk_1` FOREIGN KEY (`kd_cust`) REFERENCES `cust` (`kd_cust`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD CONSTRAINT `pengguna_ibfk_1` FOREIGN KEY (`kd_posisi`) REFERENCES `posisi` (`kd_posisi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `potpen`
--
ALTER TABLE `potpen`
  ADD CONSTRAINT `potpen_ibfk_1` FOREIGN KEY (`kd_cust`) REFERENCES `cust` (`kd_cust`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `product_pres`
--
ALTER TABLE `product_pres`
  ADD CONSTRAINT `product_pres_ibfk_1` FOREIGN KEY (`kd_canv`) REFERENCES `canvasing` (`kd_canv`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `solution_design`
--
ALTER TABLE `solution_design`
  ADD CONSTRAINT `solution_design_ibfk_1` FOREIGN KEY (`kd_cusol`) REFERENCES `cusol` (`kd_cusol`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `solution_design_ibfk_2` FOREIGN KEY (`kd_product_pres`) REFERENCES `product_pres` (`kd_product_pres`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `solution_design_ibfk_3` FOREIGN KEY (`kd_expec`) REFERENCES `expec` (`kd_expec`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
