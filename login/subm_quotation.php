<?php 
include '../config.php';
include 'fungsi_tgl.php';
error_reporting(0); // UNTUK MENYEMBUNYIKAN ERROR
session_start();
if ($_SESSION['status']!="login") {
    echo "<script>alert('Login dulu')</script>";
    echo '<script type="text/javascript">window.location="../"</script>';
}
$kd_pengguna = $_SESSION['kd_pengguna'];

$que = mysqli_query($db, "SELECT * FROM pengguna WHERE kd_pengguna='$kd_pengguna'")or die(mysqli_error());
$pengguna = mysqli_fetch_array($que);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Data Submit Quotation</title>

    <!-- Fontfaces CSS-->
    <link href="../css/font-face.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/theme.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../images/icon/icon2.jpg" />

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <img src="../images/icon/logoicon.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a href="customer_tampil.php">
                                <i class="fas fa-chart-bar"></i>Data Customer</a>
                        </li>
                        <li>
                            <a href="portofolio_tampil.php">
                                <i class="fas fa-table"></i>Portofolio Pendaftar</a>
                        </li>
                        <li>
                            <a href="approve_tampil.php">
                                <i class="fas fa-table"></i>Approve</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <?php include "menu.php"; ?>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="../images/icon/logo-person.png" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="../images/icon/logo-person.png" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $pengguna['nm_pengguna']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $pengguna['nip'] ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../logout_proses.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3>Data Submit Quotation</h3><br>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#tambah">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="col-lg-5">
                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="text" id="input" onkeyup="searchTable()" placeholder="search" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-responsive table--no-card m-b-30">
                                            <table class="table table-top-campaign">
                                                <thead>
                                                    <tr>
                                                        <th rowspan="2">NO</th>
                                                        <th rowspan="2">Nama Customer</th>
                                                        <th rowspan="2">Submission Date</th>
                                                        <th colspan="3" style="text-align: center;">Attention To:</th>
                                                        <th rowspan="2">Aksi</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                        <th>Phone Number</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    if($_SESSION['level']=="Sales"){
                                                        $query = mysqli_query($db,"SELECT kd_subm_quat, submit_quot.kd_product_pres, nm_cust, date_subm, nm_subm, position_subm, phone_subm FROM submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND cust.kd_pengguna='$_SESSION[kd_pengguna]' GROUP BY nm_cust")or die(mysqli_error($db));
                                                    }else{
                                                        $query = mysqli_query($db,"SELECT kd_subm_quat, submit_quot.kd_product_pres, nm_cust, date_subm, nm_subm, position_subm, phone_subm FROM submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
                                                    }
                                                        $no=0;
                                                        while ($data = mysqli_fetch_assoc($query)) {
                                                            
                                                            $no++;

                                                     ?>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['nm_cust'] ?></td>
                                                        <td><?php echo format_indo($data['date_subm']) ?></td>
                                                        <td><?php echo $data['nm_subm'] ?></td>
                                                        <td><?php echo $data['position_subm'] ?></td>
                                                        <td><?php echo $data['phone_subm'] ?></td>
                                                        <td width="10%">
                                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit<?php echo $data['kd_subm_quat'] ?>">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus<?php echo $data['kd_subm_quat'] ?>">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>
    </div>
    <!-- MODAL TAMBAH -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Create Submit Quotation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="subm_quotation.php" method="POST">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select name="kd_product_pres" id="select" class="form-control">
                                    <?php 
                                    if($_SESSION['level']=="Sales"){
                                        $query = mysqli_query($db,"SELECT create_quatation.kd_product_pres, nm_cust FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan AND cust.kd_pengguna='$_SESSION[kd_pengguna]' GROUP BY nm_cust")or die(mysqli_error($db));
                                    }else{
                                        $query = mysqli_query($db,"SELECT create_quatation.kd_product_pres, nm_cust FROM create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
                                    }
                                        while ($data = mysqli_fetch_array($query)) {

                                     ?>
                                    <option value="<?php echo $data['kd_product_pres'] ?>"><?php echo $data['nm_cust'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-12 col-md-5">
                                <p class="form-control-static"><hr></p>
                            </div>
                            <div class="col col-md-2">
                                <label class=" form-control-label"><b>Attention to</b></label>
                            </div>
                            <div class="col-12 col-md-5">
                                <p class="form-control-static"><hr></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Date Submission</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="date" class="form-control" name="date_subm" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Name</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="nm_subm" placeholder="Input Name" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Position</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="position_subm" placeholder="Input Position" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Phone Number</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="phone_subm" placeholder="Input Phone Number" required>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL EDIT -->
    <?php
    $edit = mysqli_query($db,"SELECT kd_subm_quat, submit_quot.kd_product_pres, nm_cust, date_subm, nm_subm, position_subm, phone_subm FROM submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
    while ($dataedit = mysqli_fetch_array($edit)){ 

    ?>
    <div class="modal fade" id="edit<?php echo $dataedit['kd_subm_quat'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Update Submit Quotation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="subm_quotation.php" method="POST">
                        <input type="hidden" name="kd_subm_quat" value="<?php echo $dataedit['kd_subm_quat'] ?>">
                        <input type="hidden" name="kd_product_pres" value="<?php echo $dataedit['kd_product_pres'] ?>">
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Customer</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <select id="select" class="form-control" disabled>
                                    <option value="<?php echo $dataedit['kd_product_pres'] ?>"><?php echo $dataedit['nm_cust'] ?></option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-12 col-md-5">
                                <p class="form-control-static"><hr></p>
                            </div>
                            <div class="col col-md-2">
                                <label class=" form-control-label"><b>Attention to</b></label>
                            </div>
                            <div class="col-12 col-md-5">
                                <p class="form-control-static"><hr></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Date Submission</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="date" class="form-control" name="date_subm" value="<?php echo $dataedit['date_subm'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Name</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="nm_subm" placeholder="Input Name" value="<?php echo $dataedit['nm_subm'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Position</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="position_subm" placeholder="Input Position" value="<?php echo $dataedit['position_subm'] ?>" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col col-md-4">
                                <label class=" form-control-label">Phone Number</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" class="form-control" name="phone_subm" placeholder="Input Phone Number" value="<?php echo $dataedit['phone_subm'] ?>" required>
                            </div>
                        </div><br>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="edit" class="btn btn-primary" value="Save">
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>

    <!-- MODAL HAPUS -->
    <?php
    $hapus = mysqli_query($db,"SELECT kd_subm_quat, submit_quot.kd_product_pres, nm_cust, date_subm, nm_subm, position_subm, phone_subm FROM submit_quot, create_quatation, product_pres, canvasing, potpen, cust, jns_layanan WHERE submit_quot.kd_product_pres=product_pres.kd_product_pres AND create_quatation.kd_product_pres=product_pres.kd_product_pres AND product_pres.kd_canv=canvasing.kd_canv AND canvasing.kd_potpen=potpen.kd_potpen AND potpen.kd_cust=cust.kd_cust AND create_quatation.kd_jns_layanan=jns_layanan.kd_jns_layanan GROUP BY nm_cust")or die(mysqli_error($db));
    while ($datahapus = mysqli_fetch_array($hapus)){ 

    ?>
        <div class="modal fade" id="hapus<?php echo $datahapus['kd_subm_quat'] ?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Delete Submit Quotation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="subm_quotation.php" method="POST">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Yakin Ingin Menghapus Data Submit Quotation Atas Pelanggan <b><?php echo $datahapus['nm_cust'] ?>?</b></label>
                                    <input type="hidden" name="kd_subm_quat" value="<?php echo $datahapus['kd_subm_quat'] ?>">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="hapus" class="btn btn-primary" value="Delete">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- QUERY PROSES -->
    <?php 
    if ($_POST['simpan']) {
    $kd_product_pres = $_POST['kd_product_pres'];
    $date_subm = $_POST['date_subm'];
    $now = date('Y-m-d');
    $nm_subm = $_POST['nm_subm'];
    $position_subm = $_POST['position_subm'];
    $phone_subm = $_POST['phone_subm'];
    $validasi_tambah = "valid";

    $cek = mysqli_query($db,"SELECT submit_quot.kd_product_pres FROM submit_quot");
    while($datacek = mysqli_fetch_assoc($cek)){
        if ($kd_product_pres==$datacek['kd_product_pres']) {
            echo "<script>alert('Data Customer Sudah Ada, Mohon Masukkan Data Customer Yang Berbeda')</script>";
            echo '<script type="text/javascript">window.location="subm_quotation.php?halaman=subm_quotation"</script>';    
            $validasi_tambah = "tidak valid";
        }
    }
    if($date_subm>$now){
        echo "<script>alert('Gagal! Tanggal Submission tidak boleh setelah hari ini')</script>";
        echo '<script type="text/javascript">window.location="subm_quotation.php?halaman=subm_quotation"</script>';    
        $validasi_tambah = "tidak valid";
    }
    
    if($validasi_tambah!="tidak valid"){
        $tambah = mysqli_query($db,"INSERT INTO submit_quot VALUES('','$kd_product_pres','$date_subm','$nm_subm','$position_subm','$phone_subm')")or die(mysqli_error($db));

        if ($tambah) {
            echo "<script>alert('Berhasil Tambah Data')</script>";
            echo '<script type="text/javascript">window.location="subm_quotation.php?halaman=subm_quotation"</script>';
        }else{
            echo "<script>alert('Gagal Tambah Data')</script>";
            echo '<script type="text/javascript">window.location="subm_quotation.php?halaman=subm_quotation"</script>';
        }
    }
}elseif ($_POST['edit']) {
    $kd_subm_quat = $_POST['kd_subm_quat'];
    $kd_product_pres = $_POST['kd_product_pres'];
    $date_subm = $_POST['date_subm'];
    $nm_subm = $_POST['nm_subm'];
    $position_subm = $_POST['position_subm'];
    $phone_subm = $_POST['phone_subm'];

    $edit = mysqli_query($db,"UPDATE submit_quot SET kd_product_pres='$kd_product_pres', date_subm='$date_subm', nm_subm='$nm_subm', position_subm='$position_subm', phone_subm='$phone_subm'");
    if ($edit) {
        echo "<script>alert('Berhasil Edit Data Submit Quotation')</script>";
        echo '<script type="text/javascript">window.location="subm_quotation.php?halaman=subm_quotation"</script>';
    }else{
        echo "<script>alert('Gagal Edit Data Canvasing')</script>";
        echo '<script type="text/javascript">window.location="subm_quotation.php?halaman=subm_quotation"</script>';
    }
}elseif ($_POST['hapus']) {
    $kd_subm_quat = $_POST['kd_subm_quat'];

    $hapus = mysqli_query($db,"DELETE FROM submit_quot WHERE kd_subm_quat='$kd_subm_quat'")or die(mysqli_error($db));

    if ($hapus) {
    echo "<script>alert('Berhasil Menghapus Data')</script>";
    echo '<script type="text/javascript">window.location="subm_quotation.php?halaman=subm_quotation"</script>';
    }else{
        echo "<script>alert('Gagal Menghapus Data')</script>";
        echo '<script type="text/javascript">window.location="subm_quotation.php?halaman=subm_quotation"</script>';
    }
}

     ?>
    <!-- DISABLE ENABLE REASON CUSTOMER NEGATIF -->
I     <script type="text/javascript">
        var sel = document.getElementById("resp"), text = document.getElementById("reason_canv");

        sel.onchange = function(e) {
            text.disabled = (sel.value != "Negatif");
        };
        function searchTable(){
            var input;
            var saring;
            var status;
            var tbody;
            var tr;
            var td;
            var i;
            var j;

            input = document.getElementById("input");
            saring = input.value.toUpperCase();
            tbody = document.getElementsByTagName("tbody")[0];;
            tr = tbody.getElementsByTagName("tr");
            for(i = 0; i < tr.length; i++){
                td = tr[i].getElementsByTagName("td");
                for(j = 0; j < td.length; j++){
                    if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1){
                        status = true;
                    }
                }
                    if (status) {
                        tr[i].style.display = "";
                        status = false;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
             }
     </script>
    <!-- Jquery JS-->
    <script src="../vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../vendor/slick/slick.min.js">
    </script>
    <script src="../vendor/wow/wow.min.js"></script>
    <script src="../vendor/animsition/animsition.min.js"></script>
    <script src="../vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../js/main.js"></script>

</body>

</html>
<!-- end document-->